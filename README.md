This is a simple program to calculate reflection, transmission, absorption, Purcell factor and electric field profiles in 1D layered heterostructures.

![Screenshot](./screenshot.png "Title")
