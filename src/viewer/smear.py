def smear(y,x,params={}):
	from statsmodels.nonparametric.smoothers_lowess import lowess
	import numpy as np
	prm = {'it':0,'frac':0.1}
	prm.update(params)
	fact=y.max()
	res=lowess(y,x,**prm)
	fact /= res.max()
	return np.multiply(res,fact)