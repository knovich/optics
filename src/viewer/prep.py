import numpy as np
import matplotlib.pyplot as plt
from imp import reload
import smear as sm

a = np.loadtxt('graph.dat')
#a[:,[1,0]] = a[:,[0,1]]
b = np.loadtxt('graph2.dat')
#b[:,[1,0]] = b[:,[0,1]]

def myplt(x,clr=True):
	if clr:
		plt.clf()
	plt.plot(x[:,0],x[:,1],'o-',ms=2.5)