import multiprocessing as mlp
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import matplotlib as mpl
import os
import smear
import imp
import sys


type_main={
	1:[('en','float64'),('lat','float64'),('rTE','complex128'),('rTM','complex128'),
							  ('tTE','complex128'),('tTM','complex128'),
							  ('rpTE','float64'),('rpTM','float64'),('tpTE','float64'),('tpTM','float64')],
	2:[('z','float64'),('n','complex128'),('E+ y','complex128'),('H+ x','complex128'),('H+ z','complex128'),
							  ('E- y','complex128'),('H- x','complex128'),('H- z','complex128'),
							  ('H+ y','complex128'),('E+ x','complex128'),('E+ z','complex128'),
							  ('H- y','complex128'),('E- x','complex128'),('E- z','complex128')],
	3:[('en','float64'),('lat','float64'),('x1','float64'),('x2','float64'),
							  ('y1','float64'),('y2','float64'),
							  ('z1','float64'),('z2','float64'),
							  ('mx1','i'),('mx2','i'),('my1','i'),('my2','i'),('mz1','i'),('mz2','i'),
							  ('kbTE','complex128'),('kbTM','complex128'),('dkbTE','complex128'),('dkbTM','complex128'),
							  ('nx1','float64'),('nx2','float64'),('ny1','float64'),('ny2','float64'),
							  ('nz1','float64'),('nz2','float64')],
	4:[('en','float64'),('lat','float64'),('kdTE','complex128'),('kdTM','complex128')]
}

type_aux={
	3:[('en','float64'),('inTE','float64'),('inTM1','float64'),('inTM2','float64'),('inTEout','float64'),('inTM1out','float64'),('inTM2out','float64'),('modes_x','int64'),('modes_y','int64'),('modes_z','int64')]
}

type_diag={
	3:[('en','float64'),('lat','float64'),
		*[('u'+str(i),'float64') for i in range(1,6+1)],
		*[('grx'+str(i),'float64') for i in range(1,6+1)],
		*[('grw'+str(i),'float64') for i in range(1,6+1)],
		('eff_eps','complex128'),
		*[(rt,'complex128') for rt in ['r1TE','r2TE','t1TE','t2TE','r1TM','r2TM','t1TM','t2TM']],
		*[(norm,'complex128') for norm in ['normX1','normX2','normY1','normY2','normZ1','normZ2']],
		]
}

discr_cmap = colors.ListedColormap(['white', 'gold','white'])
discr_norm_pi = colors.BoundaryNorm([0,np.pi-np.finfo('float64').tiny,np.pi+np.finfo('float64').tiny,np.inf], discr_cmap.N)
discr_norm_zero = colors.BoundaryNorm([-1e100,-np.finfo('float64').eps,np.finfo('float64').eps,1e100], discr_cmap.N)

opts_3d =		{'aspect':'auto','origin':'lower','interpolation':'none'}
opts_discr_pi = {'aspect':'auto','origin':'lower','interpolation':'none','cmap':discr_cmap,'norm':discr_norm_pi}
opts_discr_zero = {'aspect':'auto','origin':'lower','interpolation':'none','cmap':discr_cmap,'norm':discr_norm_zero}


def func_3d(data):
	cm = mpl.cm.jet
	cm.set_bad('w',1)
	x,y,z = data[0:3]
	#print(z)
	if np.iscomplexobj(z):
		z= np.abs(z)
	opts = data[3].copy()
	try:
		cm = opts['cmap']
	except:
		opts.update({'cmap':cm})
	#try:
	#	norm = opts['norm']
	#except:
	#	opts.update({'cmap':cm})
	extra = data[4]
	extra.update({'post':'post.py'})
	x_uni = np.all(x==x[0,0])
	y_uni = np.all(y==y[0,0])
	if not (x_uni or y_uni):
		ax=plt.axes()
		if extra.get('log'):
			norm = colors.LogNorm(vmin=max(z.min(),z.max()*1e-6), vmax=z.max())
			opts.update({'norm':norm})
		try:
			loc = locals()
			exec(open(extra['post']).read(),globals(),loc)
			ax = loc['ax']
			opts = loc['opts']
			extra = loc['extra']
			norm = loc['norm']
			cm = loc['cm']
			z = loc['z']
			opts.update({'norm':norm})
			opts.update({'cmap':cm})
		except Exception as e:
			print(sys.exc_info())
		if not extra.get('no_title'):
			plt.title(str(extra.get('title')))
		im=ax.imshow(z,extent=[x[0,0],x[0,-1],y[0,0],y[-1,0]],**opts)
		if extra.get('colorbar'):
			cbar = plt.colorbar(im)
			if extra.get('no_log_minor'):
				cbar.set_ticks(mpl.ticker.LogLocator())
			if extra.get('no_cbar_labels'):
				cbar.ax.set_yticklabels([])
	elif x_uni and y_uni:
		plt.plot(x,y)
		plt.text(x[0,0],y[0,0],str(z[0,0]))
	else:
		xax = x[0,:] if y_uni else y[:,0]
		yax = z[0,:] if y_uni else z[:,0]
		xlab = 'hw, eV' if y_uni else 'kx, 1/nm'
		plt.plot(xax,yax)
		plt.title(str(extra.get('title')))
	return {}

def func_vector(data):
	x,y,dx,dy = data[0:4]
	plt.quiver(x,y,dx,dy)
	return {}

def func_lin_rev(data):
	y,x = data
	plt.plot(x,y)
	return {}

def smear_dat(data):
	imp.reload(smear)
	x,y = data
	return (x,smear.smear(y,x,{'return_sorted':False}))

def norm_un(data):
	factor = max([np.max(arr) for arr in data])
	return [np.divide(arr,factor) for arr in data]

def func_lin(data):
	x,y = data
	plt.plot(x,y)
	return {}

def func_multi_lin(data):
	x,ys = data[0],data[1:]
	ax=plt.axes()
	try:
		loc = locals()
		exec(open('post_lin.py').read(),globals(),loc)
		ax = loc['ax']
		x = loc['x']
		ys = loc['ys']
	except Exception as e:
		print(sys.exc_info())
	for y in ys:
		ax.plot(x,y)
	return {}

def func_multi_lin_rev(data):
	ax=plt.axes()
	try:
		loc = locals()
		exec(open('post_lin.py').read(),globals(),loc)
		ax = loc['ax']
		#opts = loc['opts']
		#extra = loc['extra']
		#norm = loc['norm']
		#cm = loc['cm']
		#opts.update({'norm':norm})
		#opts.update({'cmap':cm})
	except Exception as e:
		print(sys.exc_info())
	for dat in data:
		ax.plot(dat[1],dat[0])
	x0,x1=ax.get_xlim()
	y0,y1=ax.get_ylim()
	print(x0,x1,y0,y1)
	ax.set_aspect(abs(x1-x0)/abs(y1-y0))
	return {}


sepdata3d = lambda data:(data[0]['lat'],data[0]['en'])
maskeddata = lambda data,mask:np.ma.array(data,mask=(mask | np.isnan(data)))

prof_lam_maker = lambda name:lambda data:(data[0]['z'],abs(data[0][name]))
prof_lam_maker_compl_norm = lambda name:lambda data:(data[0]['z'],data[0][name].real/max(abs(data[0][name])),
													 data[0][name].imag/max(abs(data[0][name])))

formats_funcs_main = {
	1:{	'rpTE':		(func_3d,	lambda data:(*sepdata3d(data),data[0]['rpTE'],opts_3d,{'colorbar':True,'title':'Reflection, TE'})),
		'rpTM':		(func_3d,	lambda data:(*sepdata3d(data),data[0]['rpTM'],opts_3d,{'colorbar':True,'title':'Reflection, TM'})),
		'tpTE':		(func_3d,	lambda data:(*sepdata3d(data),data[0]['tpTE'],opts_3d,{'colorbar':True,'title':'Transmission, TE'})),
		'tpTM':		(func_3d,	lambda data:(*sepdata3d(data),data[0]['tpTM'],opts_3d,{'colorbar':True,'title':'Transmission, TM'})),
		'Absorption TE':		(func_3d,	lambda data:(*sepdata3d(data),1-data[0]['rpTE']-data[0]['tpTE'],opts_3d,{'colorbar':True,'title':'Absorption, TE'})),
		'Absorption TM':		(func_3d,	lambda data:(*sepdata3d(data),1-data[0]['rpTM']-data[0]['tpTM'],opts_3d,{'colorbar':True,'title':'Absorption, TM'})),
		'r TE phase':		(func_3d,	lambda data:(*sepdata3d(data),
										np.angle(data[0]['rTE']),opts_3d,{'colorbar':True,'title':'Reflection phase, TE'})),
		'rTE+tTE':		(func_3d,	lambda data:(*sepdata3d(data),data[0]['rTE']+data[0]['tTE'],opts_3d,{'colorbar':True,'title':'Reflection, TE'})),
		'rTE-tTE':		(func_3d,	lambda data:(*sepdata3d(data),data[0]['rTE']-data[0]['tTE'],opts_3d,{'colorbar':True,'title':'Reflection, TE'}))
	},
	2:{	**{name+' absolute value':(func_multi_lin,prof_lam_maker(name)) for name in list(zip(*type_main[2][2:]))[0]},
		**{name+' re and im, normalized (to maximumal value)':(func_multi_lin,prof_lam_maker_compl_norm(name)) for name in list(zip(*type_main[2][2:]))[0]},
		'E+ y (re,im) & E- y (re,im)':(func_multi_lin,	lambda data:(data[0]['z'],data[0]['E+ y'].real,data[0]['E+ y'].imag,data[0]['E- y'].real,data[0]['E- y'].imag )),
		'E+ x (re,im) & E+ z (re,im)':(func_multi_lin,	lambda data:(data[0]['z'],data[0]['E+ x'].real,data[0]['E+ x'].imag,data[0]['E+ z'].real,data[0]['E+ z'].imag )),
		'E- x (re,im) & E- z (re,im)':(func_multi_lin,	lambda data:(data[0]['z'],np.angle(data[0]['E- x']),np.angle(data[0]['E- z']) )),
		'E+ x (abs) & E+ z (abs), normalized (to maximum value)':(func_multi_lin,	lambda data:(data[0]['z'],*norm_un((abs(data[0]['E+ x']),abs(data[0]['E+ z']))) )),
		'E- x (abs) & E- z (abs)':(func_multi_lin,	lambda data:(data[0]['z'],abs(data[0]['E- x']),abs(data[0]['E- z']) )),
		'SUM abs**2 E+- y ':(func_multi_lin,	lambda data:(data[0]['z'],abs(data[0]['E- y'])**2+abs(data[0]['E+ y'])**2 ))
		},
	3:{	'x1':		(func_3d,	lambda data:(*sepdata3d(data),maskeddata(data[0]['x1'],(data[0]['mx1']==-3)),
									opts_3d,{'colorbar':True,'log':True,'title':'x1'})),
		'x2':		(func_3d,	lambda data:(*sepdata3d(data),maskeddata(data[0]['x2'],(data[0]['mx2']==-3)),
									opts_3d,{'colorbar':True,'log':True,'title':'x2'})),
		'y1':		(func_3d,	lambda data:(*sepdata3d(data),maskeddata(data[0]['y1'],(data[0]['my1']==-3)),
									opts_3d,{'colorbar':True,'log':True,'title':'y1'})),
		'y2':		(func_3d,	lambda data:(*sepdata3d(data),maskeddata(data[0]['y2'],(data[0]['my2']==-3)),
									opts_3d,{'colorbar':True,'log':True,'title':'y2'})),
		'xsum':		(func_3d,	lambda data:(*sepdata3d(data),maskeddata(data[0]['x1']+data[0]['x2'],(data[0]['mx1']==-3)|(data[0]['mx2']==-3)),
									opts_3d,{'colorbar':True,'log':False,'title':'x'})),
		'x wg modes':(func_3d,	lambda data:(*sepdata3d(data),maskeddata(data[0]['x1']+data[0]['x2'],(data[0]['mx1']<=0)&(data[0]['mx2']<=0)),
									opts_3d,{'colorbar':True,'log':True,'title':'x'})),
		'zsum':		(func_3d,	lambda data:(*sepdata3d(data),maskeddata(data[0]['z1']+data[0]['z2'],(data[0]['mz1']==-3)|(data[0]['mz2']==-3)),
									opts_3d,{'colorbar':True,'log':True,'title':'z'})),
		'ysum':		(func_3d,	lambda data:(*sepdata3d(data),maskeddata(data[0]['y1']+data[0]['y2'],(data[0]['my1']==-3)|(data[0]['my2']==-3)),
									opts_3d,{'colorbar':True,'log':False,'title':'y'})),
		'mask1':	(func_3d,	lambda data:(*sepdata3d(data),data[0]['mx1'],opts_3d,{})),
		'mask2':	(func_3d,	lambda data:(*sepdata3d(data),data[0]['mx2'],opts_3d,{})),
		'kbTE_re':	(func_3d,	lambda data:(*sepdata3d(data),data[0]['kbTE'].real,opts_3d,{'colorbar':True})),
		'kbTE_im':	(func_3d,	lambda data:(*sepdata3d(data),data[0]['kbTE'].imag,opts_3d,{'colorbar':True})),
		'kbTM_re':	(func_3d,	lambda data:(*sepdata3d(data),data[0]['kbTM'].real,opts_3d,{'colorbar':True})),
		'kbTM_im':	(func_3d,	lambda data:(*sepdata3d(data),data[0]['kbTM'].imag,opts_3d,{'colorbar':True})),
		'DkbTE_re':	(func_3d,	lambda data:(*sepdata3d(data),data[0]['dkbTE'].real,opts_3d,{'colorbar':True})),
		'DkbTE_abs':	(func_3d,	lambda data:(*sepdata3d(data),maskeddata(
												abs(data[0]['dkbTE']),(data[0]['my1']==-3)|(data[0]['my2']==-3)),
												opts_3d,{'colorbar':True,'log':True})),
		'DkbTM_re':	(func_3d,	lambda data:(*sepdata3d(data),data[0]['dkbTM'].real,opts_3d,{'colorbar':True})),
		'DkbTM_abs':	(func_3d,	lambda data:(*sepdata3d(data),maskeddata(
												abs(data[0]['dkbTM']),(data[0]['mx1']==-3)|(data[0]['mx2']==-3)),
												opts_3d,{'colorbar':True,'log':True})),
		'x_prob_dens':	(func_3d,	lambda data:(*sepdata3d(data),maskeddata(
												abs(data[0]['dkbTM'])*(data[0]['x1']+data[0]['x2'])*data[0]['lat']/(np.divide(data[0]['en'],197.))**2,
												(data[0]['mx1']==-3)|(data[0]['mx2']==-3)),
												opts_3d,{'colorbar':True,'log':True})),
		'y_prob_dens':	(func_3d,	lambda data:(*sepdata3d(data),maskeddata(
												abs(data[0]['dkbTE'])*(data[0]['y1']+data[0]['y2'])*data[0]['lat']/(np.divide(data[0]['en'],197.))**2,
												(data[0]['my1']==-3)|(data[0]['my2']==-3)),
												opts_3d,{'colorbar':True,'log':True})),
		'z_prob_dens':	(func_3d,	lambda data:(*sepdata3d(data),maskeddata(
												abs(data[0]['dkbTM'])*(data[0]['z1']+data[0]['z2'])*data[0]['lat']/(np.divide(data[0]['en'],197.))**2,
												(data[0]['mz1']==-3)|(data[0]['mz2']==-3)),
												opts_3d,{'colorbar':True,'log':True})),
		'DkbTM_im':	(func_3d,	lambda data:(*sepdata3d(data),data[0]['dkbTM'].imag,opts_3d,{'colorbar':True})),
		'nx1':	(func_3d,	lambda data:(*sepdata3d(data),data[0]['nx1'],opts_3d,{})),
		'nx2':	(func_3d,	lambda data:(*sepdata3d(data),data[0]['nx2'],opts_3d,{})),
		'ny1':	(func_3d,	lambda data:(*sepdata3d(data),data[0]['ny1'],opts_3d,{})),
		'ny2':	(func_3d,	lambda data:(*sepdata3d(data),data[0]['ny2'],opts_3d,{})),
		'nz1':	(func_3d,	lambda data:(*sepdata3d(data),data[0]['nz1'],opts_3d,{})),
		'nz2':	(func_3d,	lambda data:(*sepdata3d(data),data[0]['nz2'],opts_3d,{}))
		},
	4:{	'reKTE':	(func_3d,	lambda data:(*sepdata3d(data),data[0]['kdTE'].real,opts_discr_pi,{'title':'Dispersion'})),
		'imKTE':	(func_3d,	lambda data:(*sepdata3d(data),data[0]['kdTE'].imag,opts_discr_zero,{'title':'Dispersion'})),
		'reKTM':	(func_3d,	lambda data:(*sepdata3d(data),data[0]['kdTM'].real,opts_discr_pi,{'title':'Dispersion'})),
		'imKTM':	(func_3d,	lambda data:(*sepdata3d(data),data[0]['kdTM'].imag,opts_discr_zero,{'title':'Dispersion'}))
		}
}

int_lam_maker = lambda name:lambda data:(data[1]['en'],data[1][name]) # I am a proper swine
sepdata_diag = lambda data:(data[2]['lat'],np.divide(data[2]['en'],197.))
diag_lam_maker= lambda name:lambda data:(*sepdata_diag(data),data[2][name],opts_3d,{'colorbar':True,'log':True,'title':name})

formats_funcs_aux = {
	3:{	**{name+' reversed':(func_lin_rev,int_lam_maker(name)) for name in list(zip(*type_aux[3][1:]))[0]},
		**{name+' normal':(func_lin,int_lam_maker(name)) for name in list(zip(*type_aux[3][1:]))[0]},
		'inTM1 smeared':(func_lin_rev,	lambda data:smear_dat((data[1]['en'],data[1]['inTM1']))),
		'inTM2 smeared':(func_lin_rev,	lambda data:smear_dat((data[1]['en'],data[1]['inTM2']))),
		'inTMboth smeared':(func_multi_lin_rev, lambda data:(smear_dat((data[1]['en'],data[1]['inTM1'])),
													   smear_dat((data[1]['en'],data[1]['inTM2'])))),
		'inTM1out smeared':(func_lin_rev,	lambda data:smear_dat((data[1]['en'],data[1]['inTM1out']))),
		'inTE smeared':(func_lin_rev,	lambda data:smear_dat((data[1]['en'],data[1]['inTE']))),
		'inTEout smeared':(func_lin_rev,	lambda data:smear_dat((data[1]['en'],data[1]['inTEout'])))
	}
}

formats_funcs_diag = {
	3:{	**{name:(func_3d,diag_lam_maker(name)) for name in list(zip(*type_diag[3][2:]))[0]},
		'gradient x1':(func_vector, lambda data:(*sepdata_diag(data),data[2]['grx1'],data[2]['grw1'])),
		'gradient tan':(func_3d, lambda data:(*sepdata_diag(data),data[2]['grx1']/data[2]['grw1'],
										opts_3d,{'colorbar':True,'log':False})),
		'effective epsilon':(func_lin_rev, lambda data:(data[2]['en'][:,0],abs(data[2]['eff_eps'][:,0])))
	}
}

class plotproc (mlp.Process):
	def __init__(self, func, data, Export=False, name=None, style=None):
		mlp.Process.__init__(self)
		self.func = func
		self.data = data
		self.name = name
		self.Export = Export
		self.style = style
		if self.Export and not self.name:
			self.name = 'fig1.eps'
	
	def run(self):
		if self.style:
			try:
				plt.style.use(os.path.join(os.getcwd(),self.style+'.mplstyle'))
			except:pass
		post = self.func(self.data)
		for f,args in post.items():
			f(*args)
		if self.Export:
			plt.savefig(self.name+'.png',bbox_inches='tight',dpi=600,transparent=True)
		else:
			plt.show()
		#del self.data

def import_dat(filename):
	a = b = c = type = None
	try:
		with open(filename,mode='rb') as file:
			type,offs,end,latd = np.fromfile(file,dtype='i',count=4)
			file.seek(offs,os.SEEK_SET)
			a = np.fromfile(file,dtype=type_main[type],count=end*latd)
			a = a.reshape(end,latd)
	except IOError:
		a = type = None
	if type in type_aux:
		try:
			with open(filename+'_aux',mode='rb') as file:
				b = np.fromfile(file,dtype=type_aux[type],count=end)
		except IOError:
			b = None
	if type in type_diag:
		try:
			with open(filename+'_diag',mode='rb') as file:
				c = np.fromfile(file,dtype=type_diag[type],count=end*latd).reshape(end,latd)
		except IOError:
			c = None
	return type,a,b,c