import tkinter as Tk
import tkinter.ttk as ttk
import tkinter.scrolledtext as tkscr
import tkinter.messagebox as msg
import glob,os
import imp
import plots
import numpy as np


#exe = [os.getcwd()+'\\engine.exe']
#exe = [r'c:\Users\knovich\Documents\_sCience\VSProjects\new_optics\bin\engine.exe']
exe = [os.path.normpath(os.path.join(os.path.realpath(__file__),'..','..','..','bin','engine.exe'))]
print(exe)


class App(Tk.Tk):
	def chd(self):
		if len(self.tx_input.get("1.0",Tk.END))>1:
			with open('input.in','w') as file:
				file.write(self.tx_input.get("1.0",Tk.END)[:-1])
		try:
			os.chdir(self.en_dir.get())
			with open('input.in','r') as file:
				self.tx_input.delete('1.0',Tk.END)
				self.tx_input.insert('1.0',file.read())
		except:pass
		self.rescan()

	def plots_reimport(self):
		imp.reload(plots)
		self.rescan()

	def launch(self):
		import subprocess,tempfile
		try:
			#os.chdir('../../bin/dbg/')
			(fd, filename) = tempfile.mkstemp(dir=os.curdir,text=True,suffix='.in')
			tfile = os.fdopen(fd, "w")
			tfile.write(self.tx_input.get("1.0",Tk.END))
			tfile.close()
			prc=subprocess.run(exe+[filename]+['wait'],creationflags=subprocess.CREATE_NEW_CONSOLE)
		except Exception as ex:
			msg.showerror('Error',str(ex))
		finally:
			os.remove(filename)
			self.rescan()

	def run(self):
		import threading as trd
		t = trd.Thread(target=self.launch)
		t.start()
	
	def rescan(self):
		self.lb_file.selection_clear(0, Tk.END)
		self.bt_plot['state'] = Tk.DISABLED
		self.bt_exportdata['state'] = Tk.DISABLED
		self.bt_exportplot['state'] = Tk.DISABLED
		self.vals.set([])
		self.out_file.set(glob.glob('*.out'))

	def on_closing(self):
		#for job in self.jobs:
		#	job.terminate()
		with open('input.in','w') as file:
			file.write(self.tx_input.get("1.0",Tk.END)[:-1])
		self.destroy()

	def spawnplot(self, export=False):
		ind = self.lb_val.curselection()
		if ind:
			p_tuple = self.func_dict[self.lb_val.get(ind)]
			data = p_tuple[1]((self.main_dat,self.aux_dat,self.diag_dat))
			proc = plots.plotproc(p_tuple[0],data,export,
						 self.lb_file.get(self.lb_file.curselection())[:-4]+'_'+self.lb_val.get(ind),self.en_style.get())
			#self.jobs.append(proc)
			proc.start()

	def exportplot(self):
		self.spawnplot(True)

	def exportdata(self):
		ind = self.lb_val.curselection()
		if ind:
			p_tuple = self.func_dict[self.lb_val.get(ind)]
			a = [x for x in p_tuple[1]((self.main_dat,self.aux_dat,self.diag_dat)) if isinstance(x,np.ndarray)]
			marr = [isinstance(x,np.ma.MaskedArray) for x in a]
			if any(marr):
				mask = a[marr.index(True)].mask
				a =[np.ma.array(x,mask=mask) for x in a]
				b = np.stack([x.flatten().compressed() for x in a],axis=-1)
			else:
				b = np.stack([x.flatten() for x in a],axis=-1)
			name = self.lb_file.get(self.lb_file.curselection())[:-4]+'_'+self.lb_val.get(ind)+'.dat'
			np.savetxt(name,b)

	def onfileselect(self,evt):
		w = evt.widget
		index = w.curselection()
		if index:
			f = w.get(index)
			type, self.main_dat, self.aux_dat, self.diag_dat = plots.import_dat(f)
			if type and not self.main_dat is None:
				self.func_dict = plots.formats_funcs_main[type].copy()
				if not self.aux_dat is None:
					self.func_dict.update(plots.formats_funcs_aux[type])
				if not self.diag_dat is None:
					self.func_dict.update(plots.formats_funcs_diag[type])
			self.vals.set(list(self.func_dict.keys()))
			self.bt_plot['state'] = Tk.NORMAL
			self.bt_exportdata['state'] = Tk.NORMAL
			self.bt_exportplot['state'] = Tk.NORMAL
	
	def __init__(self):
		Tk.Tk.__init__(self)
		self.wm_title("Optics")
		self.columnconfigure(0,weight=1)
		self.rowconfigure(0,weight=1)
		self.fr_input = Tk.Frame(master=self)
		self.fr_input.grid(column=0,row=0,sticky='nsew')
		self.fr_input['width']=10

		self.en_dir = Tk.Entry(master=self.fr_input)
		self.en_dir.insert(0,os.getcwd())
		self.en_dir.grid(column=0,columnspan=2,row=0,sticky='ew')

		self.bt_chdir = Tk.Button(master=self.fr_input, text='Change directory', command=self.chd)
		self.bt_chdir.grid(column=2,row=0)

		self.tx_input = tkscr.ScrolledText(master=self.fr_input)
		try:
			with open('input.in','r') as file:
				self.tx_input.insert('1.0',file.read())
		except:pass
		self.tx_input.grid(column=0,row=1,columnspan=3,rowspan=3,sticky='nsew')

		self.bt_run = Tk.Button(master=self.fr_input, text='Run', command=self.run)
		self.bt_run.grid(column=0,row=4)

		self.bt_reload = Tk.Button(master=self.fr_input, text='Reload plots module', command=self.plots_reimport)
		self.bt_reload.grid(column=1,row=4)

		self.bt_rescan = Tk.Button(master=self.fr_input, text='Rescan', command=self.rescan)
		self.bt_rescan.grid(column=2,row=4)

		self.out_file = Tk.StringVar()
		self.vals = Tk.StringVar()
		self.lb_file = Tk.Listbox(master=self.fr_input,activestyle='dotbox',listvariable=self.out_file,exportselection=0)
	
		self.lb_file.bind('<<ListboxSelect>>',self.onfileselect)
		self.lb_file.grid(column=0,row=5,columnspan=3,rowspan=3,sticky='nsew')
		self.sc_file = Tk.Scrollbar(master=self.fr_input,command=self.lb_file.yview)
		self.sc_file.grid(column=2,row=5,rowspan=3,sticky='nse')
		self.lb_file['yscrollcommand'] = self.sc_file.set

		self.lb_val = Tk.Listbox(master=self.fr_input,activestyle='dotbox',listvariable=self.vals,exportselection=0)
		self.lb_val.grid(column=0,row=8,columnspan=2,rowspan=5,sticky='nsew')
		self.sc_val = Tk.Scrollbar(master=self.fr_input,command=self.lb_val.yview)
		self.sc_val.grid(column=1,row=8,rowspan=5,sticky='nse')
		self.lb_val['yscrollcommand'] = self.sc_val.set

		#self.jobs = []
		
		self.en_style = Tk.Entry(master=self.fr_input)
		self.en_style.grid(column=2,row=8,sticky='ew')
		self.en_style.insert(0,'my')

		self.bt_plot = Tk.Button(master=self.fr_input,text='Plot',state=Tk.DISABLED,command=self.spawnplot)
		self.bt_plot.grid(column=2,row=9)

		self.bt_exportdata = Tk.Button(master=self.fr_input,text='Export data',state=Tk.DISABLED,command=self.exportdata)
		self.bt_exportdata.grid(column=2,row=10)
		
		self.bt_exportplot = Tk.Button(master=self.fr_input,text='Export plot',state=Tk.DISABLED,command=self.exportplot)
		self.bt_exportplot.grid(column=2,row=11)

		self.tx_input['height']=30
		self.lb_file['height']=10
		self.lb_val['height']=10

		self.fr_input.columnconfigure(1,weight=1)
		for i in [5,7,8,12]:
			self.fr_input.rowconfigure(i,weight=1)
		for i in [1,3]:
			self.fr_input.rowconfigure(i,weight=3)
		
		self.rescan()
		
		self.protocol("WM_DELETE_WINDOW", self.on_closing)

if __name__ == '__main__':
	app = App()
	app.mainloop()