	program test
	use types
	use input_parser
	use job_control
	implicit none
	integer st,i
	type(input_block_t),dimension(:),allocatable::ibs,tmp1
	type(job_t),dimension(:),allocatable::jobs,tmp
	type(computation_p),dimension(:),allocatable::arr
	character(80)::infile,cmd


	call GET_COMMAND(cmd)
	print *,cmd
	print *,"Input file:"
	read *,infile
	call parse_input(infile,ibs,st)
	if(st == STATUS_OK)then
		call populate_jobs(ibs, jobs)
	end if
	deallocate(ibs)
	


	tmp= pack(jobs,jobs%active)
	if(size(tmp) > 0)then
		allocate(arr(1:size(tmp)))
		do i= 1,size(tmp)
			arr(i)%comp=> gen_job(tmp(i))
			if(associated(arr(i)%comp))then
				print *,"STARTED ",trim(tmp(i)%job_name)
				call arr(i)%comp%driver()
				call arr(i)%comp%output()
				deallocate(arr(i)%comp)
			end if
		end do
	end if
	print *,"Done"
	read *

	end program test