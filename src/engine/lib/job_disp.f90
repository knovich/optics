	module job_disp
	
	use types
	use keywords
	use math
	use error_report
	
	use job_rta
	
	implicit none
	
	private
	public :: make_disp
	
	
	type,extends(job_rta_t):: job_disp_t
		complex(pr),dimension(:,:),&
			allocatable			::	kd_TE, kd_TM
		integer					::	l_left =1, l_right =0
	contains
		procedure				::	init=>		init_disp
		procedure				::	driver=>	driver_disp
		procedure				::	output=>	output_disp
	end type job_disp_t
	
	
	type:: output_disp_t
		real(po)				::	energy	=0.,&
									lateral	=0.
		complex(po)				::	kd_TE =0., kd_TM =0.
	end type output_disp_t
	
	
	contains
	
	
	function init_disp(this)result(status)
	integer									::	status
	class(job_disp_t),intent(inout)			::	this
	integer									::	pos, ios
	
	status= this%job_rta_t%init()
	if(status /= STATUS_OK)then
		go to 200
	end if
	allocate(this%kd_TE(ubound(this%grid,1),ubound(this%grid,2)))
	allocate(this%kd_TM,mold=this%kd_TE)
	associate(tkws=> this%keywords)
		pos= find_keyword("left_layer",tkws)
		if(pos /= 0)then
			read(tkws(pos)%value,*,iostat=ios) this%l_left
			if(ios /= 0 .or. this%l_left <= lbound(this%layers,1) .or. this%l_left >= ubound(this%layers,1))then
				!Something went wrong during value reading
				call report_error(ERR_OTHER,SEV_RETRY,&
					"Incorrect left_layer specification for "//trim(this%job_name))
				go to 100
			end if
		end if
		
		pos= find_keyword("right_layer",tkws)
		if(pos /= 0)then
			read(tkws(pos)%value,*,iostat=ios) this%l_right
			if(ios /= 0 .or. this%l_right <= lbound(this%layers,1) .or. this%l_right >= ubound(this%layers,1))then
				!Something went wrong during value reading
				call report_error(ERR_OTHER,SEV_RETRY,&
					"Incorrect right_layer specification for "//trim(this%job_name))
				go to 100
			end if
		else
			this%l_right= ubound(this%layers,1)-1
		end if
		
		if(this%l_left > this%l_right)then
			call report_error(ERR_OTHER,SEV_RETRY,&
				"Right layer is earlier than left in "//trim(this%job_name))
			go to 100
		end if
	end associate
	
	status= STATUS_OK
	return
	
100	status= STATUS_ERROR
	return
	
	!Inherited error status
200	return
	
	end function init_disp
	
	
	subroutine driver_disp(this)
	class(job_disp_t),intent(inout)			::	this
	integer									::	i,j,k
	type(computed_layer_t),&
		dimension(&
		lbound(this%layers,1)&
		:ubound(this%layers,1),&
		ubound(this%grid,2))				::	l_row
	type(matrix_t(2)),&
		dimension(ubound(this%grid,2))		::	prod_TE, prod_TM
	real(pr)								::	tol
	
	associate(	tl=> this%layers, tg=> this%grid,&
				lbtl=> this%l_left-1, ubtl=> this%l_right+1)
		
		tol= sum(this%layers(lbtl+1:ubtl-1)%thick)
		energy:do i= lbound(tg,1),ubound(tg,1)
		!Cycle through energy
			!Assign refractive indices
			print *,i,' out of ',ubound(tg,1)
			layers:do j= lbtl,ubtl
				if(tl(j)%is_var_refr)then
					l_row(j,:)%n= tl(j)%n_func(tg(i,1))
				else
					l_row(j,:)%n= tl(j)%const_n
				end if
				if(this%no_absorption)then
					l_row(j,:)%n= sqrt(cmplx(real(l_row(j,:)%n**2),0._pr))
				end if
			end do layers
			!Assign transfer matrices
			kx:do j= lbound(tg,2),ubound(tg,2)
			!Cycle through kx
				layers:do k= lbtl+1,ubtl-1
				!Cycle through layers
					call tl(k)%TrMatTE_sub(l_row(k,j),tg(i,j))
					call tl(k)%TrMatTM_sub(l_row(k,j),tg(i,j))
				end do layers
				prod_TE(j)= mat_prod(l_row(lbtl+1:ubtl-1,j)%TrMatTE)
				prod_TM(j)= mat_prod(l_row(lbtl+1:ubtl-1,j)%TrMatTM)
				!Compute Bloch KD
			end do kx
			this%kd_TE(i,:)= acos( (prod_TE%m(1,1)+prod_TE%m(2,2))/2.0_pr ) !1.0_pr/tol*
			this%kd_TM(i,:)= acos( (prod_TM%m(1,1)+prod_TM%m(2,2))/2.0_pr ) !1.0_pr/tol*
		end do energy
	end associate
				
	end subroutine driver_disp
	
	
	subroutine output_disp(this)
	class(job_disp_t),intent(in)			::	this
	type(output_disp_t),dimension&
		(lbound(this%grid,1):&
		ubound(this%grid,1),&
		lbound(this%grid,2):&
		ubound(this%grid,2))				::	out_arr
	integer									::	unit, rl, i, j, index
	
	select case (this%energy_unit)
	case (UNIT_E_EV)
		out_arr%energy= this%grid%k0*HBARC
	case (UNIT_E_NM)
		out_arr%energy= spread(this%wl_store,2,size(this%grid,2))
	end select
	select case (this%lat_unit)
	case (UNIT_LAT_KX_NM)
		out_arr%lateral= this%grid%kx
	case (UNIT_LAT_DEGREE)
		out_arr%lateral= spread(this%angle_store,1,size(this%grid,1))
	end select
	out_arr%kd_TE= this%kd_TE
	out_arr%kd_TM= this%kd_TM
	inquire(iolength=rl)output_disp_t()
	open(newunit=unit,file=trim(this%job_name)//".out",form="unformatted",status="replace",&
		access="direct",recl=rl)
	index= 1
	write(unit,rec=index)TASK_DISP,rl,size(out_arr,1),size(out_arr,2)
	do i=lbound(out_arr,1),ubound(out_arr,1)
		do j=lbound(out_arr,2),ubound(out_arr,2)
			index=index+1
			write(unit,rec=index)out_arr(i,j)
		end do
	end do
	close(unit)
	
	end subroutine output_disp
	
	
	!Return a pointer to a disp task
	function make_disp(job)result(res)
	type(job_disp_t),pointer				::	res
	type(job_t),intent(in)					::	job
	
	allocate(res)
	res%job_t= job
	if(res%init() /= STATUS_OK)then
		deallocate(res)
		nullify(res)
	end if
	
	end function make_disp
	
	end module job_disp