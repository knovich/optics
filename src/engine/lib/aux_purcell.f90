	submodule (job_purcell) aux_purcell
	implicit none
	
	contains
	
	
	!Mask modes in waveguide regime
	module subroutine wg_masks(this,i,l_row,init_X,init_Y,init_Z)
	class(job_purcell_t),intent(inout)		::	this
	integer,intent(in)						::	i
	type(computed_layer_t),&
		dimension(&
		this%l_left-1:this%l_right+1,&
		ubound(this%grid,2)),intent(in)		::	l_row
	type(vector_t(2)),dimension(2,&
		ubound(this%grid,2)),intent(in)		::	init_X, init_Y, init_Z
	real(pr),dimension(&
		ubound(this%grid,2),6)				::	u_abs
	integer									::	j, k, l, mode(6), tick(6), roots
	
	associate(	s_type=> [SQ_X, SQ_X, SQ_Y, SQ_Y, SQ_Z, SQ_Z],&
				pol=> [POL_TM, POL_TM, POL_TE, POL_TE, POL_TM, POL_TM],&
				s=> [1,-1,1,-1,1,-1],&
				tl=> this%layers, tg=> this%grid,&
				lbtl=> this%l_left-1, ubtl=> this%l_right+1, dpl=> this%dipole_layer)
		do j=lbound(u_abs,1),ubound(u_abs,1)
			do k=1,6
				u_abs(j,k)= abs(1/this%d(i,j)%u(k))
			end do
		end do
		mode(:)= 0
		tick(:)= 0
		masks:do j= lbound(tg,2)+this%mhw,ubound(tg,2)-this%mhw
			xyz:do k= 1,6	
				if(tick(k) > 0)then
					tick(k)= tick(k)-1
					cycle
				end if
				if(any(this%d(i,j-this%mhw:j+this%mhw)%mask(k)<=MASK_NO_WG))cycle
				!if(u_abs(j,k)<SUFFICIENT)cycle
				if(mid_local_max(u_abs(j-this%mhw:j+this%mhw,k)))then
					roots= this%check_kx_root(l_row(:,j),this%grid(i,j),this%grid(i,j)%kx-this%grid(i,j-1)%kx,pol(k),s(k))
					if(roots>=0)then
						print *,roots
						tick(k)= this%mhw
						mode(k)= mode(k)+1
						this%d(i,j)%mask(k)= mode(k)+MASK_NO_MODE
						call this%wg_norms(i,j,l_row, init_X, init_Y, init_Z,tails=.true.,k_num=k)
					end if
				end if
			end do xyz
		end do masks
	end associate
	
	end subroutine wg_masks
	
	
	!Norm modes in waveguide regime
	module subroutine wg_norms(this,i,j,l_row,init_X,init_Y,init_Z,tails,k_num)
	class(job_purcell_t),intent(inout)		::	this
	integer,intent(in)						::	i,j
	type(computed_layer_t),&
		dimension(&
		this%l_left-1:this%l_right+1,&
		ubound(this%grid,2)),intent(in)		::	l_row
	type(vector_t(2)),dimension(2,&
		ubound(this%grid,2)),intent(in)		::	init_X, init_Y, init_Z
	logical,intent(in),optional				::	tails
	integer,intent(in),optional				::	k_num
	real(pr),dimension(&
		ubound(this%grid,2),6)				::	u_abs
	type(vector_t(2)),dimension(6)			::	init
	type(matrix_t(2)),dimension&
		(ubound(this%layers,1),6)			::	part_prod
	complex(pr),dimension(6)				::	norms
	integer									::	k
	
	associate(	s_type=> [SQ_X, SQ_X, SQ_Y, SQ_Y, SQ_Z, SQ_Z],&
				pol=> [POL_TM, POL_TM, POL_TE, POL_TE, POL_TM, POL_TM],&
				s=> [1,-1,1,-1,1,-1],&
				tl=> this%layers, tg=> this%grid,&
				lbtl=> this%l_left-1, ubtl=> this%l_right+1, dpl=> this%dipole_layer,&
				part_TE=> this%part_TE1, part_TM=> this%part_TM1)
		init= [init_X(:,j), init_Y(:,j), init_Z(:,j)]
		part_prod= reshape([part_TM(:,j), part_TM(:,j), part_TE(:,j), part_TE(:,j), part_TM(:,j), part_TM(:,j)],[ubtl,6])
		xyz:do k= 1,6
			if(present(k_num).and.k_num/=k)cycle
			select case (this%eff_refr_kind)
			case (REFR_NO)
				this%d(i,j)%n_eff(k)= 1._pr
			case (REFR_AN)
				this%d(i,j)%n_eff(k)= this%eff_refr_analytic(i,j,pol(k),s(k))
			case (REFR_NUM)
				this%d(i,j)%n_eff(k)= this%eff_refr_numeric(i,j,k)
			end select
			this%d(i,j)%norm(k)= (tg(i,j)%k0)**2&
							*Sq_WG_norm(init=init(k),tms=part_prod(:,k),&
							s_type=s_type(k),pt=tg(i,j),&
							c_layers=l_row(:,j),layers=tl,tails=tails)&
							/sqrt(l_row(dpl,j)%n*PI)&
							/(tg(i,j)%kx*this%d(i,j)%n_eff(k))
		end do xyz
	end associate
	
	end subroutine wg_norms
	
	
	!Adjust norm with effective refractive index
	!module subroutine eff_refr_adjust(this)
	!class(job_purcell_t),intent(inout)		::	this
	!integer									::	i, j, k, l, mode(6)
	!real(pr),dimension(&
	!	ubound(this%grid,2),6)				::	u_abs
	!complex(pr),dimension(6,&
	!	-this%mhw:this%mhw)					::	norms
	!real(pr)								::	k_cur, k_top, k_bot, dk_top, dk_bot, deriv
	!
	!associate(tg=> this%grid)
	!	do i= lbound(tg,1)+1,ubound(tg,1)-1
	!		mode(:)= 0
	!		norms(:,:)= 1._pr !ieee_value(1._pr, ieee_positive_inf)
	!		u_abs= reshape(abs([	this%u_grid(i,:)%x1,this%u_grid(i,:)%x2,&
	!								this%u_grid(i,:)%y1,this%u_grid(i,:)%y2,&
	!								this%u_grid(i,:)%z1,this%u_grid(i,:)%z2]),[ubound(tg,2),6])
	!		do k= 1,6
	!			associate(mask=> this%mask(i,:,k))
	!				mode(k)= maxval(mask)/this%mw
	!				do j= 1,mode(k)
	!					l= findloc(mask,this%mw*j-this%mhw,1)
	!					k_cur= w_avg(real(tg(i,l-this%mhw:l+this%mhw)%kx),u_abs(l-this%mhw:l+this%mhw,k))
	!					k_top= this%nearest_mode(k_cur,l,i+1,k)
	!					k_bot= this%nearest_mode(k_cur,l,i-1,k)
	!					dk_top= tg(i+1,l)%k0-tg(i,l)%k0
	!					dk_bot= tg(i,l)%k0-tg(i-1,l)%k0
	!					deriv= ((k_top-k_cur)/dk_top+(k_cur-k_bot)/dk_bot)/2._pr
	!					!VALUE CHECK
	!					select case (k)
	!					case (1)
	!						this%norm_x1(i,l-this%mhw:l+this%mhw)= this%norm_x1(i,l-this%mhw:l+this%mhw)/sqrt(deriv*tg(i,l-this%mhw:l+this%mhw)%kx)
	!					case (2)
	!						this%norm_x2(i,l-this%mhw:l+this%mhw)= this%norm_x2(i,l-this%mhw:l+this%mhw)/sqrt(deriv*tg(i,l-this%mhw:l+this%mhw)%kx)
	!					case (3)
	!						this%norm_y1(i,l-this%mhw:l+this%mhw)= this%norm_y1(i,l-this%mhw:l+this%mhw)/sqrt(deriv*tg(i,l-this%mhw:l+this%mhw)%kx)
	!					case (4)
	!						this%norm_y2(i,l-this%mhw:l+this%mhw)= this%norm_y2(i,l-this%mhw:l+this%mhw)/sqrt(deriv*tg(i,l-this%mhw:l+this%mhw)%kx)
	!					case (5)
	!						this%norm_z1(i,l-this%mhw:l+this%mhw)= this%norm_z1(i,l-this%mhw:l+this%mhw)/sqrt(deriv*tg(i,l-this%mhw:l+this%mhw)%kx)
	!					case (6)
	!						this%norm_z2(i,l-this%mhw:l+this%mhw)= this%norm_z2(i,l-this%mhw:l+this%mhw)/sqrt(deriv*tg(i,l-this%mhw:l+this%mhw)%kx)
	!					end select
	!				end do
	!			end associate
	!		end do
	!	end do
	!end associate
	!
	!end subroutine eff_refr_adjust
	
	
	module function eff_refr_analytic(this,i,j,pol,sgn)
	class(job_purcell_t),&
		intent(inout),target				::	this
	real(pr)								::	eff_refr_analytic
	integer,intent(in)						::	i,j,pol,sgn
	complex(pr)								::	r1, t1, r2, t2, drx1, dtx1, drx2, dtx2, drw1, dtw1, drw2, dtw2
	type(matrix_t(2))						::	dtmdw1, dtmdw2, dtmdkx1, dtmdkx2
	type(matrix_t(2)),dimension(:),pointer	::	part1, part2
	type(matrix_t(2)),pointer				::	tm1, tm2
	complex(pr)								::	dtnorm, relr, sq,&
												dtnorm_dx, relr_dx, sq_dx,&
												dtnorm_dw, relr_dw, sq_dw,&
												v, v_x, v_w, u, u_x, u_w
	real(pr)								::	grad_x, grad_w
	
	associate(tg=> this%grid, tl=> this%layers, lb=> this%l_left, ub=> this%l_right, s=>sgn/abs(sgn))
		select case (pol)
		case (POL_TE)
			r1= this%r1TE(i,j); t1= this%t1TE(i,j); r2= this%r2TE(i,j); t2= this%t2TE(i,j)
			part1=> this%part_TE1(lb:ub,j); part2=> this%part_TE2(lb:ub,j)
			tm1=> this%prod_TE1(j); tm2=> this%prod_TE2(j)
		case (POL_TM)
			r1= this%r1TM(i,j); t1= this%t1TM(i,j); r2= this%r2TM(i,j); t2= this%t2TM(i,j)
			part1=> this%part_TM1(lb:ub,j); part2=> this%part_TM2(lb:ub,j)
			tm1=> this%prod_TM1(j); tm2=> this%prod_TM2(j)
		end select
		dtmdw1= dTrMdw(tg(i,j),part1,this%l_row(lb:ub,j),tl(lb:ub),pol)
		dtmdkx1= dTrMdkx(tg(i,j),part1,this%l_row(lb:ub,j),tl(lb:ub),pol)
		dtmdw2= dTrMdw(tg(i,j),part2,this%l_row(ub:lb:-1,j),tl(ub:lb:-1),pol)
		dtmdkx2= dTrMdkx(tg(i,j),part2,this%l_row(ub:lb:-1,j),tl(ub:lb:-1),pol)
		call drtdw(tm1,dtmdw1,tg(i,j),tl(lb-1),tl(ub+1),pol,drw1,dtw1)
		call drtdkx(tm1,dtmdkx1,tg(i,j),tl(lb-1),tl(ub+1),pol,drx1,dtx1)
		call drtdw(tm2,dtmdw2,tg(i,j),tl(ub+1),tl(lb-1),pol,drw2,dtw2)
		call drtdkx(tm2,dtmdkx2,tg(i,j),tl(ub+1),tl(lb-1),pol,drx2,dtx2)
		
		dtnorm= (t2-t1)/(2._pr*r2)
		relr= r1/r2
		sq= sqrt(dtnorm**2+relr)
		dtnorm_dx= ((dtx2-dtx1)/r2-drx2*(t2-t1)/r2)/2._pr
		relr_dx= drx1/r2-r1*drx2/r2**2
		sq_dx= (2._pr*dtnorm*dtnorm_dx+relr_dx)/(2._pr*sq)
		dtnorm_dw= ((dtw2-dtw1)/r2-drw2*(t2-t1)/r2)/2._pr
		relr_dw= drw1/r2-r1*drw2/r2**2
		sq_dw= (2._pr*dtnorm*dtnorm_dw+relr_dw)/(2._pr*sq)
		v= -dtnorm+s*sq
		v_x= -dtnorm_dx+s*sq_dx
		v_w= -dtnorm_dw+s*sq_dw
		u= r1+t2*v
		u_x= drx1+dtx2*v+t2*v_x
		u_w= drw1+dtw2*v+t2*v_w
		grad_x= u%re*u_x%re+u%im*u_x%im
		grad_w= u%re*u_w%re+u%im*u_w%im
		eff_refr_analytic= abs(-grad_x/grad_w)
	end associate
	
	end function eff_refr_analytic
	
	
	module function eff_refr_numeric(this,i,j,k)
	class(job_purcell_t),&
		intent(inout),target				::	this
	real(pr)								::	eff_refr_numeric
	integer,intent(in)						::	i,j,k
	real(pr),dimension(-1:1,-1:1)			::	u
	real(pr)								::	grad_x, grad_w, dxt, dxb, dwt, dwb
	real(pr),dimension(-1:10)				::	ws_u, ws_w, ws_x
	integer									::	index
	
	if(i == ubound(this%grid,1) .or. i == lbound(this%grid,1))then
		eff_refr_numeric= 1._pr
		return
	end if
	u= abs(this%d(i-1:i+1,j-1:j+1)%u(k))
	associate (k0=> this%grid%k0, kx=> this%grid%kx)
		dwt= k0(i+1,j)-k0(i,j)
		dwb= k0(i,j)-k0(i-1,j)
		dxt= kx(i,j+1)-kx(i,j)
		dxb= kx(i,j)-kx(i,j+1)
	end associate
	ws_u= [u(1,-1),u(1,0),u(1,1),u(0,1),u(-1,1),u(-1,0),u(-1,-1),u(0,-1),u(1,-1),u(1,0),u(1,1),u(0,1)]
	ws_w= [real(pr)::1./dwt,1./dwt,1./dwt,0.,-1./dwb,-1./dwb,-1./dwb,0.,1./dwt,1./dwt,1./dwt,0.]
	ws_x= [real(pr)::-1./dxb,0.,1./dxt,1./dxt,1./dxt,0.,-1./dxb,-1./dxb,-1./dxb,0.,1./dxt,1./dxt]
	index= maxloc(ws_u(1:8),1)
	!grad_w= (w_avg(ws_u(index-2:index+2),ws_w(index-2:index+2))-u(0,0))/dwt
	!grad_w= -(u(1,0)+u(-1,0)-2._pr*u(0,0))/(2._pr*dw_t**2)
	grad_w= (u(1,0)-u(0,0))/dwt
	this%diag(i,j)%grad_w(k)= grad_w
	!grad_x= (w_avg(ws_u(index-2:index+2),ws_x(index-2:index+2))-u(0,0))/dxt
	!grad_x= -(u(0,1)+u(0,-1)-2._pr*u(0,0))/(2._pr*dx_t**2)
	grad_x= (u(0,1)-u(0,0))/dxt
	this%diag(i,j)%grad_x(k)= grad_x
	eff_refr_numeric= abs(grad_w/grad_x)
	!eff_refr_numeric= sqrt(abs(grad_w/grad_x))
	
	end function eff_refr_numeric
	
	
	!Find value in the nearest mode
	!module function nearest_mode(this,v,index,row,k)result(res)
	!class(job_purcell_t),intent(in)			::	this
	!real(pr)								::	res
	!real(pr),intent(in)						::	v
	!integer,intent(in)						::	index, row, k
	!real(pr),dimension(&
	!	ubound(this%grid,2),6)				::	wghts
	!integer									::	i
	!real(pr)								::	k1, k2
	!
	!wghts= reshape(abs([	this%u_grid(row,:)%x1,this%u_grid(row,:)%x2,&
	!									this%u_grid(row,:)%y1,this%u_grid(row,:)%y2,&
	!									this%u_grid(row,:)%z1,this%u_grid(row,:)%z2]),[ubound(this%grid,2),6])
	!
	!associate(mw=> this%mw, mask=> this%mask(row,:,k), vals=> real(this%grid(row,:)%kx), weights=> wghts(:,k))
	!	if(mask(index)>MASK_NO_MODE)then
	!		res= w_avg(vals(index-mod(mask(index)-1,mw):index-mod(mask(index)-1,mw)+mw-1),&
	!			weights(index-mod(mask(index)-1,mw):index-mod(mask(index)-1,mw)+mw-1))
	!	else
	!		i= index-1+minloc(mask(index:),1,mask(index:)>MASK_NO_MODE)
	!		if(i>0)then
	!			k1= w_avg(vals(i:i+mw-1),weights(i:i+mw-1))
	!		else
	!			k1= ieee_value(1._pr, ieee_positive_inf)
	!		end if
	!		i= maxloc(mask(:index),1,mask(:index)>MASK_NO_MODE)
	!		if(i>0)then
	!			k2= w_avg(vals(i-mw+1:i),weights(i-mw+1:i))
	!		else
	!			k2= ieee_value(1._pr, ieee_negative_inf)
	!		end if
	!		if(v-k2 > k1-v)then
	!			res= k1
	!		else
	!			res= k2
	!		end if
	!		if(abs(res) > huge(1._pr))res= v
	!	end if
	!end associate
	!
	!end function nearest_mode
	
	
	!Light cone integral
	module subroutine int_light_cone(this, i, l_row, mask)
	class(job_purcell_t),intent(inout)		::	this
	integer,intent(in)						::	i, mask
	logical,dimension(lbound(this%grid,2)&
		:ubound(this%grid,2))				::	mask_in_ref, mask_out_ref
	type(computed_layer_t),&
		dimension(&
		this%l_left-1:this%l_right+1,&
		ubound(this%grid,2)),intent(in)		::	l_row
	complex(pr),dimension(lbound(this%grid,2)&
		:ubound(this%grid,2))				::	dth_ref, ctharr_ref, stharr_ref, fact_ref, deriv_TE, deriv_TM
	integer									::	k, x_i, y_i, z_i, x_o=5, y_o=4, z_o=6
	
	mask_in_ref=.true.; mask_out_ref=.false.
	associate( n0=> l_row(this%dipole_layer,1)%n, k0=> this%grid(i,1)%k0, kx=> this%grid(i,:)%kx, n_back=> l_row(this%l_left-1,1)%n)
		select case(mask)
		case (MASK_NO_WG)
			x_i= 2; y_i= 1; z_i= 3
			dth_ref= [complex(pr)::(0._pr,0._pr), ( (kx(k)-kx(k-1))/sqrt((n_back*k0)**2-kx(k)**2), k=lbound(this%grid,2)+1,ubound(this%grid,2) )]
			stharr_ref= kx/(n_back*k0)
			ctharr_ref= sqrt((1._pr,0._pr)-stharr_ref**2)
			dth_ref(ubound(dth_ref,1))= 0._pr
			fact_ref= dth_ref*stharr_ref
		case (MASK_BLOCH)
			where(abs(kx)>abs(n_back*k0))
				mask_in_ref= .not. mask_in_ref
			end where
			mask_out_ref= .not. mask_in_ref
			x_i= 2; y_i= 1; z_i= 3
			fact_ref= [complex(pr)::(0._pr,0._pr), (abs( kx(k)*(kx(k)-kx(k-1)) /k0**2), k=lbound(this%grid,2)+1,ubound(this%grid,2))]
			!abs( kx(k)*(kx(k+1)-kx(k))/k0**2) 
			stharr_ref= 1._pr
			ctharr_ref= 1._pr
		case (MASK_NO_MODE)
			x_i= 5; y_i= 4; z_i= 6
			if(i>lbound(this%grid,1))then
				fact_ref= [complex(pr):: (abs( kx(k)*(kx(k+1)-kx(k)) /(k0-this%grid(i-1,1)%k0)), &
										k=lbound(this%grid,2),ubound(this%grid,2)-1), (0._pr,0._pr)]
			else
				fact_ref= 0
			end if
			stharr_ref= 1._pr
			ctharr_ref= 1._pr
		end select
	end associate
	this%int_fact(i)%vals(x_i)= 0.; this%int_fact(i)%vals(y_i)= 0.; this%int_fact(i)%vals(z_i)= 0.
	
	associate(	phi1=> this%phi1, phi2=> this%phi2, phi3=> this%phi3,&
				theta1=> this%theta1, theta2=> this%theta2, theta3=> this%theta3,&
				deriv_TE=> this%d(i,:)%dKBdk0_TE, deriv_TM=> this%d(i,:)%dKBdk0_TM)
		
		associate(	fact1=> pack(fact_ref*abs(deriv_TM),this%d(i,:)%mask(1)==mask),&
					fact2=> pack(fact_ref*abs(deriv_TM),this%d(i,:)%mask(2)==mask),&
					fact3=> pack(fact_ref*abs(deriv_TM),this%d(i,:)%mask(5)==mask),&
					fact4=> pack(fact_ref*abs(deriv_TM),this%d(i,:)%mask(6)==mask),&
					stharr1=> pack(stharr_ref,this%d(i,:)%mask(1)==mask),&
					stharr2=> pack(stharr_ref,this%d(i,:)%mask(2)==mask),&
					stharr3=> pack(stharr_ref,this%d(i,:)%mask(5)==mask),&
					stharr4=> pack(stharr_ref,this%d(i,:)%mask(6)==mask),&
					ctharr1=> pack(ctharr_ref,this%d(i,:)%mask(1)==mask),&
					ctharr2=> pack(ctharr_ref,this%d(i,:)%mask(2)==mask),&
					ctharr3=> pack(ctharr_ref,this%d(i,:)%mask(5)==mask),&
					ctharr4=> pack(ctharr_ref,this%d(i,:)%mask(6)==mask),&
					x1=> pack(this%d(i,:)%field(1),this%d(i,:)%mask(1)==mask),&
					x2=> pack(this%d(i,:)%field(2),this%d(i,:)%mask(2)==mask),&
					z1=> pack(this%d(i,:)%field(5),this%d(i,:)%mask(5)==mask),&
					z2=> pack(this%d(i,:)%field(6),this%d(i,:)%mask(6)==mask),&
					mask_in=>pack(mask_in_ref,this%d(i,:)%mask(1)==mask),&
					mask_out=>pack(mask_out_ref,this%d(i,:)%mask(1)==mask))
			
			!this%int_fact(i)%vals(x_i)= (	sin(theta2)**2*(	sum(fact1*x1*ctharr1**2,mask=.not.ieee_is_nan(x1))&
			!												+sum(fact2*x2*ctharr2**2,mask=.not.ieee_is_nan(x2))&
			!											)/2._pr&
			!							+cos(theta2)**2*(	sum(fact3*z1*stharr3**2,mask=.not.ieee_is_nan(z1))&
			!												+sum(fact4*z2*stharr4**2,mask=.not.ieee_is_nan(z2))&
			!											)&
			!						)*3._pr/(4._pr*cos(theta2)**2+sin(theta2)**2)
			!
			!this%int_fact(i)%vals(z_i)= (	sin(theta3)**2*(	sum(fact1*x1*ctharr1**2,mask=.not.ieee_is_nan(x1))&
			!												+sum(fact2*x2*ctharr2**2,mask=.not.ieee_is_nan(x2))&
			!											)/2._pr&
			!							+cos(theta3)**2*(	sum(fact3*z1*stharr3**2,mask=.not.ieee_is_nan(z1))&
			!												+sum(fact4*z2*stharr4**2,mask=.not.ieee_is_nan(z2))&
			!											)&
			!						)*3._pr/(4._pr*cos(theta3)**2+sin(theta3)**2)
			this%int_fact(i)%vals(x_i)= (	sum(fact1*x1*ctharr1**2,mask=.not.ieee_is_nan(x1) .and. mask_in)&
										+sum(fact2*x2*ctharr2**2,mask=.not.ieee_is_nan(x2) .and. mask_in)&
									)*3._pr
			
			this%int_fact(i)%vals(z_i)= (	sum(fact3*z1*stharr3**2,mask=.not.ieee_is_nan(z1) .and. mask_in)&
										+sum(fact4*z2*stharr3**2,mask=.not.ieee_is_nan(z2) .and. mask_in)&
									)*3._pr/2._pr
				
			if(mask==MASK_BLOCH)then
				this%int_fact(i)%vals(x_o)= (	sum(fact1*x1,mask=.not.ieee_is_nan(x1) .and. mask_out)&
											+sum(fact2*x2,mask=.not.ieee_is_nan(x2) .and. mask_out)&
										)
			
				this%int_fact(i)%vals(z_o)= (	sum(fact3*z1,mask=.not.ieee_is_nan(z1) .and. mask_out)&
											+sum(fact4*z2,mask=.not.ieee_is_nan(z2) .and. mask_out)&
										)
				end if
		end associate
		
		associate(	fact1=> pack(fact_ref*abs(deriv_TE),this%d(i,:)%mask(3)==mask),&
					fact2=> pack(fact_ref*abs(deriv_TE),this%d(i,:)%mask(4)==mask),&
					stharr1=> pack(stharr_ref,this%d(i,:)%mask(3)==mask),&
					stharr2=> pack(stharr_ref,this%d(i,:)%mask(4)==mask),&
					ctharr1=> pack(ctharr_ref,this%d(i,:)%mask(3)==mask),&
					ctharr2=> pack(ctharr_ref,this%d(i,:)%mask(4)==mask),&
					y1=> pack(this%d(i,:)%field(3),this%d(i,:)%mask(3)==mask),&
					y2=> pack(this%d(i,:)%field(4),this%d(i,:)%mask(4)==mask),&
					mask_in=>pack(mask_in_ref,this%d(i,:)%mask(3)==mask),&
					mask_out=>pack(mask_out_ref,this%d(i,:)%mask(4)==mask))
				
			this%int_fact(i)%vals(y_i)= (	sum(fact1*y1,mask=.not.ieee_is_nan(y1) .and. mask_in)&
										+sum(fact2*y2,mask=.not.ieee_is_nan(y2) .and. mask_in)&
									)
				
			if(mask==MASK_BLOCH)then
				this%int_fact(i)%vals(y_o)= (	sum(fact1*y1,mask=.not.ieee_is_nan(y1) .and. mask_out)&
											+sum(fact2*y2,mask=.not.ieee_is_nan(y2) .and. mask_out)&
										)
			end if
		end associate
	end associate
	
	end subroutine int_light_cone
	
	
	!Renormalize waveguide modes according to wtf
	module subroutine wg_renorm(this)
	class(job_purcell_t),intent(inout)		::	this
	integer									::	k
	
	if(.not. this%no_trim)then
		do k=1,6
			where(this%d%mask(k)==MASK_NO_MODE .or. this%d%mask(k)==MASK_DENY)this%d%field(k)= 0._pr
			where(this%d%mask(k)>MASK_NO_MODE)this%d%field(k)= this%d%field(k)/this%d%norm(k) !this%x1/
		end do
	else
		do k=1,6
			where(this%d%mask(k)>MASK_NO_WG)this%d%field(k)= this%d%u(k)
		end do
	end if
			
	end subroutine wg_renorm
	
	
	!Check if found is root
	module function check_kx_root(this, comp_ls, pt, dkx, polar, sign)
	class(job_purcell_t),&
		intent(in)&
		,target&
											::	this
	integer									::	check_kx_root
	type(computed_layer_t),&
		dimension(this%l_left-1:this%l_right+1),&
		intent(in)							::	comp_ls
	type(grid_point_t),intent(in)			::	pt
	complex(pr),intent(in)					::	dkx
	integer,intent(in)						::	polar, sign
	type(computed_layer_t),dimension(this%l_left-1&
		:this%l_right+1)					::	tmp_ls
	type(computed_layer_t)					::	left, right
	complex(pr),dimension(4)				::	box
	complex(pr)								::	kx,im_part
	integer									::	dir
	
	!print *,'Started'
	tmp_ls= comp_ls
	left= comp_ls(this%l_left-1)
	right= comp_ls(this%l_right+1)
	!MAKE BOX
	kx= pt%kx
	im_part= cmplx(0._pr,abs(this%max_im_kx))
	box= kx+[	abs(dkx)+im_part,&
				-abs(dkx)+im_part,&
				-abs(dkx)-im_part,&
				abs(dkx)-im_part]
	!CALL FUNCTION
	print *,kx,polar,sign
	select case (polar)
	case (POL_TE)
		dir= SQ_Y
	case (POL_TM)
		dir= SQ_X
	end select
	check_kx_root= box_zeros(f,box,4)
	
	contains
	
		function f(kx)
		complex(pr)								::	f
		complex(pr),intent(in)					::	kx
		type(matrix_t(2)),dimension(this%l_left-1:this%l_right+1)::matr
		type(matrix_t(2))						::	prod1, prod2
		integer									::	i
		type(grid_point_t)						::	tmp_point
		complex(pr)								::	r1,t1,r2,t2,val, pq0,pql
	
		tmp_point= pt; tmp_point%kx= kx
		do i= lbound(matr,1)+1,ubound(matr,1)-1
			select case (polar)
			case (POL_TE)
				call this%layers(i)%TrMatTE_sub(tmp_ls(i),tmp_point)
				matr(i)= tmp_ls(i)%TrMatTE
				!pq0= left%n *	sqrt(1._pr - tmp_point%kx**2/(tmp_point%k0*left%n)**2)
				!pql= right%n  * sqrt(1._pr - tmp_point%kx**2/(tmp_point%k0*right%n)**2)
			case (POL_TM)
				call this%layers(i)%TrMatTM_sub(tmp_ls(i),tmp_point)
				matr(i)= tmp_ls(i)%TrMatTM
				!pq0= sqrt(1._pr - tmp_point%kx**2/(tmp_point%k0*left%n)**2) / left%n
				!pql= sqrt(1._pr - tmp_point%kx**2/(tmp_point%k0*right%n)**2)/ right%n
			end select
		end do
		
		prod1= mat_prod(matr(lbound(matr,1)+1:ubound(matr,1)-1))
		
		call init_field_decay(prod1,layer=[left,right],point=tmp_point,direction=dir,u_p=val)
		
		!val= prod1%m(1,1)+prod1%m(2,1)/pq0+prod1%m(1,2)*pql+prod1%m(2,2)*pql/pq0
		
		f= val
		
		end function f
	
	end function check_kx_root
	
	end submodule aux_purcell