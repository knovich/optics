	module materials
	
	use types	
	use math
	use error_report
	
	implicit none
	
	private
	
	public :: assign_refr
	
	type:: external_refr_t
		character(MAX_NAME_LEN)	::	name
		real(pr),dimension(:,:),&
			allocatable			::	array
	end type external_refr_t
	
	type(external_refr_t),dimension(:),&
		allocatable							::	ext_mats
	
	contains

	
	!Assign refractivity function based on its name
	subroutine assign_refr(layer,name)
	type(un_layer_t),intent(inout)			::	layer
	character(*),intent(in)					::	name
	integer									::	index
	
	select case (name)
	case ("Bulk_exc")
		layer%n_func=> nBulk_Exc
	case ("Plasma")
		layer%n_func=> nPlasma
	case ("Al02GaAs")
		layer%n_func=> Al02GaAs
	case ("Al095GaAs")
		layer%n_func=> Al095GaAs
	case ("InAsQD")
		layer%n_func=> InAsQD
	case ("GaAs")
		layer%n_func=> GaAs
	case ("AlAs")
		layer%n_func=> AlAs
	case ("nAg")
		layer%n_func=> nAg
	case ("im_Ag")
		layer%n_func=> im_Ag
	case ("CBP")
		layer%n_func=> nCBP
	case ("AG_Dresden")
		layer%n_func=> nAG_Dresden
	case ("SiO2_Dresden")
		layer%n_func=> nSiO2_Dresden
	case ("TiO2_Dresden")
		layer%n_func=> nTiO2_Dresden
	case default
		index= get_index(trim(name))
		if(index == 0)then
			!Material not loaded
			inquire(file=trim(name))
			if(.true.)then
				!File with material present
				call load_material(trim(name),index)
				if(index == 0)then
					!Failed to load material
					goto 100
				end if
			else
				!No material file
				goto 100
			end if
		end if
		layer%sp_index= index
		layer%n_func=> nExt
	end select
	
	return
100	call report_error(ERR_MATERIAL,SEV_RETRY,"Faulty material '"//trim(name)//"' present.")
	
	end subroutine assign_refr
	
	
	!Get index of external material or 0 if it's not loaded
	function get_index(name)result(i)
	integer									::	i
	character(*),intent(in)					::	name
	
	do i=1,ubound(ext_mats,1)
		if(ext_mats(i)%name == name)then
			return
		end if
	end do
	i= 0
	
	end function get_index
	
	
	!Load external material from file
	subroutine load_material(name,index)
	character(*),intent(in)					::	name
	integer,intent(out)						::	index
	type(external_refr_t),dimension(:),&
		allocatable							::	tmp
	type(external_refr_t)					::	new_mat
	integer									::	ios, unit, length, i
	
	index= 0
	new_mat%name= name
	open(newunit=unit,file=name,iostat=ios)
	read(unit,*,iostat=ios)length
	allocate(new_mat%array(length,3))
	read(unit,*,iostat=ios)(new_mat%array(i,:),i=1,length)
	if(ios /= 0)goto 100
	if(allocated(ext_mats))then
		allocate(tmp(size(ext_mats)+1))
		tmp= [ext_mats,new_mat]
		deallocate(ext_mats)
		call move_alloc(tmp,ext_mats)
	else
		allocate(ext_mats(1))
		ext_mats= new_mat
	end if
	index= ubound(ext_mats,1)
100	return
	
	end subroutine load_material
	
	
	!External material
	function nExt(this,point,dedhw)result(n_func)
	complex(pr)								::	n_func
	class(un_layer_t)						::	this
	class(grid_point_t),intent(in)			::	point
	complex(pr),optional,intent(out)		::	dedhw
	real(pr)								::	lambda,&
												l1, l2, n1, n2, k1, k2, t
	integer									::	i, l
	
	lambda= 2*PI/point%k0
	if(ieee_is_nan(lambda))then
		lambda= ieee_value(1._pr, ieee_positive_inf)
	end if
	associate(mat_arr=> ext_mats(this%sp_index)%array)
		l= ubound(mat_arr,1)
		if(lambda <= mat_arr(1,1))then
			n_func= cmplx(mat_arr(1,2),mat_arr(1,3))
		elseif(lambda >= mat_arr(l,1))then
			n_func= cmplx(mat_arr(l,2),mat_arr(l,3))
		else
			i= 1
			do while(lambda > mat_arr(i,1))
			   i= i+1
			end do
			l1= mat_arr(i-1,1);l2=mat_arr(i,1)
			n1= mat_arr(i-1,2);n2=mat_arr(i,2)
			k1= mat_arr(i-1,3);k2=mat_arr(i,3)
			t= (lambda-l1)/(l2-l1)
			n_func= cmplx(n1+t*(n2-n1),k1+t*(k2-k1))
		end if
		if(present(dedhw))then
			if(lambda <= mat_arr(1,1) .or. lambda >= mat_arr(l,1))then
				dedhw= 0._pr
			else
				dedhw= cmplx(n2-n1,k2-k1)/(l2-l1)*(-lambda**2/EVNM)*2._pr*n_func
			end if
		end if
	end associate
	
	end function nExt
	
	
	!	p1 : exciton resonanse frequency
    !	p2 : transverse-longitudinal splitting /greek{w}_{LT}
    !	p3 : non-radiative damping /greek{G}
    !	p4 : background dielectric constant
	function nBulk_Exc(this,point,dedhw)result(n_func)
	complex(pr)								::	n_func
	class(un_layer_t)						::	this
	class(grid_point_t),intent(in)			::	point
	complex(pr),optional,intent(out)		::	dedhw
	complex(pr)								::	hw
	
	hw= point%k0*HBARC
	associate(p=> this%param)
		n_func= sqrt(p(4)*(1.,0.)*(1._pr+p(2)/(p(1)-hw-(0.,1.)*p(3))))
		if(present(dedhw))then
			dedhw= p(2)*p(4)/(p(1)-hw-(0.,1.)*p(3))**2
		end if
	end associate
	
	end function nBulk_Exc
	
	
	!e_background=p1
	!W_plasm=p2     !measured in ev
	!gamma=p3       !measured in ev
	function nPlasma(this,point,dedhw)result(n_func)
	complex(pr)								::	n_func
	class(un_layer_t)						::	this
	class(grid_point_t),intent(in)			::	point
	complex(pr),optional,intent(out)		::	dedhw
	complex(pr)								::	hw
	
	hw= point%k0*HBARC
	associate(p=> this%param)
		n_func= sqrt(p(1)-p(2)**2/(hw*(hw+(0.,1.)*p(3))))
		if(present(dedhw))then
			dedhw= p(2)**2*(2._pr*hw+(0.,1.)*p(3))/(hw*(hw+(0.,1.)*p(3)))**2
		end if
	end associate
	
	end function nPlasma
	
	
	
	
	!*************************************************************
    function nAg(this,point,dedhw)
    !       Indices these clement--Ca colle avec Johnson
    complex(pr)								::	nAg
	class(un_layer_t)						::	this
	class(grid_point_t),intent(in)			::	point
	complex(pr),optional,intent(out)		::	dedhw
    complex(pr)								::	lnm,epsAgR,epsAgI,im

	im  = (0.d0,1.d0)
    lnm=2*PI/point%k0
    epsAgR= -14.463366 + 0.13725*lnm - 3.91145*10.0**(-4.0)*lnm**2.0+3.31078*10.0**(-7.0)*lnm**3.0- &
        1.13012*10.0**(-10.0)*lnm**4.0
    epsAgI= 2.80407 - 0.01862*lnm + 4.73233*10.0**(-5.0)*lnm**2.0 - &
        4.93745*10.0**(-8.0)*lnm**3.0+1.83724*10.0**(-11.0)*lnm**4.0
    nAg= Sqrt(0.625*epsAgR + 1.0*im*epsAgI)
	if(present(dedhw))then
		dedhw= 0._pr
	end if

	end function nAg
	
	
	
	function im_Ag(this,point,dedhw)
    !       Indices these clement--Ca colle avec Johnson
    complex(pr)								::	im_Ag
	class(un_layer_t)						::	this
	class(grid_point_t),intent(in)			::	point
	complex(pr),optional,intent(out)		::	dedhw
    complex(pr)								::	lnm,epsAgR,epsAgI,im

	im  = (0.d0,1.d0)
    lnm=2*PI/point%k0
    epsAgR= -14.463366 + 0.13725*lnm - 3.91145*10.0**(-4.0)*lnm**2.0+3.31078*10.0**(-7.0)*lnm**3.0- &
        1.13012*10.0**(-10.0)*lnm**4.0
    epsAgI= 2.80407 - 0.01862*lnm + 4.73233*10.0**(-5.0)*lnm**2.0 - &
        4.93745*10.0**(-8.0)*lnm**3.0+1.83724*10.0**(-11.0)*lnm**4.0
    im_Ag= imag(Sqrt(0.625*epsAgR + 1.0*im*epsAgI))*im
	if(present(dedhw))then
		dedhw= 0._pr
	end if

	end function im_Ag

	
	
    function GaAs(this,point,dedhw)
    complex(pr) GaAs, hw
	class(un_layer_t)						::	this
	class(grid_point_t),intent(in)			::	point
	complex(pr),optional,intent(out)		::	dedhw
    complex(pr)								::	lnm,nnGaAs,kkGaAs,im

    Im  = (0.d0,1.d0)
    lnm=2*PI/point%k0
    
    nnGaAs = 40.76263-0.22568*lnm + 5.68996*10.0**(-4.0)*lnm**2.0 - 7.53668*10.0**(-7.0)*lnm**3.0+5.49968*10.0**(-10.0)*lnm**4.0- 2.09346*10.0**(-13.0)*lnm**5.0+3.24754*10.0**(-17.0)*lnm**6.0

    kkGaAs = 33.2812 - 0.22647*lnm + 6.42013*10.0**(-4.0)*lnm**2.0 - 9.58893*10.0**(-7.0)*lnm**3.0+7.91698*10.0**(-10.0)*lnm**4.0- 3.42088*10.0**(-13.0)*lnm**5.0+6.04523*10.0**(-17.0)*lnm**6.0
    GaAs = nnGaAs
	if(present(dedhw))then
		dedhw= 0._pr
	end if

	end function GaAs
	
	
	
    function AlAs(this,point,dedhw)
    complex(pr)								::	AlAs
	class(un_layer_t)						::	this
	class(grid_point_t),intent(in)			::	point
	complex(pr),optional,intent(out)		::	dedhw
    complex(pr)								::	lnm,nnAlAs,kkAlAs,im

    Im  = (0.d0,1.d0)
    lnm=2*PI/point%k0

    nnAlAs = 12.42573- 0.0507*lnm+1.16615*10.0**(-4.0)*lnm**2.0- &
        1.44956*10.0**(-7.0)*lnm**3.0+1.01636*10.0**(-10.0)*lnm**4.0- &
        3.79152*10.0**(-14.0)*lnm**5.0+5.86303*10.0**(-18.0)*lnm**6.0
    kkAlAs = 0
    AlAs = nnAlAs+0*Im*kkAlAs
	if(present(dedhw))then
		dedhw= 0._pr
	end if

	end function AlAs

	
	
	function InAsQD(this,point,dedhw)
    complex(pr) InAsQD
	class(un_layer_t)						::	this
	class(grid_point_t),intent(in)			::	point
	complex(pr),optional,intent(out)		::	dedhw
    complex(pr)								::	lnm,nnInGaAs,kkInGaAs,im

    Im  = (0._pr,1._pr)
    lnm=2*PI/point%k0

    nnInGaAs = 37.12937- 0.20494*lnm+5.40787*10.0**(-4.0)*lnm**2.0- &
        7.74589*10.0**(-7.0)*lnm**3.0+6.27306*10.0**(-10.0)*lnm**4.0- &
        2.70094*10.0**(-13.0)*lnm**5.0+4.80532*10.0**(-17.0)*lnm**6.0
    kkInGaAs = 176.288- 1.21835*lnm+0.00347*lnm**2.0- &
        5.18629*10.0**(-6.0)*lnm**3.0+4.29387*10.0**(-9.0)*lnm**4.0- &
        1.86628*10.0**(-12.0)*lnm**5.0+3.32802*10.0**(-16.0)*lnm**6.0
    InAsQD = nnInGaAs
	if(present(dedhw))then
		dedhw= 0._pr
	end if

	end function InAsQD

	
	
    function Al02GaAs(this,point,dedhw)
    complex(pr) Al02GaAs
	class(un_layer_t)						::	this
	class(grid_point_t),intent(in)			::	point
	complex(pr),optional,intent(out)		::	dedhw
    complex(pr)								::	lnm,nnGaAs,kkGaAs,im,&
												nnAlAs,kkAlAs,&
												cGaAs, cAlAs

    Im  = (0._pr,1._pr)
    lnm=2*PI/point%k0

    nnGaAs = 40.76263- 0.22568*lnm+5.68996*10.0**(-4.0)*lnm**2.0- &
        7.53668*10.0**(-7.0)*lnm**3.0+5.49968*10.0**(-10.0)*lnm**4.0- &
        2.09346*10.0**(-13.0)*lnm**5.0+3.24754*10.0**(-17.0)*lnm**6.0

    kkGaAs = 33.2812- 0.22647*lnm+6.42013*10.0**(-4.0)*lnm**2.0- &
        9.58893*10.0**(-7.0)*lnm**3.0+7.91698*10.0**(-10.0)*lnm**4.0- &
        3.42088*10.0**(-13.0)*lnm**5.0+6.04523*10.0**(-17.0)*lnm**6.0
    cGaAs = nnGaAs

    nnAlAs = 12.42573- 0.0507*lnm+1.16615*10.0**(-4.0)*lnm**2.0- &
        1.44956*10.0**(-7.0)*lnm**3.0+1.01636*10.0**(-10.0)*lnm**4.0- &
        3.79152*10.0**(-14.0)*lnm**5.0+5.86303*10.0**(-18.0)*lnm**6.0
    kkAlAs = 0
    cAlAs = nnAlAs

    Al02GaAs=0.2*cAlAs+0.8*cGaAs
	if(present(dedhw))then
		dedhw= 0._pr
	end if

	end function Al02GaAs

	
	
    function Al095GaAs(this,point,dedhw)
    complex(pr)								::	Al095GaAs
	class(un_layer_t)						::	this
	class(grid_point_t),intent(in)			::	point
	complex(pr),optional,intent(out)		::	dedhw
    complex(pr)								::	lnm,nnGaAs,kkGaAs,im,&
												nnAlAs,kkAlAs,&
												cGaAs, cAlAs

    Im  = (0._pr,1._pr)
    lnm=2*PI/point%k0

    nnGaAs = 40.76263- 0.22568*lnm+5.68996*10.0**(-4.0)*lnm**2.0- &
        7.53668*10.0**(-7.0)*lnm**3.0+5.49968*10.0**(-10.0)*lnm**4.0- &
        2.09346*10.0**(-13.0)*lnm**5.0+3.24754*10.0**(-17.0)*lnm**6.0

    kkGaAs = 33.2812- 0.22647*lnm+6.42013*10.0**(-4.0)*lnm**2.0- &
        9.58893*10.0**(-7.0)*lnm**3.0+7.91698*10.0**(-10.0)*lnm**4.0- &
        3.42088*10.0**(-13.0)*lnm**5.0+6.04523*10.0**(-17.0)*lnm**6.0
    cGaAs = nnGaAs

    nnAlAs = 12.42573- 0.0507*lnm+1.16615*10.0**(-4.0)*lnm**2.0- &
        1.44956*10.0**(-7.0)*lnm**3.0+1.01636*10.0**(-10.0)*lnm**4.0- &
        3.79152*10.0**(-14.0)*lnm**5.0+5.86303*10.0**(-18.0)*lnm**6.0
    kkAlAs = 0
    cAlAs = nnAlAs

    Al095GaAs=0.95*cAlAs+0.05*cGaAs
	if(present(dedhw))then
		dedhw= 0._pr
	end if

	end function Al095GaAs
	
	
    !range 1.0325..4.0623
	function nCBP(this,point,dedhw)
    complex(pr)								::	nCBP
	class(un_layer_t)						::	this
	class(grid_point_t),intent(in)			::	point
	complex(pr),optional,intent(out)		::	dedhw
    real(pr)								::	n,k,w
    real(pr)								::	y0,x0,A1,A2,t1,xc,H,w1,w2,Intercept,B1,B2,B3,B4,B5,B6,B7
    real(pr)								::	LOGx01,LOGx02,h1,h2,p,span,Section1,Section2,A,B,C

    w=real(point%k0*HBARC)

    !calculate n
    if(in_range(w,0._pr,3.02195_pr))then
        !exp_grow
        y0=1.63849_pr
        x0=0.7912_pr
        A1=0.07795_pr
        t1=1.95805_pr
        n=y0 + A1*exp((w-x0)/t1)
    elseif(in_range(w,3.02195_pr,3.14354_pr))then
        !bigaussian left
        y0=-1.17058_pr
        xc=3.14354_pr
        H=3.05229_pr
        w1=2.71508_pr
        w2=0.48927_pr
        n=y0 + H*exp(-(w - xc)**2/(2*w1**2))
    elseif(in_range(w,3.14354_pr,3.5913_pr))then
        !bigaussian right
        y0=-1.17058_pr
        xc=3.14354_pr
        H=3.05229_pr
        w1=2.71508_pr
        w2=0.48927_pr
        n=y0 + H*exp(-(w - xc)**2/(2*w2**2))
        if(n<0.8618_pr)then
            n=0.8618_pr
        endif
    elseif(in_range(w,3.5913_pr,ieee_value(1._pr, ieee_positive_inf)))then
        !poly6
        Intercept=7632960.58179870_pr
        B1=-12013933.7141127_pr
        B2=7877424.09854510_pr
        B3=-2754238.18552228_pr
        B4=541579.718026487_pr
        B5=-56786.4409253009_pr
        B6=2480.50314831196_pr
        n=Intercept + B1*w + B2*w**2 + B3*w**3 + B4*w**4 + B5*w**5 + B6*w**6
    endif

    !calculate k
    if(in_range(w,0._pr,2.065_pr))then
        !zero
        k=0.0
    elseif(in_range(w,2.065_pr,3.44167_pr))then
        !BiDose
        A1=0.00188_pr
        A2=1.81569_pr
        LOGx01=3.6981_pr
        LOGx02=3.33439_pr
        h1=3.7721_pr
        h2=8.2886_pr
        p=0.92221_pr
        span=A2 - A1;
        Section1=span*p/(1_pr+10_pr**((LOGx01-w)*h1));
        Section2=span*(1_pr-p)/(1_pr+10_pr**((LOGx02-w)*h2));
        k=A1 + Section1 +Section2;
    elseif(in_range(w,3.44167_pr,3.54_pr))then
        !parabola
        A=-266.70035_pr
        B=152.78905_pr
        C=-21.85373_pr
        k=A + B*w + C*w**2
    elseif(in_range(w,3.54_pr,3.5913_pr))then
        !lineal neg slope
        k=2.72424_pr-0.68181_pr*w
    elseif(in_range(w,3.5913_pr,3.99677_pr))then
        !poly7
        Intercept=-349170243.548719_pr
        B1=650160672.874805_pr
        B2=-518691956.939573_pr
        B3=229830806.058979_pr
        B4=-61085891.6246801_pr
        B5=9738853.59445574_pr
        B6=-862353.923109652_pr
        B7=32716.7431660919_pr
        k=Intercept + B1*w + B2*w**2 + B3*w**3 + B4*w**4 + B5*w**5 + B6*w**6 + B7*w**7
    elseif(in_range(w,3.99677_pr,ieee_value(1._pr, ieee_positive_inf)))then
        !lineal pos slope
        k=-0.63924_pr+0.27091_pr*w
    endif

    nCBP=cmplx(n,k)
	if(present(dedhw))then
		dedhw= 0._pr
	end if
	end function nCBP
	
	
    ! INTERVAL OF WORKING: [2.066, 4.065 eV]
	function nAG_Dresden(this,point,dedhw)
	complex(pr)								::	nAG_Dresden
	class(un_layer_t)						::	this
	class(grid_point_t),intent(in)			::	point
	complex(pr),optional,intent(out)		::	dedhw
    real(pr)								::	n_coef_a(9), n_coef_b(9),n_coef_c(9),w
    real(pr)								::	k_coef_a(9), k_coef_b(8)
    real(pr)								::	n,k,hw

	hw=real(point%k0*HBARC)
	
    n_coef_a(1) =      0.5794
    n_coef_b(1) =       4.314
    n_coef_c(1) =      0.2859
    n_coef_a(2) =      0.5866
    n_coef_b(2) =       4.073
    n_coef_c(2) =      0.1829
    n_coef_a(3) =       0.108
    n_coef_b(3) =      -3.631
    n_coef_c(3) =       5.609
    n_coef_a(4) =      0.4728
    n_coef_b(4) =       4.641
    n_coef_c(4) =      0.4403
    n_coef_a(5) =      0.8716
    n_coef_b(5) =       5.268
    n_coef_c(5) =      0.9354
    n_coef_a(6) =       16.25
    n_coef_b(6) =       12.72
    n_coef_c(6) =       3.783
    n_coef_a(7) =      0.3843
    n_coef_b(7) =       3.901
    n_coef_c(7) =      0.1148



    n = n_coef_a(1)*exp(-((hw-n_coef_b(1))/n_coef_c(1))**2) + n_coef_a(2)*exp(-((hw-n_coef_b(2))/n_coef_c(2))**2) + &
        n_coef_a(3)*exp(-((hw-n_coef_b(3))/n_coef_c(3))**2) + n_coef_a(4)*exp(-((hw-n_coef_b(4))/n_coef_c(4))**2) + &
        n_coef_a(5)*exp(-((hw-n_coef_b(5))/n_coef_c(5))**2) + n_coef_a(6)*exp(-((hw-n_coef_b(6))/n_coef_c(6))**2) + &
        n_coef_a(7)*exp(-((hw-n_coef_b(7))/n_coef_c(7))**2)

    k_coef_a(1) =       3.512
    k_coef_a(2) =       1.556
    k_coef_b(1) =       3.127
    k_coef_a(3) =     -0.3708
    k_coef_b(2) =       1.622
    k_coef_a(4) =     -0.8501
    k_coef_b(3) =       1.088
    k_coef_a(5) =      -0.609
    k_coef_b(4) =      0.3571
    k_coef_a(6) =     -0.5545
    k_coef_b(5) =    -0.03997
    k_coef_a(7) =     -0.2808
    k_coef_b(6) =    -0.04237
    k_coef_a(8) =    -0.08223
    k_coef_b(7) =     -0.1294
    k_coef_a(9) =    -0.07303
    k_coef_b(8) =    -0.06969
    w =       1.018


    k =              k_coef_a(1) + k_coef_a(2)*cos(hw*w) + k_coef_b(1)*sin(hw*w) + &
        k_coef_a(3)*cos(2*hw*w) + k_coef_b(2)*sin(2*hw*w) + k_coef_a(4)*cos(3*hw*w) + k_coef_b(3)*sin(3*hw*w) + &
        k_coef_a(5)*cos(4*hw*w) + k_coef_b(4)*sin(4*hw*w) + k_coef_a(6)*cos(5*hw*w) + k_coef_b(5)*sin(5*hw*w) + &
        k_coef_a(7)*cos(6*hw*w) + k_coef_b(6)*sin(6*hw*w) + k_coef_a(8)*cos(7*hw*w) + k_coef_b(7)*sin(7*hw*w) + &
        k_coef_a(9)*cos(8*hw*w) + k_coef_b(8)*sin(8*hw*w)


    nAG_Dresden=cmplx(n,k)
	
	if(present(dedhw))then
		dedhw= 0._pr
	end if

	end function nAG_Dresden
	
	
    ! INTERVAL OF WORKING: [2.066, 4.065 eV]
	function nTiO2_Dresden(this,point,dedhw)
	complex(pr)								::	nTiO2_Dresden
	class(un_layer_t)						::	this
	class(grid_point_t),intent(in)			::	point
	complex(pr),optional,intent(out)		::	dedhw
    real(pr)								::	An, Bn, Cn, Ak, Bk, Ck
    real(pr)								::	n,k,hw

	hw=real(point%k0*HBARC)

    An=2.09852011771642
    Bn=2.19766557252836E-12
    Cn=8.02564845866147E-03
    Ak=8.27641143459131E-08
    Bk=3.76656920493543
    Ck=1.46772585150214E-02

    N = An + 1E+6*Bn*hw**2/(2*3.14159265*197.32696)**2+1E+12*Cn*hw**4/(2*3.14159265*197.32696)**4
    K = Ak*exp(Bk*(1239.8*hw/(2*3.14159265*197.32696)-Ck))

    nTiO2_Dresden=cmplx(n,k)
	
	if(present(dedhw))then
		dedhw= 0._pr
	end if

	end function nTiO2_Dresden
	
	
    ! INTERVAL OF WORKING: [2.066, 4.065 eV]
	function nSiO2_Dresden(this,point,dedhw)
	complex(pr)								::	nSiO2_Dresden
	class(un_layer_t)						::	this
	class(grid_point_t),intent(in)			::	point
	complex(pr),optional,intent(out)		::	dedhw
    real(pr)								::	An, Bn, Cn, Ak, Bk, Ck
    real(pr)								::	n,k,hw

	hw=real(point%k0*HBARC)

    An=1.45169933854404
    Bn=2.65279582996002E-04
    Cn=3.62326886815615E-04
    Ak=2.35908509413402E-06
    Bk=5.39072203860777E-08
    Ck=6.24413667201945E-09

    N = An + 1E+6*Bn*hw**2/(2*3.14159265*197.32696)**2+1E+12*Cn*hw**4/(2*3.14159265*197.32696)**4
    K = Ak + 1E+6*Bk*hw**2/(2*3.14159265*197.32696)**2+1E+12*Ck*hw**4/(2*3.14159265*197.32696)**4

    nSiO2_Dresden=cmplx(n,k)
	
	if(present(dedhw))then
		dedhw= 0._pr
	end if

    end function nSiO2_Dresden
			
	
	end module materials