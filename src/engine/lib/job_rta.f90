	module job_rta
	!Reflection, transmission, absorption
	
	use types
	use keywords
	use math
	use error_report
	
	implicit none
	
	private
	
	public :: make_rta, job_rta_t
	
	type,extends(computation_t):: job_rta_t
		type(grid_point_t),&
			dimension(:,:),&
			allocatable			::	grid
		complex(pr),dimension(:,:),&
			allocatable			::	r1TE, t1TE, r1TM, t1TM
		real(pr),dimension(:,:),&
			allocatable			::	rpowTE, tpowTE, rpowTM, tpowTM
		integer					::	energy_unit	= UNIT_E_EV,&
									lat_unit	= UNIT_LAT_KX_NM
		real(pr),dimension(:),&
			allocatable			::	angle_store, wl_store
		logical					::	calculate_power = .true.
		logical					::	no_absorption	= .false.
	contains
		procedure				::	init=>		init_rta
		procedure				::	driver=>	driver_rta
		procedure				::	output=>	output_rta
	end type job_rta_t
	
	
	type:: output_rta_t
		real(po)				::	energy	=0.,&
									lateral	=0.
		complex(po)				::	rTE	=0.,&
									rTM	=0.,&
									tTE	=0.,&
									tTM	=0.
		real(po)				::	rpTE =0., rpTM =0., tpTE =0., tpTM =0.
	end type output_rta_t
	
			
	contains
	
	
	!Init
	function init_rta(this)result(status)
	integer									::	status
	class(job_rta_t),intent(inout)			::	this
	integer									::	en_gr_pts,&
												kx_gr_pts,&
												pos,&
												ios
	character(MAX_NAME_LEN)					::	unit
	real(pr)								::	tmp_en_lo, tmp_en_hi,&
												tmp_ang_lo, tmp_ang_hi,&
												tmp_kx_lo, tmp_kx_hi
	complex(pr),dimension(:),allocatable	::	n0
	integer									::	i
	
	en_gr_pts= 0; kx_gr_pts= 0; pos= 0; ios= 0
	unit= ""
	tmp_en_lo= 0.; tmp_en_hi= 0.; tmp_ang_lo= 0.; tmp_ang_hi= 0.; tmp_kx_lo= 0.; tmp_kx_hi= 0.
	associate(tkws=> this%keywords)
		pos= find_keyword("energy",tkws)
		energy:if(pos /= 0)then
			associate (kw=> tkws(pos))
				select case (kw%key)
				case ("energy")
					en_gr_pts= 1
					read(kw%value,*,iostat=ios) unit
					if(ios /= 0 .or. verify(trim(unit),NUMCHARS) == 0)then
						!No unit specified, assuming eV
						read(kw%value,*,iostat=ios) tmp_en_lo
						tmp_en_hi= tmp_en_lo
					else
						select case (unit)
						case ("eV")
							this%energy_unit= UNIT_E_EV
						case ("nm")
							this%energy_unit= UNIT_E_NM
						case default
							go to 100
						end select
						read(kw%value,*,iostat=ios) unit, tmp_en_lo
						tmp_en_hi= tmp_en_lo
					end if
				case ("energy_grid")
					read(kw%value,*,iostat=ios) unit
					if(ios /= 0 .or. verify(trim(unit),NUMCHARS) == 0)then
						!No unit specified, assuming eV
						read(kw%value,*,iostat=ios) tmp_en_lo, tmp_en_hi, en_gr_pts
					else
						select case (unit)
						case ("eV")
							this%energy_unit= UNIT_E_EV
						case ("nm")
							this%energy_unit= UNIT_E_NM
						case default
							go to 100
						end select
						read(kw%value,*,iostat=ios) unit, tmp_en_lo, tmp_en_hi, en_gr_pts
					end if
				case default
					go to 100
				end select
				if(ios /= 0 .or. en_gr_pts < 1)then
					!Something went wrong during value reading
					call report_error(ERR_NOENERGY,SEV_RETRY,&
						"Incorrect energy specification for "//trim(this%job_name))
					go to 100
				end if
			end associate
		else
			call report_error(ERR_NOENERGY,SEV_RETRY,"No energy specified for "//trim(this%job_name))
			go to 100
		end if energy
		
		pos= find_keyword("kx",tkws)
		lateral:if(pos /= 0)then
			associate (kw=> tkws(pos))
				select case (kw%key)
				case ("angle")
					this%lat_unit= UNIT_LAT_DEGREE
					kx_gr_pts= 1
					read(kw%value,*,iostat=ios) tmp_ang_lo
					tmp_ang_hi= tmp_ang_lo
				case ("angle_grid")
					this%lat_unit= UNIT_LAT_DEGREE
					read(kw%value,*,iostat=ios) tmp_ang_lo, tmp_ang_hi, kx_gr_pts
				case ("kx")
					this%lat_unit= UNIT_LAT_KX_NM
					kx_gr_pts= 1
					read(kw%value,*,iostat=ios) tmp_kx_lo
					tmp_kx_hi= tmp_kx_lo
				case ("kx_grid")
					this%lat_unit= UNIT_LAT_KX_NM
					read(kw%value,*,iostat=ios) tmp_kx_lo, tmp_kx_hi, kx_gr_pts
				case default
					go to 100
				end select
				if(ios /= 0 .or. kx_gr_pts < 1)then
					!Something went wrong during value reading
					call report_error(ERR_NOLAT,SEV_RETRY,&
						"Incorrect lateral dimension specification for "//trim(this%job_name))
					go to 100
				end if
			end associate
		else
			call report_error(ERR_NOLAT,SEV_RETRY,&
				"No lateral dimension specified for "//trim(this%job_name))
			go to 100
		end if lateral
		
		pos= find_keyword("no_absorption",tkws)
		if(pos /= 0)then
			read(tkws(pos)%value,*,iostat=ios) this%no_absorption
			if(ios /= 0)then
				!Something went wrong during value reading
				call report_error(ERR_OTHER,SEV_RETRY,&
					"Incorrect absorption specification for "//trim(this%job_name))
				go to 100
			end if
		end if

	end associate
	
	allocate(this%grid(en_gr_pts,kx_gr_pts),n0(en_gr_pts))
	associate(tg=> this%grid)
		!Assign grid values
		select case (this%energy_unit)
		case (UNIT_E_EV)
			tg%k0= spread(interpolate_grid(tmp_en_lo/HBARC,tmp_en_hi/HBARC,en_gr_pts), 2, kx_gr_pts)
		case (UNIT_E_NM)
			allocate(this%wl_store(en_gr_pts))
			this%wl_store= interpolate_grid(tmp_en_lo,tmp_en_hi,en_gr_pts)
			tg%k0= spread(2.*PI/this%wl_store, 2, kx_gr_pts)
		end select
		select case (this%lat_unit)
		case (UNIT_LAT_KX_NM)
			tg%kx= spread(interpolate_grid(tmp_kx_lo,tmp_kx_hi,kx_gr_pts), 1, en_gr_pts)
		case (UNIT_LAT_DEGREE)
			!Need to calculate kx based on the first layer refractivity
			select case (this%layers(0)%is_var_refr)
			case (.true.)
				n0= [(this%layers(0)%n_func(tg(i,1)),i= 1,en_gr_pts)]
			case (.false.)
				n0= this%layers(0)%const_n
			end select
			allocate(this%angle_store(kx_gr_pts))
			this%angle_store= interpolate_grid(tmp_ang_lo,tmp_ang_hi,kx_gr_pts)
			tg%kx= get_kx(tg%k0,spread(n0, 2, kx_gr_pts),spread(this%angle_store,1,en_gr_pts))
		end select
		!allocate r,t,a
		allocate(this%r1TE(ubound(tg,1),ubound(tg,2)))
		allocate(this%t1TE,this%r1TM,this%t1TM, mold=this%r1TE)
		if(this%calculate_power)then
			allocate(this%rpowTE(ubound(tg,1),ubound(tg,2)))
			allocate(this%tpowTE,this%rpowTM,this%tpowTM, mold=this%rpowTE)
		end if
	end associate
	
	status= STATUS_OK
	return
	
100	status= STATUS_ERROR
	return
	
	end function init_rta
	
	
	!Main process
	subroutine driver_rta(this)
	class(job_rta_t),intent(inout)			::	this
	type(computed_layer_t),&
		dimension(&
		lbound(this%layers,1)&
		:ubound(this%layers,1),&
		ubound(this%grid,2))				::	l_row
	type(matrix_t(2)),&
		dimension(ubound(this%grid,2))		::	prod_TE, prod_TM
	integer									::	i,j,k,&
												lbtl, ubtl
	
	lbtl= lbound(this%layers,1); ubtl= ubound(this%layers,1)
	associate(tl=> this%layers, tg=> this%grid)
		do i= 1,ubound(tg,1)
		!Cycle through energy
			!Assign refractive indices
			do j= lbtl,ubtl
				if(tl(j)%is_var_refr)then
					l_row(j,:)%n= tl(j)%n_func(tg(i,1))
				else
					l_row(j,:)%n= tl(j)%const_n
				end if
				if(this%no_absorption)then
					l_row(j,:)%n= sqrt(cmplx(real(l_row(j,:)%n**2),0._pr))
				end if
			end do
			!Assign transfer matrices
			do j= 1,ubound(tg,2)
			!Get product of TMs
				do k= lbtl+1,ubtl-1
					!Cycle through layers
					!Cycle through kx
					call tl(k)%TrMatTE_sub(l_row(k,j),tg(i,j))
					call tl(k)%TrMatTM_sub(l_row(k,j),tg(i,j))
				end do
				prod_TE(j)= mat_prod(l_row(lbtl+1:ubtl-1,j)%TrMatTE)
				call rta(prod_TE(j),tg(i,j),l_row(lbtl,j),l_row(ubtl,j),POL_TE,&
						this%r1TE(i,j),this%t1TE(i,j),this%rpowTE(i,j),this%tpowTE(i,j))
				prod_TM(j)= mat_prod(l_row(lbtl+1:ubtl-1,j)%TrMatTM)
				call rta(prod_TM(j),tg(i,j),l_row(lbtl,j),l_row(ubtl,j),POL_TM,&
						this%r1TM(i,j),this%t1TM(i,j),this%rpowTM(i,j),this%tpowTM(i,j))
			end do
		end do
	end associate		
	
	end subroutine driver_rta
	
	
	!Writing all to file
	subroutine output_rta(this)
	class(job_rta_t),intent(in)			::	this
	type(output_rta_t),dimension&
		(lbound(this%grid,1):&
		ubound(this%grid,1),&
		lbound(this%grid,2):&
		ubound(this%grid,2))				::	out_arr
	integer									::	unit, rl, i, j, index
	
	select case (this%energy_unit)
	case (UNIT_E_EV)
		out_arr%energy= this%grid%k0*HBARC
	case (UNIT_E_NM)
		out_arr%energy= spread(this%wl_store,2,size(this%grid,2))
	end select
	select case (this%lat_unit)
	case (UNIT_LAT_KX_NM)
		out_arr%lateral= this%grid%kx
	case (UNIT_LAT_DEGREE)
		out_arr%lateral= spread(this%angle_store,1,size(this%grid,1))
	end select
	out_arr%rTE= this%r1TE; out_arr%rTM= this%r1TM; out_arr%tTE= this%t1TE; out_arr%tTM= this%t1TM
	out_arr%rpTE= this%rpowTE; out_arr%rpTM= this%rpowTM
	out_arr%tpTE= this%tpowTE; out_arr%tpTM= this%tpowTM
	inquire(iolength=rl)output_rta_t()
	open(newunit=unit,file=trim(this%job_name)//".out",form="unformatted",status="replace",&
		access="direct",recl=rl)
	index= 1
	write(unit,rec=index)TASK_RTA,rl,size(out_arr,1),size(out_arr,2)
	do i=lbound(out_arr,1),ubound(out_arr,1)
		do j=lbound(out_arr,2),ubound(out_arr,2)
			index=index+1
			write(unit,rec=index)out_arr(i,j)
		end do
	end do
	close(unit)
	
	end subroutine output_rta
	
	
	!Return a pointer to an RTA task
	function make_rta(job)result(res)
	type(job_rta_t),pointer					::	res
	type(job_t),intent(in)					::	job
	
	allocate(res)
	res%job_t= job
	if(res%init() /= STATUS_OK)then
		deallocate(res)
		nullify(res)
	end if
	
	end function make_rta
	
	end module job_rta