	module keywords
	!Functions for working with keyword lists

	use types
	
	implicit none

	private :: find_string, generic_key
	
	contains


	!Search for string in string array, return first occurrence or the pre-first index
	pure function find_string(kw, arr)
	integer									::	find_string, j
	character(*),intent(in)					::	kw
	character(*),dimension(:),intent(in)	::	arr
	logical,dimension(ubound(arr,1))		::	mask

	
	find_string= 0
	
	if(size(arr) > 0)then
		mask= (arr == kw)
		do j=1,size(arr)
			if(mask(j))then
				find_string= j
				exit
			end if
		end do
	end if

	end function find_string


	!Change mutually exclusive keys to their generic names
	elemental function generic_key(key)
	character(MAX_NAME_LEN)					::	generic_key
	character(*),intent(in)					::	key
	integer									::	i

	do i= 1,size(GENERIC_KEYS,1)
		if(find_string(key,GENERIC_KEYS(i,:)) > 0)then
			generic_key= GENERIC_KEYS(i,1)
			return
		end if
	end do
	generic_key= key

	end function generic_key


	!Find keyword or its equivalents
	pure function find_keyword(kw, arr)
	integer									::	find_keyword
	character(*),intent(in)					::	kw
	type(keyword_t),dimension(:),intent(in)	::	arr
	character(MAX_NAME_LEN),&
		dimension(ubound(arr,1))			::	arr2

	if(ubound(arr,1) > 0)then
		arr2= generic_key(arr(:)%key)		!Process exclusions
		find_keyword= find_string(generic_key(kw), arr2)
	else
		find_keyword= 0
	end if
		
	end function find_keyword


	!Add keyword to an array
	subroutine push_keyword(kw, arr)
	type(keyword_t),intent(in)				::	kw
	type(keyword_t),dimension(:),&
		allocatable,intent(inout)			::	arr
	type(keyword_t),dimension(:),&
		allocatable							::	tmp

	if(allocated(arr))then
		allocate(tmp(size(arr,1)+1))
		tmp= [arr,kw]
		deallocate(arr)
		call move_alloc(tmp,arr)
	else
		!allocate(arr(1))
		arr= [kw]
	end if
	
	end subroutine push_keyword
	
	
	!Translate task name into task number
	pure function get_task(name)
	integer									::	get_task
	character(*),intent(in)					::	name
	
	get_task= find_string(name,TASKS_KEYS)
	
	end function get_task
	

	end module keywords