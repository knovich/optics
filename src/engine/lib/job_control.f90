	module job_control
	!Functions for creating jobs lists which will be later processed by tasks launcher
	
	use types
	use keywords
	use error_report
	use materials
	use math
	use job_rta
	use job_profile
	use job_purcell
	use job_disp
	
	implicit none
	
	private
	
	public :: populate_jobs, gen_job
	

	contains

	
	!Fill jobs array using parsed input
	subroutine populate_jobs(blocks, jobs)
	type(input_block_t),&
		dimension(:),intent(in)				::	blocks
	type(job_t),dimension(:),&
		allocatable,intent(out)				::	jobs
	type(job_t),dimension(:),&
		allocatable							::	tmp
	integer									::	nof_jobs
	integer									::	i, k, pos, lbnd, offs
	type(keyword_t)							::	kw
	logical									::	has_common
	
	has_common= (blocks(1)%block_name == "common")
	if(has_common)then
		lbnd= 0
		offs= 1
		nof_jobs= ubound(blocks,1)-1
	else
		lbnd= 1
		offs= 0
		nof_jobs= ubound(blocks,1)
	end if
	allocate(jobs(lbnd:nof_jobs))
	do k= lbnd,nof_jobs
		jobs(k)%job_name= blocks(k+offs)%block_name
		if(allocated(blocks(k+offs)%keywords))then
			call push_keyword(blocks(k+offs)%keywords(1), jobs(k)%keywords)
			do i= 2,size(blocks(k+offs)%keywords)
				kw= blocks(k+offs)%keywords(i)
				pos= find_keyword(kw%key, jobs(k)%keywords)
				if(pos < 1)then
					call push_keyword(kw, jobs(k)%keywords)
				else
					call report_error(ERR_DUPLICATE_KEYWORD,SEV_WARN,&
						"Duplicate or equivalent keyword '"//trim(kw%key)//&
						"' in block "//trim(blocks(k+offs)%block_name)//&
						", will use value "//trim(kw%value))
					jobs(k)%keywords(pos)= kw
				end if
			end do
		end if
	end do
	if(has_common .and. allocated(blocks(1)%keywords))then
		!there are some common keywords
		do i= 1,size(jobs(0)%keywords)
			kw= jobs(0)%keywords(i)
			do k= 1,nof_jobs
				pos= find_keyword(kw%key, jobs(k)%keywords)
				if(pos < 1)then
					call push_keyword(kw, jobs(k)%keywords)
				else
					call report_error(ERR_REDEFINED_KEYWORD,SEV_MSG,&
						"Redefined keyword '"//trim(kw%key)//&
						"' in job "//trim(jobs(k)%job_name)//&
						", will use value "//trim(jobs(k)%keywords(pos)%value))
				end if
			end do
		end do
		allocate(tmp(ubound(jobs,1)))
		tmp= [jobs(1:)]
		deallocate(jobs)
		call move_alloc(tmp,jobs)
	end if
	call check_empty(jobs)
	call check_active(jobs)
	call check_structures(jobs)
	call check_tasks(jobs)

	end subroutine populate_jobs

	
	!Process and deactivate jobs with no keywords
	impure elemental subroutine check_empty(job)
	type(job_t),intent(inout)				::	job
	
	if(.not. allocated(job%keywords))then
		call push_keyword(keyword_t("active","no"),job%keywords)
		call report_error(ERR_EMPTY_JOB,SEV_MSG,"Empty job "//trim(job%job_name)//". It will be inactive.")
	end if
	end subroutine check_empty

	
	!Set activity status for jobs
	impure elemental subroutine check_active(job)
	type(job_t),intent(inout)				::	job
	integer									::	pos
	
	pos= find_keyword("active",job%keywords)
	if(pos == 0)then
		job%active= .true.
		call push_keyword(keyword_t("active","yes"),job%keywords)
	else if(trim(job%keywords(pos)%value) == "no")then
		job%active= .false.
	else if(trim(job%keywords(pos)%value) /= "yes")then
		job%active= .false.
		print *,job%keywords(pos)%value
		call report_error(ERR_VALUE,SEV_WARN,&
			"Incorrect activity value for job "//trim(job%job_name)//". It will be inactive.")
	end if	
		
	end subroutine check_active
	
	
	!Check if structure files are present
	impure elemental subroutine check_structures(job)
	type(job_t),intent(inout)				::	job
	integer									::	pos, st
	logical									::	is_str
	character(MAX_NAME_LEN)					::	name

	if(job%active)then
		pos= find_keyword("structure",job%keywords)
		if(pos == 0)then
			is_str= .false.
		else
			name= job%keywords(pos)%value
			inquire(file= name, exist= is_str)
		end if
		if(.not. is_str)then
			job%active= .false.
			call report_error(ERR_INCONSISTENT_TASK,SEV_RETRY,&
				"Absent structure file. Job name: "//trim(job%job_name)//". Job will be inactive.")
		else
			call read_struct(name,job%layers,st,job)
			if(st /= STATUS_OK)then
				job%active= .false.
				call report_error(ERR_STRUCTURE,SEV_RETRY,&
					"Bad structure "//trim(name)//". Job "//trim(job%job_name)//" inactivated.")
			end if
		end if
	end if
	
	end subroutine check_structures

	
	!Check if tasks are present
	impure elemental subroutine check_tasks(job)
	type(job_t),intent(inout)				::	job
	integer									::	pos
	logical									::	no_task
	
	no_task= .false.
	if(job%active)then
		pos= find_keyword("task",job%keywords)
		if(pos == 0)then
			no_task= .true.
		else
			job%task= get_task(job%keywords(pos)%value)
			if(job%task == TASK_UNASSIGNED)then
				no_task= .true.
			end if
		end if
		if(no_task)then
			job%active= .false.
			call report_error(ERR_INCONSISTENT_TASK,SEV_RETRY,&
				"Incorrect or absent task value for job "//trim(job%job_name)//". It will be inactive.")
		end if
	end if
	
	end subroutine check_tasks
	
	
	!Read structure from file
	subroutine read_struct(name,struct,st,job)
	type(un_layer_t),&
		dimension(:),allocatable,&
		intent(out)							::	struct
	character(*),intent(in)					::	name
	integer,intent(out)						::	st
	type(job_t),intent(in)					::	job
	character(MAX_BUF_LEN)					::	line, tmp, l_name, lparam_name
	character(MAX_BUF_LEN),dimension(:),&
		allocatable							::	lines
	integer									::	unit, ios, nofl, i, l_type, pos

	st= STATUS_OK
	!TODO
	line=""; tmp=""; l_name=""
	ios= 0; nofl= 0; l_type=0
	open(newunit=unit,file=name,status='old',action='read',iostat=ios)
	read(unit,'(a)',iostat=ios) tmp
	read(unit,*,iostat=ios) nofl
	allocate(struct(0:nofl+1))
	read(unit,'(a)',iostat=ios) line
	read(line,*,iostat=ios) l_type,tmp
	if(tmp /= "first")then
		go to 100
	else
		select case (l_type)
		case (LT_CONST_REFR)
			read(line,*,iostat=ios) l_type, tmp, struct(0)%const_n
		case (LT_VAR_REFR)
			struct(0)%is_var_refr= .true.
			read(line,*,iostat=ios) l_type, tmp, l_name, struct(0)%param
			call assign_refr(struct(0),l_name)
		case default
			go to 100
		end select
		if (ios /= 0)then
			call report_error(ERR_OTHER,SEV_RETRY,"Incorrect layer spec "//trim(line))
			go to 100
		end if
		call assign_TrMat(struct(0),l_type)
	end if
	allocate(lines(nofl))
	do i=1,nofl
		read(unit,'(a)',iostat=ios) lines(i)
		read(lines(i),*,iostat=ios) l_type
		if(l_type == LT_PARAM)then
			read(lines(i),*,iostat=ios) l_type, lparam_name
			pos= find_keyword("layer_"//trim(lparam_name),job%keywords)
			if (pos == 0 .or. ios /= 0)then
				call report_error(ERR_OTHER,SEV_RETRY,"Incorrect parametric layer "//trim(lparam_name))
				go to 100
			end if
			lines(i)= job%keywords(pos)%value
			read(lines(i),*,iostat=ios) l_type
		end if
		select case (l_type)
		case (LT_CONST_REFR)
			read(lines(i),*,iostat=ios) l_type, struct(i)%thick, struct(i)%const_n
		case (LT_VAR_REFR)
			struct(i)%is_var_refr= .true.
			read(lines(i),*,iostat=ios) l_type, struct(i)%thick, l_name, struct(i)%param
			call assign_refr(struct(i),l_name)
		case (LT_QW)
			read(lines(i),*,iostat=ios) l_type, tmp, struct(i)%param_qw
			struct(i)%thick= 0._pr
			if(tmp /= "qw")then
				go to 100
			else if(struct(i-1)%is_var_refr)then
				struct(i)%is_var_refr= .true.
				struct(i)%n_func=> struct(i-1)%n_func
				struct(i)%param= struct(i-1)%param
			else
				struct(i)%const_n= struct(i-1)%const_n
			end if
		case default
			go to 100
		end select
		if (ios /= 0)then
			call report_error(ERR_OTHER,SEV_RETRY,"Incorrect layer spec "//trim(lines(i)))
			go to 100
		end if
		call assign_TrMat(struct(i),l_type)
	end do
	read(unit,'(a)',iostat=ios) line
	read(line,*,iostat=ios) l_type,tmp
	if(tmp /= "last")then
		go to 100
	else
		select case (l_type)
		case (LT_CONST_REFR)
			read(line,*,iostat=ios) l_type, tmp, struct(nofl+1)%const_n
		case (LT_VAR_REFR)
			struct(nofl+1)%is_var_refr= .true.
			read(line,*,iostat=ios) l_type, tmp, l_name, struct(nofl+1)%param
			call assign_refr(struct(nofl+1),l_name)
		case default
			go to 100
		end select
		if (ios /= 0)then
			call report_error(ERR_OTHER,SEV_RETRY,"Incorrect layer spec "//trim(line))
			go to 100
		end if
		call assign_TrMat(struct(nofl+1),l_type)
	end if
	
	do i=lbound(struct,1),ubound(struct,1)
		if(struct(i)%is_var_refr .and. .not. associated(struct(i)%n_func))then
			call report_error(ERR_MATERIAL,SEV_RETRY,"Incorrect material.")
			go to 100
		end if
	end do
	if(ios == 0)then
		st= STATUS_OK
		return
	end if		
100	deallocate(struct)
	st= STATUS_ERROR
	return
	
	end subroutine read_struct
	
	
	!Generate specific job
	function gen_job(job)
	class(computation_t),pointer			::	gen_job
	type(job_t),intent(in)					::	job
	
	select case (job%task)
	case (TASK_RTA)
		gen_job=> make_rta(job)
	case (TASK_PROFILE)
		gen_job=> make_profile(job)
	case (TASK_PURCELL)
		gen_job=> make_purcell(job)
	case (TASK_DISP)
		gen_job=> make_disp(job)
	end select
	
	end function gen_job
	
	end module job_control