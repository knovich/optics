	module input_parser
	!This module parses the input file with keywords and creates raw jobs list
	!which is later to be processed to generate handsome tasks
	
	use types
	use keywords
	use error_report
	
	implicit none
	
	private
	!Constants for input file sections
	integer,parameter::STATE_OUT= 0
	integer,parameter::STATE_COMMON= 1
	integer,parameter::STATE_JOB= 2
	
	public::parse_input
	
	
	contains
	
	
	!Main parser subprogram, returns parsing status (OK, RETRY, or ERROR)
	subroutine parse_input(infile, input_blocks, parse_status)
	character(*),intent(in)					::	infile
	type(input_block_t),dimension(:),&
		allocatable,intent(out)				::	input_blocks	!Raw jobs list (as input from file)
	integer,intent(out)						::	parse_status
	integer									::	unit
	character(MAX_BUF_LEN)					::	buffer,&
												s_line
	logical									::	common_flag,&
												jobs_flag,&
												eof
	integer									::	state,&
												line, ios,&
												com_pos
	integer									::	current_block
	
	buffer= ""
	common_flag= .false.; jobs_flag= .false.; eof= .false.
	state= STATE_OUT
	line= 0; ios= 0; com_pos= 0; current_block= 0
	parse_status= STATUS_OK
	open(newunit=unit,file=infile,status='old',action='read',iostat=ios)
	if(ios /= 0)then
		call report_error(ERR_NO_FILE, SEV_RETRY,"Cannot access file "//infile)
		call ask_retry()
		parse_status= STATUS_RETRY
		return
	end if
	do
		read(unit,'(A)',iostat= ios) buffer; call remove_tabs(buffer); buffer= adjustl(buffer)
		line= line+1; write(s_line,*)line
		if(index(buffer,"%terminate") == 1 .or. ios < 0)then
			!End of file (explicit or implicit)
			if(.not. state == STATE_OUT)then
				call report_error(ERR_EOF,SEV_RETRY,"Unexpected end of file")
				parse_status= STATUS_RETRY; exit
			else
				eof= .true.
				exit
			end if
		end if
		!Trim commentary
		com_pos= scan(buffer,"#")
		if(com_pos == 0) com_pos= len(buffer)+1
		buffer= buffer(1:com_pos-1)
		if(len_trim(buffer) == 0)then
			!Blank line or commentary
			cycle
		else
			select case (state)
			case (STATE_OUT)
				!Accept %common or %job
				if(index(buffer,"%common ") == 1)then
					if(.not. (common_flag .or. jobs_flag))then
						!No previous occurrences of any blocks
						common_flag= .true.
						state= STATE_COMMON
						call add_input_block(input_blocks,"common")
					else
						call report_error(ERR_INCORRECT_COMMON_BLOCK,SEV_RETRY,&
							"Incorrect common block in line "//s_line)
						parse_status= STATUS_RETRY; exit
					end if
				else if(index(buffer,"%job ") == 1)then
					jobs_flag= .true.
					state= STATE_JOB
					call add_input_block(input_blocks,buffer(len("%job "):))
				else if(buffer(1:1) == "%")then
					call report_error(ERR_UNKNOWN_SECTION,SEV_RETRY,&
						"Unexpected block tocken in line "//s_line)
					parse_status= STATUS_RETRY; exit
				else
					call report_error(ERR_UNKNOWN_KEYWORD, SEV_WARN,&
						"Unknown keyword "//trim(buffer)//" in line "//s_line)
				end if
			
			case (STATE_COMMON)
				!Accept keywords or %end common
				if(index(buffer,"%end common ") == 1)then
					state= STATE_OUT
				else if(buffer(1:1) == "%")then
					call report_error(ERR_UNMATCHED_SECTION,SEV_RETRY,&
						"Unexpected block tocken in line "//s_line)
					parse_status= STATUS_RETRY; exit
				else
					call add_keyword(buffer)
				end if
				
			case (STATE_JOB)
				!Accept keywords or %end job
				if(index(buffer,"%end job ") == 1)then
					state= STATE_OUT
				else if(buffer(1:1) == "%")then
					call report_error(ERR_UNMATCHED_SECTION,SEV_RETRY,&
						"Unexpected block tocken in line "//s_line)
					parse_status= STATUS_RETRY; exit
				else
					call add_keyword(buffer)
				end if
				
			end select
		end if
	end do
	
	if(.not. jobs_flag .and. common_flag)then
		!No jobs blocks found, but a common block present
		call report_error(ERR_NO_JOBS,SEV_RETRY,message="No jobs blocks present on input")
		parse_status= STATUS_RETRY
	end if
	
	if(parse_status == STATUS_RETRY)then
		call ask_retry()
	end if
	
	if(.not. allocated(input_blocks))then
		call report_error(ERR_NO_JOBS,SEV_RETRY,message="No jobs blocks present on input")		
		parse_status= STATUS_EMPTY
	end if

	close(unit)
	
	contains
	
		!Allocate blocks array and add a new block with corresponding name
		subroutine add_input_block(input_blocks,name)
		character(*),intent(in)					::	name
		type(input_block_t),&
			dimension(:),allocatable,&
			intent(inout)						::	input_blocks
		type(input_block_t),&
			dimension(:),allocatable			::	tmp
		integer									::	nof_jobs

		nof_jobs= 1
		if(allocated(input_blocks))then
			allocate(tmp(size(input_blocks)+1))
			tmp= [input_blocks,input_block_t(block_name= name)]
			deallocate(input_blocks)
			call move_alloc(tmp,input_blocks)
		else
			allocate(input_blocks(1))
			input_blocks= [input_block_t(block_name= name)]
		end if
		if(name /= "common")then
			nof_jobs= ubound(input_blocks,1)
			if(common_flag)then
				nof_jobs= nof_jobs-1
			end if
			write(input_blocks(ubound(input_blocks,1))%block_name,'(a,i3.3,a,a)')&
				"job",nof_jobs,"_",trim(adjustl(name))
		end if
		current_block= ubound(input_blocks,1)
		
		end subroutine add_input_block
		
		
		!Split key and value and add to current block's keywords
		subroutine add_keyword(buffer)
		character(MAX_BUF_LEN),intent(in)		::	buffer
		integer									::	colon_pos
		type(keyword_t)							::	keyw
		
		colon_pos= scan(buffer,":")
		if(colon_pos == 0)then
			call report_error(ERR_UNKNOWN_KEYWORD, SEV_WARN,&
				"Unknown keyword "//trim(buffer)//" in line "//s_line)
		else
			keyw= keyword_t(adjustl(buffer(1:colon_pos-1)),adjustl(buffer(colon_pos+1:)))
			call push_keyword(keyw, input_blocks(current_block)%keywords)
		end if
		
		end subroutine add_keyword
		
	end subroutine parse_input
	
	
	!Remove all non-CHARSET symbols
	subroutine remove_tabs(str)
	character(*),intent(inout)				::	str
	character,parameter						::	ch_space	= " "
	integer									::	r
	
	do 
		r= verify(str, CHARSET)
		if(r > 0)then
			str(r:r)= ch_space
		else
			exit
		end if
	end do
	
	end subroutine remove_tabs
	
	
	end module input_parser