	module math
	!General maths

	use types
	use,intrinsic::ieee_arithmetic

	implicit none
	
	real(pr),parameter::HBARC= 197.327_pr
	real(pr),parameter::EVNM= 1239.84_pr
	real(pr),parameter::PI= 3.14159265358979324_pr
	!real(pr),parameter::inf_pos= ieee_value(1._pr, ieee_positive_inf)
	!real(pr),parameter::inf_neg= ieee_value(1._pr, ieee_negative_inf)
	
	integer,parameter::POL_TE=1
	integer,parameter::POL_TM=2
	
	integer,parameter::SQ_X=10
	integer,parameter::SQ_Y=11
	integer,parameter::SQ_Z=12
	integer,parameter::BL_X=13
	integer,parameter::BL_Y=14
	integer,parameter::BL_Z=15
	
	integer,parameter::MASK_DENY=-4
	integer,parameter::MASK_NO_BLOCH=-3
	integer,parameter::MASK_BLOCH=-2
	integer,parameter::MASK_NO_WG=-1
	integer,parameter::MASK_NO_MODE=0
	
	interface operator(.dot.)
		module procedure matrix_dot_vector
		module procedure matrix_dot_matrix
	end interface
	
	interface operator(+)
		module procedure matrix_plus_matrix
	end interface
	
	interface sum
		module procedure vector_sum
	end interface
	
	interface assignment(=)
		module procedure assign_vector, assign_matrix
	end interface
	
	interface operator(/)
		module procedure vector_div_complex
	end interface

	contains
	
	
	!Operator function for matrix-vector multiplication
	!DEC$ ATTRIBUTES FORCEINLINE :: assign_vector
	elemental subroutine assign_vector(a,b)
	type(vector_t(sz=*)),intent(out)		::	a
	type(vector_t(sz=*)),intent(in)			::	b
	integer									::	i
	
	!a%v= (0._pr,0._pr)
	i= min(a%sz,b%sz)
	a%v(1:i)= b%v(1:i)
	if(i<a%sz)a%v(i+1:)= (0._pr,0._pr)
	
	end subroutine assign_vector
	
	
	!DEC$ ATTRIBUTES FORCEINLINE :: assign_matrix
	elemental subroutine assign_matrix(a,b)
	type(matrix_t(sz=*)),intent(out)		::	a
	type(matrix_t(sz=*)),intent(in)			::	b
	integer									::	i
	
	!a%v= (0._pr,0._pr)
	i= min(a%sz,b%sz)
	a%m(1:i,1:i)= b%m(1:i,1:i)
	if(i<a%sz)a%m(i+1:,i+1:)= (0._pr,0._pr)
	
	end subroutine assign_matrix
	
	
	!DEC$ ATTRIBUTES FORCEINLINE :: matrix_dot_vector
	elemental function matrix_dot_vector(m,v)result(r)
	type(matrix_t(sz=*)),intent(in)			::	m
	type(vector_t(sz=m%sz)),intent(in)		::	v
	type(vector_t(sz=m%sz))					::	r
	
	r%v= matmul(m%m,v%v)
	
	end function matrix_dot_vector
	
	!DEC$ ATTRIBUTES FORCEINLINE :: matrix_dot_matrix
	elemental function matrix_dot_matrix(m1,m2)result(r)
	type(matrix_t(sz=*)),intent(in)			::	m1
	type(matrix_t(sz=m1%sz)),intent(in)		::	m2
	type(matrix_t(sz=m1%sz))				::	r
	
	r%m= matmul(m1%m,m2%m)
	
	end function matrix_dot_matrix
	
	!DEC$ ATTRIBUTES FORCEINLINE :: matrix_plus_matrix
	elemental function matrix_plus_matrix(m1,m2)result(r)
	type(matrix_t(sz=*)),intent(in)			::	m1
	type(matrix_t(sz=m1%sz)),intent(in)		::	m2
	type(matrix_t(sz=m1%sz))				::	r
	
	r%m= m1%m+m2%m
	
	end function matrix_plus_matrix
	
	
	!Overloaded intrinsic for vector summation
	!DEC$ ATTRIBUTES FORCEINLINE :: vector_sum
	pure function vector_sum(vecs)result(res)
	type(vector_t(sz=*)),dimension(:),&
		intent(in)							::	vecs
	type(vector_t(sz=vecs%sz))				::	res
	integer									::	i, l, u
	
	l= lbound(res%v,1); u= ubound(res%v,1)
	res%v= [(sum(vecs%v(i)),i= l,u)]
	
	end function vector_sum
	
	
	!Overloaded intrinsic for vector division by complex
	!DEC$ ATTRIBUTES FORCEINLINE :: vector_div_complex
	elemental function vector_div_complex(vec,c)result(res)
	type(vector_t(sz=*)),intent(in)			::	vec
	complex(pr),intent(in)					::	c
	type(vector_t(sz=vec%sz))				::	res
	
	res%v= vec%v/c
	
	end function vector_div_complex
	
	
	!Argument of a complex number
	!DEC$ ATTRIBUTES FORCEINLINE :: carg
	elemental real(pr) function carg(c)
	complex(pr),intent(in)					::	c
	
	carg= atan2(c%im,c%re)
	
	end function carg
	
	
	!Check if number is in range
	elemental logical function in_range(x,a,b)
	real(pr),intent(in)						::	x,a,b
	
	if(x<b .and. x>=a)then
        in_range=.true.
    else
        in_range=.false.
	end if
	
	end function in_range
	
	!Make intermediate values
	function interpolate_grid(lo, hi, dm)result(arr)
	integer, intent(in)						::	dm
	real(pr),dimension(dm)					::	arr
	real(pr),intent(in)						::	lo, hi
	integer									::	i
	
	if(dm == 1)then
		arr= lo
	else
		arr= [(lo+(hi-lo)*real(i-1)/real(dm-1), i= 1,dm)]
	end if
	
	end function interpolate_grid
	
	
	!Conventional weighed average
	pure function w_avg(vals,weights)
	real(pr)								::	w_avg
	real(pr),dimension(:),intent(in)		::	vals,weights
	
	w_avg= sum(vals*weights)/sum(weights)
	
	end function w_avg
	
	
	pure function w_avg_c(vals,weights)
	real(pr)								::	w_avg_c
	complex(pr),dimension(:),intent(in)		::	vals
	real(pr),dimension(:),intent(in)		::	weights
	
	w_avg_c= sum(vals*weights)/sum(weights)
	
	end function w_avg_c
	
	
	!Test if center point is a local max
	pure function mid_local_max(arr)result(res)
	logical									::	res
	real(pr),dimension(:),intent(in)		::	arr
	integer									::	sz, mid, mid1, mid2, i
	
	sz= size(arr)
	res= .true.
	if(sz<3 .or. any(ieee_is_nan(arr)))then
		res= .false.
		return
	end if
	if(mod(sz,2)==1)then
		mid=sz/2+1
		do i= mid+1,sz
			if(arr(i)>=arr(i-1))res= .false.
		end do
		do i= 1,mid-1
			if(arr(i)>=arr(i+1))res= .false.
		end do
	else
		mid1=sz/2
		mid2=sz+1
		do i= mid2+1,sz
			if(arr(i)>=arr(i-1))res= .false.
		end do
		do i= 1,mid1-1
			if(arr(i)>=arr(i+1))res= .false.
		end do
	end if
	
	end function mid_local_max
	
	
	!Test if center point is a local min
	pure function mid_local_min(arr)result(res)
	logical									::	res
	real(pr),dimension(:),intent(in)		::	arr
	integer									::	sz, mid, mid1, mid2, i
	
	sz= size(arr)
	res= .true.
	if(sz<3 .or. any(ieee_is_nan(arr)))then
		res= .false.
		return
	end if
	if(mod(sz,2)==1)then
		mid=sz/2+1
		do i= mid+1,sz
			if(arr(i)<=arr(i-1))res= .false.
		end do
		do i= 1,mid-1
			if(arr(i)<=arr(i+1))res= .false.
		end do
	else
		mid1=sz/2
		mid2=sz+1
		do i= mid2+1,sz
			if(arr(i)<=arr(i-1))res= .false.
		end do
		do i= 1,mid1-1
			if(arr(i)<=arr(i+1))res= .false.
		end do
	end if
	
	end function mid_local_min
	
	
	!Get kx from refractivity and angle
	!DEC$ ATTRIBUTES FORCEINLINE :: get_kx
	elemental function get_kx(k0,refr,an)
	complex(pr)								::	get_kx
	complex(pr),intent(in)					::	k0, refr
	real(pr),intent(in)						::	an
	
	get_kx= refr*k0*sin(PI*an/180._pr)
	
	end function get_kx
	

	!Matrix product
	!DEC$ ATTRIBUTES FORCEINLINE :: mat_prod
	function mat_prod(arr)result(prod)
	type(matrix_t(2)),&
		dimension(:),intent(in)				::	arr
	type(matrix_t(2))				::	prod
	integer									::	i

	prod%m=0
	do i= 1,2
		prod%m(i,i)= 1._pr
	end do
	do i= ubound(arr,1),lbound(arr,1),-1
		prod%m= matmul(arr(i)%m,prod%m)
	end do
	
	end function mat_prod
	
	
	!Assign cpecific transfer matrices
	elemental subroutine assign_TrMat(layer,l_type)
	class(un_layer_t),intent(inout)			::	layer
	integer,intent(in)						::	l_type
	
	select case (l_type)
	case (LT_CONST_REFR,LT_VAR_REFR)
		layer%TrMatTE_sub=> TrMatTE_refr
		layer%TrMatTM_sub=> TrMatTM_refr
		layer%dTMdwTE_fun=> dTMdwTE_refr
		layer%dTMdwTM_fun=> dTMdwTM_refr
		layer%dTMdkxTE_fun=> dTMdkxTE_refr
		layer%dTMdkxTM_fun=> dTMdkxTM_refr
	case (LT_QW)
		layer%TrMatTE_sub=> TrMatTE_qw
		layer%TrMatTM_sub=> TrMatTM_qw
		!TODO MAKE OK
		layer%dTMdwTE_fun=> dTMdwTE_refr
		layer%dTMdwTM_fun=> dTMdwTM_refr
		layer%dTMdkxTE_fun=> dTMdkxTE_refr
		layer%dTMdkxTM_fun=> dTMdkxTM_refr
	end select
	
	end subroutine assign_TrMat
	
	
	!Transfer matrix derivative (k0)
	pure recursive function dTrMdw(point,part_prod,cl,ls,polar)result(dtdw)
	type(matrix_t(2))						::	dtdw
	type(grid_point_t),intent(in)			::	point
	type(matrix_t(2)),dimension(:),&
		intent(in)							::	part_prod
	class(computed_layer_t),dimension(:),&
		intent(in)							::	cl
	class(un_layer_t),dimension(:),&
		intent(in)							::	ls
	type(matrix_t(2))						::	tm
	integer,intent(in)						::	polar
	procedure(dTM_fun),pointer				::	dfunc
	
	select case(polar)
	case(POL_TE)
		dfunc=> ls(1)%dTMdwTE_fun
		tm= cl(1)%TrMatTE
	case(POL_TM)
		dfunc=> ls(1)%dTMdwTM_fun
		tm= cl(1)%TrMatTM
	end select
	if(size(part_prod)==1)then
		dtdw= dfunc(ls(1),cl(1),point)
	else
		dtdw= (dfunc(ls(1),cl(1),point) .dot. part_prod(2)) + (tm .dot. dTrMdw(point,part_prod(2:),cl(2:),ls(2:),polar))
	end if
	
	end function dTrMdw
	
	!Transfer matrix derivative (kx)
	pure recursive function dTrMdkx(point,part_prod,cl,ls,polar)result(dtdkx)
	type(matrix_t(2))						::	dtdkx
	type(grid_point_t),intent(in)			::	point
	type(matrix_t(2)),dimension(:),&
		intent(in)							::	part_prod
	class(computed_layer_t),dimension(:),&
		intent(in)							::	cl
	class(un_layer_t),dimension(:),&
		intent(in)							::	ls
	type(matrix_t(2))						::	tm
	integer,intent(in)						::	polar
	procedure(dTM_fun),pointer				::	dfunc
	
	select case(polar)
	case(POL_TE)
		dfunc=> ls(1)%dTMdkxTE_fun
		tm= cl(1)%TrMatTE
	case(POL_TM)
		dfunc=> ls(1)%dTMdkxTM_fun
		tm= cl(1)%TrMatTM
	end select
	if(size(part_prod)==1)then
		dtdkx= dfunc(ls(1),cl(1),point)
	else
		dtdkx= (dfunc(ls(1),cl(1),point) .dot. part_prod(2)) + (tm .dot. dTrMdkx(point,part_prod(2:),cl(2:),ls(2:),polar))
	end if
	
	end function dTrMdkx
	
	
	!Transfer matrix for a conventional layer with given refractivity
	pure subroutine TrMatTE_refr(this,layer,point)
	type(matrix_t(2))						::	tm
	class(un_layer_t),intent(in)			::	this
	class(computed_layer_t),intent(inout)	::	layer
	class(grid_point_t),intent(in)			::	point
	complex(pr)								::	phase, ncosth, sinph, cosph
	
	!TODO test for NaN
	ncosth= layer%n  *  sqrt(1 - point%kx**2/(point%k0*layer%n)**2)
	phase= this%thick*  sqrt(((point%k0*layer%n)-point%kx)*((point%k0*layer%n)+point%kx))
	cosph= cos(phase)
	sinph= sin(phase)
	
	tm%m(1,:)= [cosph,							(0._pr,-1._pr)*sinph/ncosth	]
	tm%m(2,:)= [(0._pr,-1._pr)*sinph*ncosth,	cosph						]
	
	layer%TrMatTE= tm
	
	end subroutine TrMatTE_refr
	
	
	pure function dTMdwTE_refr(this,layer,point)result(dtm)
	type(matrix_t(2))						::	dtm
	class(un_layer_t),intent(in)			::	this
	class(computed_layer_t),intent(in)		::	layer
	class(grid_point_t),intent(in)			::	point
	complex(pr)								::	phase, ncosth, sinph, cosph,&
												dph, dn, drevncosth, dncosth
	
	!TODO test for NaN
	associate(kx=>point%kx,k0=>point%k0,n=>layer%n)
		if(this%is_var_refr)then
			ncosth= this%n_func(point,dn); dn= HBARC*dn/(2._pr*n)
		else
			dn= 0
		end if
		if(abs(n*k0-kx)<epsilon(1._pr))then
			dtm%m(:,:)= 0
		else
			ncosth= n  *  sqrt(1 - kx**2/(k0*n)**2)
			dncosth= (n*dn+kx**2/k0**3)/ncosth
			drevncosth= -dncosth/(n**2-kx**2/k0**2)
			phase= this%thick*  sqrt((n*k0-kx)*(n*k0+kx))
			dph= this%thick* (k0**2*n*dn+k0*n**2)/sqrt((n*k0-kx)*(n*k0+kx))
			cosph= cos(phase)
			sinph= sin(phase)
	
			dtm%m(1,:)= [-sinph*dph,							(0._pr,-1._pr)*(sinph*drevncosth+cosph*dph/ncosth)	]
			dtm%m(2,:)= [(0._pr,-1._pr)*(sinph*dncosth+cosph*dph*ncosth),	-sinph*dph						]
		end if
	end associate
	
	end function dTMdwTE_refr
	
	pure function dTMdkxTE_refr(this,layer,point)result(dtm)
	type(matrix_t(2))						::	dtm
	class(un_layer_t),intent(in)			::	this
	class(computed_layer_t),intent(in)		::	layer
	class(grid_point_t),intent(in)			::	point
	complex(pr)								::	phase, ncosth, sinph, cosph,&
												dph, drevncosth, dncosth
	
	!TODO test for NaN
	associate(kx=>point%kx,k0=>point%k0,n=>layer%n)
		if(abs(n*k0-kx)<epsilon(1._pr))then
			dtm%m(:,:)= 0
		else
			ncosth= n  *  sqrt(1 - kx**2/(k0*n)**2)
			dncosth= -kx/k0/sqrt(n**2*k0**2-kx**2)
			drevncosth= -dncosth/(n**2-kx**2/k0**2)
			phase= this%thick*  sqrt((n*k0-kx)*(n*k0+kx))
			dph= -this%thick* kx/sqrt((n*k0-kx)*(n*k0+kx))
			cosph= cos(phase)
			sinph= sin(phase)
	
			dtm%m(1,:)= [-sinph*dph,							(0._pr,-1._pr)*(sinph*drevncosth+cosph*dph/ncosth)	]
			dtm%m(2,:)= [(0._pr,-1._pr)*(sinph*dncosth+cosph*dph*ncosth),	-sinph*dph						]
		end if
	end associate
	
	end function dTMdkxTE_refr
	
	
	pure subroutine TrMatTM_refr(this,layer,point)
	type(matrix_t(2))						::	tm
	class(un_layer_t),intent(in)			::	this
	class(computed_layer_t),intent(inout)	::	layer
	class(grid_point_t),intent(in)			::	point
	complex(pr)								::	phase, costhbyn, sinph, cosph
	
	costhbyn= sqrt(1 - point%kx**2/(point%k0*layer%n)**2) / layer%n
	phase= this%thick*sqrt(((point%k0*layer%n)-point%kx)*((point%k0*layer%n)+point%kx))
	cosph= cos(phase)
	sinph= sin(phase)
	
	tm%m(1,:)= [cosph,							(0._pr,-1._pr)*sinph/costhbyn	]
	tm%m(2,:)= [(0._pr,-1._pr)*sinph*costhbyn,	cosph							]
	
	layer%TrMatTM= tm
	
	end subroutine TrMatTM_refr
	
	
	pure function dTMdwTM_refr(this,layer,point)result(dtm)
	type(matrix_t(2))						::	dtm
	class(un_layer_t),intent(in)			::	this
	class(computed_layer_t),intent(in)		::	layer
	class(grid_point_t),intent(in)			::	point
	complex(pr)								::	phase, costhbyn, sinph, cosph,&
												dph, dn, drevcosthbyn, dcosthbyn
	
	!TODO test for NaN
	associate(kx=>point%kx,k0=>point%k0,n=>layer%n)
		if(this%is_var_refr)then
			costhbyn= this%n_func(point,dn); dn= HBARC*dn/(2._pr*n)
		else
			dn= 0
		end if
		if(abs(n*k0-kx)<epsilon(1._pr))then
			dtm%m(:,:)= 0
		else
			costhbyn= sqrt(1 - kx**2/(k0*n)**2)/n
			dcosthbyn= ((n*dn+kx**2/k0**3)/n**4-2*dn*(n**2-kx**2/k0**2)/n**5)/costhbyn
			drevcosthbyn= -dcosthbyn/costhbyn**2
			phase= this%thick*  sqrt((n*k0-kx)*(n*k0+kx))
			dph= this%thick* (k0**2*n*dn+k0*n**2)/sqrt((n*k0-kx)*(n*k0+kx))
			cosph= cos(phase)
			sinph= sin(phase)
	
			dtm%m(1,:)= [-sinph*dph,							(0._pr,-1._pr)*(sinph*drevcosthbyn+cosph*dph/costhbyn)	]
			dtm%m(2,:)= [(0._pr,-1._pr)*(sinph*dcosthbyn+cosph*dph*costhbyn),	-sinph*dph						]
		end if
	end associate
	
	end function dTMdwTM_refr
	
	pure function dTMdkxTM_refr(this,layer,point)result(dtm)
	type(matrix_t(2))						::	dtm
	class(un_layer_t),intent(in)			::	this
	class(computed_layer_t),intent(in)		::	layer
	class(grid_point_t),intent(in)			::	point
	complex(pr)								::	phase, costhbyn, sinph, cosph,&
												dph, drevcosthbyn, dcosthbyn
	
	!TODO test for NaN
	associate(kx=>point%kx,k0=>point%k0,n=>layer%n)
		if(abs(n*k0-kx)<epsilon(1._pr))then
			dtm%m(:,:)= 0
		else
			costhbyn= sqrt(1 - kx**2/(k0*n)**2)/n
			dcosthbyn= -kx/k0/n**2/sqrt(n**2*k0**2-kx**2)
			drevcosthbyn= -dcosthbyn/costhbyn**2
			phase= this%thick*  sqrt((n*k0-kx)*(n*k0+kx))
			dph= -this%thick* kx/sqrt((n*k0-kx)*(n*k0+kx))
			cosph= cos(phase)
			sinph= sin(phase)
	
			dtm%m(1,:)= [-sinph*dph,							(0._pr,-1._pr)*(sinph*drevcosthbyn+cosph*dph/costhbyn)	]
			dtm%m(2,:)= [(0._pr,-1._pr)*(sinph*dcosthbyn+cosph*dph*costhbyn),	-sinph*dph						]
		end if
	end associate
	
	end function dTMdkxTM_refr
	
	
	!p1 : resonanse frequency
	!p2 : radiative damping /greek{G}_0
	!p3 : non-radiative damping /greek{G}
	!p4 : splitting of Z - polariton greek{d}
	pure subroutine TrMatTE_qw(this,layer,point)
	type(matrix_t(2))						::	tm
	class(un_layer_t),intent(in)			::	this
	class(computed_layer_t),intent(inout)	::	layer
	class(grid_point_t),intent(in)			::	point
	complex(pr)								::	costh, ncosth, gbycosth, r,rnarp1
	
	costh= sqrt(1 - point%kx**2/(point%k0*layer%n)**2)
	associate (p=> this%param_qw)
		gbycosth= p(2)/costh
		!r= (0._pr,1._pr)*gbycosth &
		!	/(p(1)-HBARC*point%k0-(0._pr,1._pr)*(gbycosth+p(3)))
		rnarp1= (0._pr,1._pr)*gbycosth &
			/(p(1)-HBARC*point%k0-(0._pr,1._pr)*p(3))
	end associate
	
	ncosth= layer%n*costh
	tm%m(1,:)= [1._pr,						0._pr			]
	tm%m(2,:)= [-2._pr*ncosth*rnarp1,	(1._pr,0._pr)	]	!r/(1._pr+r)
	
	layer%TrMatTE= tm
	
	end subroutine TrMatTE_qw
	
	
	!As above for TM polarisation
	pure subroutine TrMatTM_qw(this,layer,point)
	type(matrix_t(2))						::	tm
	class(un_layer_t),intent(in)			::	this
	class(computed_layer_t),intent(inout)	::	layer
	class(grid_point_t),intent(in)			::	point
	complex(pr)								::	costh, costhbyn, gcosth, r, sinsqth, gsincos, rnarp1,&
												D, zd, G0, G
	
	costh= sqrt(1 - point%kx**2/(point%k0*layer%n)**2)
	sinsqth= point%kx**2/(point%k0*layer%n)**2
	associate (p=> this%param_qw)
		G0= cmplx(0._pr, p(2))
		G= cmplx(0._pr, p(3))
		D= p(1)-HBARC*point%k0
		zd= p(4)
		gcosth= p(2)*costh
		gsincos= p(2)*sinsqth/costh
		r= (0._pr,1._pr)*gcosth &
			/(p(1)-HBARC*point%k0-(0._pr,1._pr)*(gcosth+p(3))) &
			- (0._pr,1._pr)*gsincos &
			/(p(1)+p(4)-HBARC*point%k0-(0._pr,1._pr)*(gsincos+p(3)))
		
		rnarp1= D*G0*(costh**2-sinsqth)+zd*G0*costh**2-G*G0*(costh**2-sinsqth)
		rnarp1= rnarp1/costh/((G-D)**2-zd*(G-D)+2._pr*(G-D)*G0*sinsqth/costh+G0**2*sinsqth)
	end associate
	
	costhbyn= costh*layer%n
	tm%m(1,:)= [(1._pr,0._pr),	-2._pr*costhbyn*rnarp1			]
	tm%m(2,:)= [(0._pr,0._pr),	(1._pr,0._pr)	]
	!*r/(1._pr+r)
	layer%TrMatTM= tm
	
	end subroutine TrMatTM_qw
	
	
	
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	
	!Calculate reflection, transmission et al
	elemental subroutine rta(matrix,point,firstl,lastl,pol,r,t,rpow,tpow)
	type(matrix_t(2)),intent(in)			::	matrix
	type(grid_point_t),intent(in)			::	point
	type(computed_layer_t),intent(in)		::	firstl,lastl
	integer,intent(in)						::	pol
	complex(pr),intent(out),optional		::	r, t
	real(pr),intent(out),optional			::	rpow, tpow
	complex(pr)								::	pq0, pql, denom
	
	select case (pol)
	case (POL_TE)
		pq0= firstl%n *	sqrt(1._pr - point%kx**2/(point%k0*firstl%n)**2)
		pql= lastl%n  * sqrt(1._pr - point%kx**2/(point%k0*lastl%n)**2)
	case(POL_TM)
		pq0= sqrt(1._pr - point%kx**2/(point%k0*firstl%n)**2)/ firstl%n
		pql= sqrt(1._pr - point%kx**2/(point%k0*lastl%n)**2)/ lastl%n
	end select
	
	associate(tm=> matrix%m)
		denom= (tm(1,1)+tm(1,2)*pql)*pq0+(tm(2,1)+tm(2,2)*pql)
		if(present(r))then
			r= ((tm(1,1)+tm(1,2)*pql)*pq0-(tm(2,1)+tm(2,2)*pql))/denom
			if(present(rpow))then
				rpow= abs(r)**2
			end if
		end if
		if(present(t))then
			t= 2._pr*pq0/denom
			if(present(tpow))then
				tpow= pql*abs(t)**2/pq0
			end if
		end if
	end associate
	
	end subroutine rta
	
	subroutine drtdw(matrix,dtm,point,firstl,lastl,pol,dr,dt)
	type(matrix_t(2)),intent(in)			::	matrix,dtm
	type(grid_point_t),intent(in)			::	point
	type(un_layer_t),intent(in)				::	firstl,lastl
	integer,intent(in)						::	pol
	complex(pr),intent(out)					::	dr, dt
	complex(pr)								::	pq0,pql,nom,denom,&
												d_pq0,d_pql,d_nom,d_denom
	complex(pr)								::	n0,nl,d_n0,d_nl,asqrt0,d_sqrt0,asqrtl,d_sqrtl
	
	if(firstl%is_var_refr)then
		n0= firstl%n_func(point,d_n0)
		d_n0= HBARC*d_n0/2._pr/n0
	else
		n0= firstl%const_n
		d_n0= 0
	end if
	if(lastl%is_var_refr)then
		nl= lastl%n_func(point,d_nl)
		d_nl= HBARC*d_nl/2._pr/nl
	else
		nl= lastl%const_n
		d_nl= 0
	end if
	associate (tm=> matrix%m, d_tm=> dtm%m, kx=> point%kx, k0=> point%k0)
		asqrt0= sqrt(1._pr - kx**2/(k0*n0)**2)
		d_sqrt0= kx**2*(d_n0*k0+n0)/(n0*k0)**3/asqrt0
		asqrtl= sqrt(1._pr - kx**2/(k0*nl)**2)
		d_sqrtl= kx**2*(d_nl*k0+nl)/(nl*k0)**3/asqrtl
		select case (pol)
		case (POL_TE)
			pq0= n0 *	asqrt0
			d_pq0= d_n0 * asqrt0 + n0 * d_sqrt0
			pql= nl  * asqrtl
			d_pql= d_nl * asqrtl + nl * d_sqrtl
		case (POL_TM)
			pq0= asqrt0/ n0
			d_pq0= d_sqrt0/n0-asqrt0*d_n0/n0**2
			pql= asqrtl/ nl
			d_pql= d_sqrtl/nl-asqrtl*d_nl/nl**2
		end select
		denom= (tm(1,1)+tm(1,2)*pql)*pq0+(tm(2,1)+tm(2,2)*pql)
		d_denom= pq0*(d_tm(1,1)+d_tm(1,2)*pql+tm(1,2)*d_pql)+d_pq0*(tm(1,1)+tm(1,2)*pql) + (d_tm(2,1)+d_tm(2,2)*pql+tm(2,2)*d_pql)
		nom= (tm(1,1)+tm(1,2)*pql)*pq0-(tm(2,1)+tm(2,2)*pql)
		d_nom= pq0*(d_tm(1,1)+d_tm(1,2)*pql+tm(1,2)*d_pql)+d_pq0*(tm(1,1)+tm(1,2)*pql) - (d_tm(2,1)+d_tm(2,2)*pql+tm(2,2)*d_pql)
		
		dr= d_nom/denom-nom*d_denom/denom**2
		dt= 2._pr*(d_pq0/denom-pq0*d_denom/denom**2)
	end associate
	
	end subroutine drtdw
	
	
	subroutine drtdkx(matrix,dtm,point,firstl,lastl,pol,dr,dt)
	type(matrix_t(2)),intent(in)			::	matrix,dtm
	type(grid_point_t),intent(in)			::	point
	type(un_layer_t),intent(in)				::	firstl,lastl
	integer,intent(in)						::	pol
	complex(pr),intent(out)					::	dr, dt
	complex(pr)								::	pq0,pql,nom,denom,&
												d_pq0,d_pql,d_nom,d_denom
	complex(pr)								::	n0,nl,asqrt0,d_sqrt0,asqrtl,d_sqrtl
	
	if(firstl%is_var_refr)then
		n0= firstl%n_func(point)
	else
		n0= firstl%const_n
	end if
	if(lastl%is_var_refr)then
		nl= lastl%n_func(point)
	else
		nl= lastl%const_n
	end if
	associate (tm=> matrix%m, d_tm=> dtm%m, kx=> point%kx, k0=> point%k0)
		asqrt0= sqrt(1._pr - kx**2/(k0*n0)**2)
		d_sqrt0= -kx/asqrt0
		asqrtl= sqrt(1._pr - kx**2/(k0*nl)**2)
		d_sqrtl= -kx/asqrtl
		select case (pol)
		case (POL_TE)
			pq0= n0 *	asqrt0
			d_pq0= n0 * d_sqrt0
			pql= nl  * asqrtl
			d_pql= nl * d_sqrtl
		case (POL_TM)
			pq0= asqrt0/ n0
			d_pq0= d_sqrt0/n0
			pql= asqrtl/ nl
			d_pql= d_sqrtl/nl
		end select
		denom= (tm(1,1)+tm(1,2)*pql)*pq0+(tm(2,1)+tm(2,2)*pql)
		d_denom= pq0*(d_tm(1,1)+d_tm(1,2)*pql+tm(1,2)*d_pql)+d_pq0*(tm(1,1)+tm(1,2)*pql) + (d_tm(2,1)+d_tm(2,2)*pql+tm(2,2)*d_pql)
		nom= (tm(1,1)+tm(1,2)*pql)*pq0-(tm(2,1)+tm(2,2)*pql)
		d_nom= pq0*(d_tm(1,1)+d_tm(1,2)*pql+tm(1,2)*d_pql)+d_pq0*(tm(1,1)+tm(1,2)*pql) - (d_tm(2,1)+d_tm(2,2)*pql+tm(2,2)*d_pql)
		
		dr= d_nom/denom-nom*d_denom/denom**2
		dt= 2._pr*(d_pq0/denom-pq0*d_denom/denom**2)
	end associate
	
	end subroutine drtdkx
	
	
	!Calculate S-quantization initial fields
	pure subroutine init_field_Sq(r1,r2,t1,t2,layer,point,direction,ivp,ivm,evp,evm,u_p,u_m)
	type(vector_t(2))						::	f
	complex(pr),intent(in)					::	r1,r2,t1,t2
	type(computed_layer_t),intent(in)		::	layer(2)
	type(grid_point_t),intent(in)			::	point
	integer,intent(in)						::	direction
	type(vector_t(2)),intent(out),optional	::	ivp,ivm
	complex(pr),intent(out),optional		::	evp,evm
	complex(pr),intent(out),optional		::	u_p, u_m
	complex(pr)								::	dtnorm, relr, sq, costh, v1, v2, lrn1, lrn2
	
	dtnorm= (t2-t1)/(2._pr*r2)
	relr= r1/r2
	!if(abs(r2) < tiny(1._pr) .and. abs(r1) < tiny(1._pr))then
	!	dtnorm= 0
	!	relr= 0
	!end if
	sq= sqrt(dtnorm**2+relr)
	lrn1= layer(1)%n
	lrn2= layer(2)%n
	costh= sqrt(1._pr - point%kx**2/(point%k0*lrn2)**2)

	v1= -dtnorm+sq
	if(present(evp))evp= v1
	if(present(u_p))u_p= r1+t2*v1 !t1/v1+r2 !t1+r2*v1
	if(present(ivp))then
		select case (direction)
		case (SQ_Y)
			ivp%v(1)= t1+r2*v1+v1
			ivp%v(2)= lrn2*costh*(t1+r2*v1-v1)
		case (SQ_X)
			ivp%v(1)= lrn1*t1+lrn2*(r2*v1-v1)
			ivp%v(2)= costh*(t1+r2*v1+v1)
		case (SQ_Z)
			ivp%v(1)= lrn1*t1+lrn2*(r2*v1-v1)
			ivp%v(2)= costh*(t1+r2*v1+v1)
		end select
	end if
	v2= -dtnorm-sq
	if(present(evm))evm= v2
	if(present(u_m))u_m= r1+t2*v2 !t1/v2+r2 !t1+r2*v2
	if(present(ivm))then
		select case (direction)
		case (SQ_Y)
			ivm%v(1)= t1+r2*v2+v2
			ivm%v(2)= lrn2*costh*(t1+r2*v2-v2)
		case (SQ_X)
			ivm%v(1)= lrn1*t1+lrn2*(r2*v2-v2)
			ivm%v(2)= costh*(t1+r2*v2+v2)
		case (SQ_Z)
			ivm%v(1)= lrn1*t1+lrn2*(r2*v2-v2)
			ivm%v(2)= costh*(t1+r2*v2+v2)
		end select
	end if
	
	end subroutine init_field_Sq
	
	
	!Calculate decaying waveguide initial fields
	pure subroutine init_field_decay(tm,layer,point,direction,ivp,ivm,evp,evm,u_p,u_m)
	type(vector_t(2))						::	f
	type(matrix_t(2)),intent(in)			::	tm
	type(computed_layer_t),intent(in)		::	layer(2)
	type(grid_point_t),intent(in)			::	point
	integer,intent(in)						::	direction
	type(vector_t(2)),intent(out),optional	::	ivp,ivm
	complex(pr),intent(out),optional		::	evp,evm
	complex(pr),intent(out),optional		::	u_p, u_m
	complex(pr)								::	costh, lrn, pq0, pql
	
	lrn= layer(2)%n
	costh= sqrt(1._pr - point%kx**2/(point%k0*lrn)**2)

	if(present(evp))evp= 1._pr
	if(present(evm))evm= 0._pr
	
	select case (direction)
	case (SQ_Y)
		if(present(ivp))then
			ivp%v(1)= 1._pr
			ivp%v(2)= lrn*costh
		end if
		pq0= layer(1)%n *	sqrt(1._pr - point%kx**2/(point%k0*layer(1)%n)**2)
		pql= layer(2)%n  * sqrt(1._pr - point%kx**2/(point%k0*layer(2)%n)**2)
	case (SQ_X,SQ_Z)
		if(present(ivp))then
			ivp%v(1)= lrn
			ivp%v(2)= costh
		end if
		pq0= sqrt(1._pr - point%kx**2/(point%k0*layer(1)%n)**2) / layer(1)%n
		pql= sqrt(1._pr - point%kx**2/(point%k0*layer(2)%n)**2) / layer(2)%n
	end select
	if(present(ivm))then
		ivm%v= 0._pr
	end if
	
	if(present(u_p))u_p= tm%m(1,1)+tm%m(2,1)/pq0+tm%m(1,2)*pql+tm%m(2,2)*pql/pq0
	if(present(u_m))u_m= 1._pr
	
	end subroutine init_field_decay
	
	
	!Initial fields for periodic boundary conditions
	pure subroutine init_field_bloch(tm, polar, ivp, ivm, mask, Kbloch, tl)
	type(matrix_t(2)),intent(in)			::	tm
	integer,intent(in)						::	polar
	type(vector_t(2)),intent(out),optional	::	ivp,ivm
	integer,dimension(:),&
		intent(out),optional				::	mask
	complex(pr),intent(out),optional		::	Kbloch
	real(pr),intent(in),optional			::	tl
	complex(pr)								::	tr, l1, l2
	type(vector_t(2))						::	check1, check2
	complex(pr)								::	det
	
	tr= (tm%m(1,1)+tm%m(2,2))/2._pr
	if(present(mask))then
		if(abs(tr) >= 1._pr .or. abs(imag(tr))>= 1e-4 )then !abs(imag(tr))>= tiny(1._pr)
			mask= MASK_NO_BLOCH
		else
			mask= MASK_BLOCH
		end if
	end if
	if(present(Kbloch))Kbloch= acos(tr)/tl
	associate(tmm=>tm%m)
		det= tmm(1,1)*tmm(2,2)-tmm(1,2)*tmm(2,1)
	end associate
	!print *,det
	l1= tr+sqrt(tr**2-det)
	l2= tr-sqrt(tr**2-det)
	!print *,l1,l2,l1*l2
	select case (polar)
	case (POL_TE)
		if(present(ivp))ivp%v= [(1._pr,0._pr), (l1-tm%m(1,1))/tm%m(1,2)]
		if(present(ivm))ivm%v= [(1._pr,0._pr), (l2-tm%m(1,1))/tm%m(1,2)]
	case (POL_TM)
		if(present(ivp))ivp%v= [tm%m(1,2)/(l1-tm%m(1,1)), (1._pr,0._pr)]
		if(present(ivm))ivm%v= [tm%m(1,2)/(l2-tm%m(1,1)), (1._pr,0._pr)]
	end select
	!check1= tm .dot. ivp
	!check2= tm .dot. ivm
	!print *,abs(ivp%v),abs(check1%v),abs(l1*(ivp%v))
	!print *,ivm%v,check2%v,l2*(ivm%v)
	!print *
	
	end subroutine init_field_bloch
	
	
	!Calculate norm for light cone mode
	pure function Sq_3D_norm(vec, r1, r2, t1, t2, n0, n1, complete)
	complex(pr)								::	Sq_3D_norm
	complex(pr),intent(in)					::	vec, r1, r2, t1, t2, n0, n1
	logical,optional,intent(in)				::	complete
	
	if(present(complete))then
		if(.not. complete)then
			Sq_3D_norm= 4.
			return
		end if
	end if
	!Sq_3D_norm=  (n0**2*(abs(1.0_pr+t2*vec+r1)**2)+n1**2*(abs(vec+r2*vec+t1)**2))/2._pr
	Sq_3D_norm=  (n0**2*(1.0_pr+abs(t2*vec+r1)**2)+n1**2*(abs(vec)**2+abs(r2*vec+t1)**2))!/2._pr

	end function Sq_3D_norm
	
	
	!Calculate norm for waveguide mode
	pure function Sq_WG_norm(init,tms,s_type,pt,c_layers,layers,tails)
	complex(pr)								::	Sq_WG_norm
	type(vector_t(2)),intent(in)			::	init
	type(matrix_t(2)),dimension(:),&
		intent(in)							::	tms
	integer,intent(in)						::	s_type
	type(grid_point_t),intent(in)			::	pt
	type(computed_layer_t),dimension(0:size(tms)),&
		intent(in)							::	c_layers
	type(un_layer_t),dimension(0:size(tms)),&
		intent(in)							::	layers
	logical,optional,intent(in)				::	tails
	integer									::	i
	type(vector_t(2)),dimension(1:size(tms))::	bnd
	complex(pr),dimension(0:size(tms))		::	kz, ps, ns
	real(pr),dimension(0:size(tms))			::	thicks
	complex(pr)								::	c1, c2, d1, d2, f, g
	real(pr)								::	c3, c4, d3, d4
	complex(pr),dimension(0:size(tms))		::	integral1, integral2
	complex(pr)								::	dedhw, tmp
	complex(pr),dimension(0:size(tms))		::	eps
	
	ns= c_layers%n; thicks= layers%thick
	do i=1,size(tms)
		bnd(i)= tms(i) .dot. init
	end do
	kz= sqrt(((pt%k0*ns)-pt%kx)*((pt%k0*ns)+pt%kx))
	select case (s_type)
	case (SQ_Y,BL_Y)
		ps= ns * sqrt(1 - pt%kx**2/(pt%k0*ns)**2)
	case (SQ_X,SQ_Z,BL_X,BL_Z)
		ps= sqrt(1 - pt%kx**2/(pt%k0*ns)**2) / ns
	end select
	integral1= 0._pr; integral2= 0._pr
	!Integrals over layers
	!HERE BE DRAGONS
	do i=1,size(tms)-1
		associate(	a=> real(kz(i)), b=> imag(kz(i)), p=> ps(i), n=> ns(i), t=>thicks(i), i1=> integral1(i), i2=> integral2(i),&
					lr=> layers(i))
			
			f= bnd(i+1)%v(1)
			g= bnd(i+1)%v(2)
			
			
			c1= (f+g/p)/2._pr
			c2= (f-g/p)/2._pr
			d1= (g+f*p)/2._pr
			d2= (g-f*p)/2._pr
			
			c3= 2._pr*abs(c1*conjg(c2))
			c4= carg(c1*conjg(c2))
			d3= 2._pr*abs(d1*conjg(d2))
			d4= carg(d1*conjg(d2))
			
			if(abs(a)>tiny(1._pr))then
				i1= i1+c3*(sin(a*t-c4)+sin(c4))/a
				i2= i2+d3*(sin(a*t-d4)+sin(d4))/a
			else
				i1= i1+c3*cos(c4)*t
				i2= i2+d3*cos(d4)*t
			end if
			if(abs(b)>tiny(1._pr))then
				i1= i1+abs(c1)**2*(exp(2._pr*b*t)-1._pr)/(2._pr*b)&
					+abs(c2)**2*(-exp(-2._pr*b*t)+1._pr)/(2._pr*b)
				i2= i2+abs(d1)**2*(exp(2._pr*b*t)-1._pr)/(2._pr*b)&
					+abs(d2)**2*(-exp(-2._pr*b*t)+1._pr)/(2._pr*b)
			else
				i1= i1+(abs(c1)**2+abs(c2)**2)*t
				i2= i2+(abs(d1)**2+abs(d2)**2)*t
			end if
			eps(i)= n**2
			!if(abs(imag(n))>tiny(1._pr) .and. lr%is_var_refr)then
			if(lr%is_var_refr)then
				tmp= lr%n_func(pt,dedhw)
				eps(i)= eps(i)+pt%k0*HBARC*dedhw
			end if
			integral1(i)= i1
			integral2(i)= i2
		end associate
					!HERE BE DRAGONS
	end do
	!Tails
	if(present(tails) .and. tails)then
		eps([0,size(tms)])= ns([0,size(tms)])**2
		integral1([0,size(tms)])= abs(bnd([1,size(tms)])%v(1))**2/(2._pr*imag(kz([0,size(tms)])))
		integral2([0,size(tms)])= abs(bnd([1,size(tms)])%v(2))**2/(2._pr*imag(kz([0,size(tms)])))
	else
		eps([0,size(tms)])= 0._pr
		integral1([0,size(tms)])= 0._pr
		integral2([0,size(tms)])= 0._pr
	end if
		
	select case (s_type)
	case (SQ_Y,BL_Y)
		Sq_WG_norm= sum(eps*integral1)
	case (SQ_X,SQ_Z)
		Sq_WG_norm= sum(eps*(integral2+integral1*abs(pt%kx/pt%k0/ns)**2))
	case (BL_X)
		Sq_WG_norm= sum(eps*integral2)
	case (BL_Z)
		Sq_WG_norm= sum(eps*integral1)
	end select
	
	end function Sq_WG_norm
	
	
	!Determine waveguide
	function is_waveguide(point,layers,near,factor)
	logical									::	is_waveguide
	type(grid_point_t),intent(in)			::	point
	type(computed_layer_t),&
		dimension(0:),intent(in)			::	layers
	logical,optional,intent(out)			::	near
	real(pr),optional,intent(in)			::	factor
	
	if(real(point%kx) > real(point%k0*maxval(real(layers([0,ubound(layers,1)])%n))))then
		is_waveguide= .true.
	else
		is_waveguide= .false.
	end if
	
	!Near light cone boundary
	if(present(near))then
		if(abs(real(point%kx)/real(point%k0*maxval(real(layers([0,ubound(layers,1)])%n)))-1)<factor)then
			near= .true.
		else
			near= .false.
		end if
	end if
	
	end function is_waveguide
	
	
	!Determine if field is decaying
	pure function is_decaying(init,point,str,last_n,s_type)
	logical									::	is_decaying
	type(vector_t(2)),intent(in)			::	init
	type(grid_point_t),intent(in)			::	point
	type(un_layer_t),dimension(0:),&
		intent(in)							::	str
	complex(pr),intent(in)					::	last_n
	integer,intent(in)						::	s_type
	type(computed_layer_t)					::	l_c
	type(un_layer_t)						::	l_un
	type(matrix_t(2))						::	tm
	type(vector_t(2))						::	field
	procedure(TrMat_sub),pointer			::	tm_sub
	real(pr)								::	th
	
	!TODO this is so ugly overall, it makes me want to kill myself
	th= minval(str(1:ubound(str,1)-1)%thick)/100._pr	!TODO remove ugly magic number
	!th= min(real(0.001_pr/sqrt((point%k0*last_n)**2-point%kx**2)),minval(str(1:ubound(str,1)-1)%thick)/100._pr)
	l_c%n= last_n
	select case (s_type)
	case (SQ_Y)
		tm_sub=> str(ubound(str,1))%TrMatTE_sub
		call tm_sub(un_layer_t(thick=th),l_c,point)
		tm= l_c%TrMatTE
	case (SQ_X,SQ_Z)
		tm_sub=> str(ubound(str,1))%TrMatTM_sub
		call tm_sub(un_layer_t(thick=th),l_c,point)
		tm= l_c%TrMatTM
	end select
	field= tm .dot. init
	if(abs(field%v(1)) <= abs(init%v(1)))then
		is_decaying= .true.
	else
		is_decaying= .false.
	end if
	
	end function is_decaying
	
	
	!CERNLib function to determine number of roots in a complex polygon
	function box_zeros(f,zp,n)
	integer									::	box_zeros, n
	complex(pr)								::	f,zp(*)

	real(pr)								::	eps
	complex(pr)								::	w,x,r,cst,const,hf
	dimension w(12),x(12)

	complex(pr)								::	i,cpi2,dz,h,a,b,c1,c2,bb,aa,u,s8,s16,fpp,fmm,fpm,fmp
	integer									::	nfmax,j,nf,k
	parameter (i = (0d0,1d0))
	parameter ( cpi2 = 2*pi*i)
	parameter (hf = 0.5, dz = (1+i)*1d-8)
	parameter (eps = 1d-4, cst = 0.005d0, nfmax = 200000)

	data x( 1) /9.6028985649753623d-1/, w( 1) /1.0122853629037626d-1/
	data x( 2) /7.9666647741362674d-1/, w( 2) /2.2238103445337447d-1/
	data x( 3) /5.2553240991632899d-1/, w( 3) /3.1370664587788729d-1/
	data x( 4) /1.8343464249564980d-1/, w( 4) /3.6268378337836198d-1/
	data x( 5) /9.8940093499164993d-1/, w( 5) /2.7152459411754095d-2/
	data x( 6) /9.4457502307323258d-1/, w( 6) /6.2253523938647893d-2/
	data x( 7) /8.6563120238783174d-1/, w( 7) /9.5158511682492785d-2/
	data x( 8) /7.5540440835500303d-1/, w( 8) /1.2462897125553387d-1/
	data x( 9) /6.1787624440264375d-1/, w( 9) /1.4959598881657673d-1/
	data x(10) /4.5801677765722739d-1/, w(10) /1.6915651939500254d-1/
	data x(11) /2.8160355077925891d-1/, w(11) /1.8260341504492359d-1/
	data x(12) /9.5012509837637440d-2/, w(12) /1.8945061045506850d-1/

	h=0
	do j = 1,n
		nf=0
		a=zp(j)
		if(j < n) then
			b=zp(j+1)
		else
			b=zp(1)
		endif
		if(b == a) cycle
		const=cst/abs(b-a)
		bb=a
		
1		aa=bb
		bb=b
		
2		c1=hf*(bb+aa)
		c2=hf*(bb-aa)
		s8=0
		
		do k = 1,4
			u=c2*x(k)
			fpp=f(c1+u+dz)
			fpm=f(c1+u-dz)
			fmp=f(c1-u+dz)
			fmm=f(c1-u-dz)
			s8= s8 + w(k)*( ((fpp-fpm)/dz)/(fpp+fpm) +&
							((fmp-fmm)/dz)/(fmp+fmm) )
		end do
		
		s16=0
		
		do k = 5,12
			u=c2*x(k)
			fpp=f(c1+u+dz)
			fpm=f(c1+u-dz)
			fmp=f(c1-u+dz)
			fmm=f(c1-u-dz)
			s16= s16+w(k)*( ((fpp-fpm)/dz)/(fpp+fpm) +&
							((fmp-fmm)/dz)/(fmp+fmm) )
		end do
		
		s16=c2*s16
		nf=nf+48
		if(abs(s16-c2*s8) <= eps*(1+abs(s16))) then
			h=h+s16
			if(bb /= b) go to 1
		else
			bb=c1
			if(1+const*abs(c2) /= 1 .and. nf <= nfmax) go to 2
			r=0
			print *,'SHEISE'
			go to 99
		endif
	end do
	
	r=h/cpi2
	print *,r
99	box_zeros= nint(abs(r))
	
	end function box_zeros
	
	end module math