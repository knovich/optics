	module types
	!Generic types and constants	
	
	implicit none

	integer,parameter::MAX_FILE_NAME= 1024		!TODO Should be revised
	integer,parameter::MAX_NAME_LEN= 100
	
	integer,parameter::pr= 8	!TODO UGLY
	integer,parameter::po= 8	!TODO UGLY

	!Task type constants
	integer,parameter::TASK_UNASSIGNED= 0
	integer,parameter::TASK_RTA= 1							!Reflection, transmission, absorption
	integer,parameter::TASK_PROFILE= 2						!Profile (S-quantization)
	integer,parameter::TASK_PURCELL= 3						!Purcell factor
	integer,parameter::TASK_DISP= 4
	
	character(*),parameter::TASKS_KEYS(*)=[character(MAX_NAME_LEN) ::&
						"RTA",&
						"Profile",&
						"Purcell",&
						"Disp"]

	!Unit kind constants
	integer,parameter::UNIT_E_EV= 1							!Energy in electron-volts
	integer,parameter::UNIT_E_NM= 2							!Wavelength in nm
	integer,parameter::UNIT_LAT_DEGREE= 11					!Outer angle in degrees
	integer,parameter::UNIT_LAT_RAD= 12						!Outer angle in radians
	integer,parameter::UNIT_LAT_KX_NM= 13					!Lateral wavevector in 1/nm

	!For keeping input keywords
	type keyword_t
		character(MAX_NAME_LEN)	::	key = "", value = ""
	end type keyword_t
	
	!Parse status constants
	integer,parameter::STATUS_ERROR= -1
	integer,parameter::STATUS_OK= 0
	integer,parameter::STATUS_RETRY= 1						!Retry input reading
	integer,parameter::STATUS_EMPTY= 2						!No blocks read
	
	!Other status constatnts
	integer,parameter::STATUS_STR_ERROR= 3					!Generic structure error

	!Parser buffer length
	integer,parameter::MAX_BUF_LEN= 256

	
	
	!Generic keys
	!TODO make prettier
	character(*),parameter::&
							GENERIC_KEYS(*,*)=reshape(&
											[character(MAX_NAME_LEN)::&
											"gen_en","gen_en","gen_en","energy","energy_grid",&
											"gen_lat","kx","kx_grid","angle","angle_grid"],&
											[2,5],&
											[character(MAX_NAME_LEN)::	"",""],[2,1])
	
	
	!Usual characters
	character(*),parameter::CHARSET="ABCDEFGHIJKLMNOPQRSTUVWXYZ"//&
									"abcdefghijklmnopqrstuvwxyz"//&
									"0123456789[]{}\|/.,<>?:"";'"//&
									"`~!@#$%^&*()_+-= "
	
	character(*),parameter::NUMCHARS=".0123456789"

	!Layer types for str file
	integer,parameter::LT_CONST_REFR=1
	integer,parameter::LT_VAR_REFR=2
	integer,parameter::LT_QW=3
	integer,parameter::LT_PARAM=4
	
	!Layer parameter count
	integer,parameter::PAR_SIZE=4
	!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

	!Syntax block from init file
	type:: input_block_t
		character(MAX_NAME_LEN)	::	block_name			= ""
		type(keyword_t),&
			dimension(:),&
			allocatable			::	keywords
	end type input_block_t

	!Generic complex matrix
	type:: matrix_t(sz)
		integer,len				::	sz
		complex(pr),dimension(sz,sz)&
								::	m
	end type matrix_t
	
	!Generic complex vector
	type:: vector_t(sz)
		integer,len				::	sz
		complex(pr),dimension(sz)&
								::	v
	end type vector_t
		
	!For keeping energy values or energy grid
	type:: energy_margins_t
		integer					::	unit					!Measurement unit (nm or eV)
		logical					::	grid					!Is grid present or just a single value
		real(pr)				::	energy					!For sole energy
		real(pr)				::	energy_lo	,&
									energy_hi				!For energy grid
		integer					::	nof_points				!Number of grid points
	end type energy_margins_t

	!For angles or kx values or grid
	type:: lateral_margins_t
		integer					::	unit					!Measurement unit (nm or eV)
		logical					::	grid					!Is grid present or just a single value
		real(pr)				::	lat						!For sole angle or K_x
		real(pr)				::	lat_lo	,&
									lat_hi					!For angle or K_x boundaries
		integer					::	nof_points				!Number of grid points
	end type lateral_margins_t

	!Point in a calculation grid
	type:: grid_point_t
		complex(pr)				::	k0					= 0
		complex(pr)				::	kx					= 0
	end type grid_point_t
	
	
	!Type for a unique layer
	type:: un_layer_t
		real(pr)				::	thick				= 0
		real(pr),dimension(PAR_SIZE)&
								::	param=0,param_qw				= 0
		logical					::	is_var_refr			= .false.	!Is refractivity a function of wl
		complex(pr)				::	const_n				= (0.,0.)
		integer					::	sp_index			= 0			!Special material index
		procedure(n_func),&
			pointer				::	n_func				=> null()
		procedure(TrMat_sub),&
			pointer				::	TrMatTE_sub		=> null(),&
									TrMatTM_sub		=> null()
		procedure(dTM_fun),&
			pointer				::	dTMdwTE_fun		=> null(),&
									dTMdwTM_fun		=> null(),&
									dTMdkxTE_fun	=> null(),&
									dTMdkxTM_fun	=> null()
	end type un_layer_t
	
	!Layer prepared for computation
	type:: computed_layer_t
		complex(pr)				::	n					= (0,0)
		type(matrix_t(2))		::	TrMatTE,&
									TrMatTM
	end type computed_layer_t
	
	abstract interface
		function n_func(this, point, dedhw)
			import								::	un_layer_t, computed_layer_t, grid_point_t, pr
			complex(pr)							::	n_func
			class(un_layer_t)					::	this
			class(grid_point_t),intent(in)		::	point
			complex(pr),optional,intent(out)	::	dedhw
		end function n_func
		
		pure subroutine TrMat_sub(this, layer, point)
			import								::	un_layer_t, computed_layer_t, &
													grid_point_t, matrix_t
			class(un_layer_t),intent(in)		::	this
			class(computed_layer_t),&
				intent(inout)					::	layer
			class(grid_point_t),intent(in)		::	point
		end subroutine TrMat_sub
		
		pure function dTM_fun(this, layer, point)
			import								::	un_layer_t, computed_layer_t, &
													grid_point_t, matrix_t
			type(matrix_t(2))					::	dTM_fun
			class(un_layer_t),intent(in)		::	this
			class(computed_layer_t),&
				intent(in)						::	layer
			class(grid_point_t),intent(in)		::	point
		end function dTM_fun
	end interface
	
	!Input job record
	type:: job_t
		character(MAX_NAME_LEN)	::	job_name			= ""
		integer					::	task				= TASK_UNASSIGNED
		logical					::	active				= .true.
		type(un_layer_t),&
			dimension(:),&
			allocatable			::	layers
		type(keyword_t),&
			dimension(:),&
			allocatable			::	keywords	
	end type job_t
	
	type,extends(job_t),abstract:: computation_t
	contains
		procedure(init_job_func),deferred		::	init
		procedure(driver_sub),deferred			::	driver
		procedure(output_sub),deferred			::	output
	end type computation_t
	
	type:: computation_p
		class(computation_t),&
			pointer				:: comp
	end type computation_p
	
	abstract interface
		function init_job_func(this)
			import								::	computation_t
			integer								::	init_job_func
			class(computation_t),intent(inout)	::	this
		end function init_job_func
		subroutine driver_sub(this)
			import								::	computation_t
			class(computation_t),intent(inout)	::	this
		end subroutine driver_sub
		subroutine output_sub(this)
			import								::	computation_t
			class(computation_t),intent(in)		::	this
		end subroutine output_sub
	end interface
	
	
	end module types