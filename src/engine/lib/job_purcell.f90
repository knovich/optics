	module job_purcell

	use types
	use keywords
	use math
	use error_report

	use job_rta
	
	implicit none

	private
	public :: make_purcell
	
	real(pr),parameter::NOISE_THRESHOLD=1e-5_pr
	real(pr),parameter::SUFFICIENT=1._pr
	
	integer,parameter::REFR_NO=0
	integer,parameter::REFR_AN=1
	integer,parameter::REFR_NUM=2
	
	integer,parameter::INTS=6
	
	type:: int_fact_t
		real(po)				::	energy =0._pr
		real(po),dimension(INTS)::	vals =0._pr
		integer(8),dimension(3)	::	mode_num =0
	end type int_fact_t
	
	type::	u_t
		complex(pr)				::	x1 =0._pr, x2 =0._pr,&
									y1 =0._pr, y2 =0._pr,&
									z1 =0._pr, z2 =0._pr
	end type u_t
	
	type::data_t
		real(pr)				::	field(6)=0.
		complex(pr)				::	norm(6)=1.
		integer					::	mask(6)
		complex(pr)				::	u(6)
		real(pr)				::	n_eff(6)=0
		complex(pr)				::	KBloch_TE, KBloch_TM, dKBdk0_TE=1, dKBdk0_TM=1
	end type data_t

	type:: diag_purcell_t
		real(po)				::	energy	=0.,&
									lateral	=0.
		real(po)				::	u_abs(6)=0
		real(po)				::	grad_x(6)=0,grad_w(6)=0
		complex(po)				::	eff_eps =0
		complex(po)				::	rt(8) =0
		complex(po)				::	norm(6)=1.
	end type diag_purcell_t
	
	type,extends(job_rta_t):: job_purcell_t
		complex(pr),dimension(:,:),&
			allocatable			::	r2TE, t2TE, r2TM, t2TM
		type(data_t),dimension(:,:),&
			allocatable			::	d
		type(diag_purcell_t),dimension(:,:),&
			allocatable			::	diag
		type(matrix_t(2)),dimension(:),&
			allocatable			::	prod_TE1, prod_TM1, &
									prod_TE2, prod_TM2
		type(matrix_t(2)),dimension&
			(:,:),allocatable	::	part_TE1, part_TM1, part_TE2, part_TM2
		type(computed_layer_t),dimension(:,:),&
			allocatable			::	l_row
		integer					::	dipole_layer	=0
		integer					::	mw =3, mhw =1
		real(pr)				::	dipole_z		=0.
		type(int_fact_t),&
			dimension(:),&
			allocatable			::	int_fact
		logical					::	integrals	=.true.
		real(pr)				::	phi1 =PI/2._pr,	theta1 =PI/2._pr,&
									phi2 =0._pr,	theta2 =PI/2._pr,&
									phi3 =0._pr,	theta3 =0._pr
		logical					::	no_trim		=.false., no_mode_contour=.true., no_norm = .false.
		logical					::	s_quantization = .true.
		logical					::	do_diag = .false.
		logical					::	norm_3d = .true.
		integer					::	l_left =1, l_right =0
		real(pr)				::	b_fact=0.0
		integer					::	eff_refr_kind=REFR_NO
		real(pr)				::	max_im_kx = 1._pr
		logical					::	ext_grid = .false.
		character(MAX_FILE_NAME)::	ext_grid_file
	contains
		procedure				::	init=>		init_purcell
		procedure				::	driver=>	driver_purcell
		procedure				::	output=>	output_purcell
		procedure,private		::	external_grid_init
		procedure,private		::	wg_masks
		procedure,private		::	wg_norms
		!procedure,private		::	eff_refr_adjust
		procedure,private		::	eff_refr_analytic
		procedure,private		::	eff_refr_numeric
		!procedure,private		::	nearest_mode
		procedure,private		::	wg_renorm
		procedure,private		::	int_light_cone
		procedure,private		::	check_kx_root
	end type job_purcell_t


	type:: output_purcell_t
		real(po)				::	energy	=0.,&
									lateral	=0.
		real(po)				::	x1 =0., x2 =0., &
									y1 =0., y2 =0., &
									z1 =0., z2 =0.
		integer					::	mask_x1 =0, mask_x2 =0, &
									mask_y1 =0, mask_y2 =0, &
									mask_z1 =0, mask_z2 =0
		complex(po)				::	KBloch_TE =0, KBloch_TM =0, dKBdk0_TE =0, dKBdk0_TM =0
		real(po)				::	n_eff(6)=0
	end type output_purcell_t
	
	
	interface
	module subroutine wg_masks(this,i,l_row,init_X,init_Y,init_Z)
	class(job_purcell_t),intent(inout)		::	this
	integer,intent(in)						::	i
	type(computed_layer_t),&
		dimension(&
		this%l_left-1:this%l_right+1,&
		ubound(this%grid,2)),intent(in)		::	l_row
	type(vector_t(2)),dimension(2,&
		ubound(this%grid,2)),intent(in)		::	init_X, init_Y, init_Z
	end subroutine wg_masks
	
	module subroutine wg_norms(this,i,j,l_row,init_X,init_Y,init_Z,tails,k_num)
	class(job_purcell_t),intent(inout)		::	this
	integer,intent(in)						::	i,j
	type(computed_layer_t),&
		dimension(&
		this%l_left-1:this%l_right+1,&
		ubound(this%grid,2)),intent(in)		::	l_row
	type(vector_t(2)),dimension(2,&
		ubound(this%grid,2)),intent(in)		::	init_X, init_Y, init_Z
	logical,intent(in),optional				::	tails
	integer,intent(in),optional				::	k_num
	end subroutine wg_norms
	
	!module subroutine eff_refr_adjust(this)
	!class(job_purcell_t),intent(inout)		::	this
	!end subroutine eff_refr_adjust
	
	module function eff_refr_analytic(this,i,j,pol,sgn)
	class(job_purcell_t),&
		intent(inout),target				::	this
	real(pr)								::	eff_refr_analytic
	integer,intent(in)						::	i,j,pol,sgn
	end function eff_refr_analytic
	
	module function eff_refr_numeric(this,i,j,k)
	class(job_purcell_t),&
		intent(inout),target				::	this
	real(pr)								::	eff_refr_numeric
	integer,intent(in)						::	i,j,k
	end function eff_refr_numeric
	
	!module function nearest_mode(this,v,index,row,k)result(res)
	!class(job_purcell_t),intent(in)			::	this
	!real(pr)								::	res
	!real(pr),intent(in)						::	v
	!integer,intent(in)						::	index, row, k
	!end function nearest_mode
	
	module subroutine int_light_cone(this, i, l_row, mask)
	class(job_purcell_t),intent(inout)		::	this
	integer,intent(in)						::	i, mask
	type(computed_layer_t),&
		dimension(&
		this%l_left-1:this%l_right+1,&
		ubound(this%grid,2)),intent(in)		::	l_row
	end subroutine int_light_cone
	
	module subroutine wg_renorm(this)
	class(job_purcell_t),intent(inout)		::	this
	end subroutine wg_renorm
	
	module function check_kx_root(this, comp_ls, pt, dkx, polar, sign)
	class(job_purcell_t),&
		intent(in)&
		,target&
											::	this
	integer									::	check_kx_root
	type(computed_layer_t),&
		dimension(this%l_left-1:this%l_right+1),&
		intent(in)							::	comp_ls
	type(grid_point_t),intent(in)			::	pt
	complex(pr),intent(in)					::	dkx
	integer,intent(in)						::	polar, sign
	end function check_kx_root
	
	end interface
	

	contains

	
	function init_purcell(this)result(status)
	integer									::	status
	class(job_purcell_t),intent(inout)		::	this
	integer									::	pos, ios
	
	this%calculate_power= .false.
	status= this%job_rta_t%init()
	if(status /= STATUS_OK)then
		go to 200
	end if
	!Got grid, angles and wavelengths
	
	
	!Get dipole position
	associate(tkws=> this%keywords)
		pos= find_keyword("s_quantization",tkws)
		if(pos /= 0)then
			associate (kw=> tkws(pos))
				read(kw%value,*,iostat=ios) this%s_quantization
				if(ios /= 0)then
					!Something went wrong during value reading
					call report_error(ERR_NOSQ,SEV_RETRY,&
						"Incorrect S-quantization specification for "//trim(this%job_name))
					go to 100
				end if
			end associate
		end if
		
		pos= find_keyword("left_layer",tkws)
		if(pos /= 0)then
			read(tkws(pos)%value,*,iostat=ios) this%l_left
			if(ios /= 0 .or. this%l_left <= lbound(this%layers,1) .or. this%l_left >= ubound(this%layers,1))then
				!Something went wrong during value reading
				call report_error(ERR_OTHER,SEV_RETRY,&
					"Incorrect left_layer specification for "//trim(this%job_name))
				go to 100
			end if
		end if
		
		pos= find_keyword("right_layer",tkws)
		if(pos /= 0)then
			read(tkws(pos)%value,*,iostat=ios) this%l_right
			if(ios /= 0 .or. this%l_right <= lbound(this%layers,1) .or. this%l_right >= ubound(this%layers,1))then
				!Something went wrong during value reading
				call report_error(ERR_OTHER,SEV_RETRY,&
					"Incorrect right_layer specification for "//trim(this%job_name))
				go to 100
			end if
		else
			this%l_right= ubound(this%layers,1)-1
		end if
		
		if(this%l_left > this%l_right)then
			call report_error(ERR_OTHER,SEV_RETRY,&
				"Right layer is earlier than left in "//trim(this%job_name))
			go to 100
		end if
		
		pos= find_keyword("dipole_layer",tkws)
		if(pos /= 0)then
			read(tkws(pos)%value,*,iostat=ios) this%dipole_layer
			if(ios /= 0 .or. this%dipole_layer < this%l_left .or. this%dipole_layer > this%l_right)then
				call report_error(ERR_NODIP,SEV_RETRY,&
					"Incorrect dipole layer in "//trim(this%job_name))
				go to 100
			end if
		else
			call report_error(ERR_NODIP,SEV_RETRY,&
				"No dipole layer specified for "//trim(this%job_name))
			go to 100
		end if
		
		pos= find_keyword("dipole_z_in_layer",tkws)
		if(pos /= 0)then
			read(tkws(pos)%value,*,iostat=ios) this%dipole_z
			if(ios /= 0 .or. this%dipole_z < 0. .or. this%dipole_z > this%layers(this%dipole_layer)%thick)then
				call report_error(ERR_NODIPZ,SEV_RETRY,&
					"Incorrect dipole z in "//trim(this%job_name))
				go to 100
			end if
		else
			call report_error(ERR_NODIPZ,SEV_RETRY,&
				"No dipole z specified for "//trim(this%job_name))
			go to 100
		end if
		
		pos= find_keyword("integrals",tkws)
		if(pos /= 0)then
			read(tkws(pos)%value,*,iostat=ios) this%integrals
			if(ios /= 0)then
				!Something went wrong during value reading
				call report_error(ERR_OTHER,SEV_RETRY,&
					"Incorrect integrals specification for "//trim(this%job_name))
				go to 100
			end if
		end if
		
		pos= find_keyword("no_trim",tkws)
		if(pos /= 0)then
			read(tkws(pos)%value,*,iostat=ios) this%no_trim
			if(ios /= 0)then
				!Something went wrong during value reading
				call report_error(ERR_OTHER,SEV_RETRY,&
					"Incorrect trimming specification for "//trim(this%job_name))
				go to 100
			end if
		end if
		
		pos= find_keyword("no_mode_contour",tkws)
		if(pos /= 0)then
			read(tkws(pos)%value,*,iostat=ios) this%no_mode_contour
			if(ios /= 0)then
				!Something went wrong during value reading
				call report_error(ERR_OTHER,SEV_RETRY,&
					"Incorrect contouring specification for "//trim(this%job_name))
				go to 100
			end if
		end if
		
		pos= find_keyword("no_norm",tkws)
		if(pos /= 0)then
			read(tkws(pos)%value,*,iostat=ios) this%no_norm
			if(ios /= 0)then
				!Something went wrong during value reading
				call report_error(ERR_OTHER,SEV_RETRY,&
					"Incorrect norming specification for "//trim(this%job_name))
				go to 100
			end if
		end if
		
		pos= find_keyword("dipole_phi1",tkws)
		if(pos /= 0)then
			associate(an=> this%phi1)
				read(tkws(pos)%value,*,iostat=ios) an
				if(ios /= 0)then
					!Something went wrong during value reading
					call report_error(ERR_OTHER,SEV_RETRY,&
						"Incorrect dipole angle specification for "//trim(this%job_name))
					go to 100
				end if
				an= PI*an/180._pr
			end associate
		end if
		
		pos= find_keyword("dipole_theta1",tkws)
		if(pos /= 0)then
			associate(an=> this%theta1)
				read(tkws(pos)%value,*,iostat=ios) an
				if(ios /= 0)then
					!Something went wrong during value reading
					call report_error(ERR_OTHER,SEV_RETRY,&
						"Incorrect dipole angle specification for "//trim(this%job_name))
					go to 100
				end if
				an= PI*an/180._pr
			end associate
		end if
		
		pos= find_keyword("dipole_phi2",tkws)
		if(pos /= 0)then
			associate(an=> this%phi2)
				read(tkws(pos)%value,*,iostat=ios) an
				if(ios /= 0)then
					!Something went wrong during value reading
					call report_error(ERR_OTHER,SEV_RETRY,&
						"Incorrect dipole angle specification for "//trim(this%job_name))
					go to 100
				end if
				an= PI*an/180._pr
			end associate
		end if
		
		pos= find_keyword("dipole_theta2",tkws)
		if(pos /= 0)then
			associate(an=> this%theta2)
				read(tkws(pos)%value,*,iostat=ios) an
				if(ios /= 0)then
					!Something went wrong during value reading
					call report_error(ERR_OTHER,SEV_RETRY,&
						"Incorrect dipole angle specification for "//trim(this%job_name))
					go to 100
				end if
				an= PI*an/180._pr
			end associate
		end if
		
		pos= find_keyword("dipole_phi3",tkws)
		if(pos /= 0)then
			associate(an=> this%phi3)
				read(tkws(pos)%value,*,iostat=ios) an
				if(ios /= 0)then
					!Something went wrong during value reading
					call report_error(ERR_OTHER,SEV_RETRY,&
						"Incorrect dipole angle specification for "//trim(this%job_name))
					go to 100
				end if
				an= PI*an/180._pr
			end associate
		end if
		
		pos= find_keyword("dipole_theta3",tkws)
		if(pos /= 0)then
			associate(an=> this%theta3)
				read(tkws(pos)%value,*,iostat=ios) an
				if(ios /= 0)then
					!Something went wrong during value reading
					call report_error(ERR_OTHER,SEV_RETRY,&
						"Incorrect dipole angle specification for "//trim(this%job_name))
					go to 100
				end if
				an= PI*an/180._pr
			end associate
		end if
		
		pos= find_keyword("mode_width",tkws)
		if(pos /= 0)then
			associate(mw=> this%mw)
				read(tkws(pos)%value,*,iostat=ios) mw
				if(ios /= 0)then
					!Something went wrong during value reading
					call report_error(ERR_OTHER,SEV_RETRY,&
						"Incorrect mode width specification for "//trim(this%job_name))
					go to 100
				end if
				mw= mw+mod(mw+1,2)
				this%mhw= mw/2
			end associate
		end if
		
		pos= find_keyword("b_fact",tkws)
		if(pos /= 0)then
			read(tkws(pos)%value,*,iostat=ios) this%b_fact
			if(ios /= 0)then
				!Something went wrong during value reading
				call report_error(ERR_OTHER,SEV_RETRY,&
					"Incorrect b_fact specification for "//trim(this%job_name))
				go to 100
			end if
		end if
		
		pos= find_keyword("do_diag",tkws)
		if(pos /= 0)then
			read(tkws(pos)%value,*,iostat=ios) this%do_diag
			if(ios /= 0)then
				!Something went wrong during value reading
				call report_error(ERR_OTHER,SEV_RETRY,&
					"Incorrect do_diag specification for "//trim(this%job_name))
				go to 100
			end if
		end if
		
		pos= find_keyword("eff_refr_kind",tkws)
		if(pos /= 0)then
			read(tkws(pos)%value,*,iostat=ios) this%eff_refr_kind
			if(ios /= 0)then
				!Something went wrong during value reading
				call report_error(ERR_OTHER,SEV_RETRY,&
					"Incorrect eff_refr_kind specification for "//trim(this%job_name))
				go to 100
			end if
		end if
		
		pos= find_keyword("norm_3d",tkws)
		if(pos /= 0)then
			read(tkws(pos)%value,*,iostat=ios) this%norm_3d
			if(ios /= 0)then
				!Something went wrong during value reading
				call report_error(ERR_OTHER,SEV_RETRY,&
					"Incorrect norm_3d specification for "//trim(this%job_name))
				go to 100
			end if
		end if
		
		pos= find_keyword("max_im_kx",tkws)
		if(pos /= 0)then
			read(tkws(pos)%value,*,iostat=ios) this%max_im_kx
			if(ios /= 0)then
				!Something went wrong during value reading
				call report_error(ERR_OTHER,SEV_RETRY,&
					"Incorrect max_im_kx specification for "//trim(this%job_name))
				go to 100
			end if
		end if
		
		pos= find_keyword("ext_grid",tkws)
		if(pos /= 0)then
			read(tkws(pos)%value,*,iostat=ios) this%ext_grid_file
			if(ios /= 0)then
				!Something went wrong during value reading
				call report_error(ERR_OTHER,SEV_RETRY,&
					"Incorrect ext_grid specification for "//trim(this%job_name))
				go to 100
			end if
			this%ext_grid= .true.
			this%no_trim= .true.
			this%integrals= .false.
			call this%external_grid_init()
		end if
	end associate
	
	allocate(this%d(ubound(this%grid,1),ubound(this%grid,2)))
	allocate(this%diag(ubound(this%grid,1),ubound(this%grid,2)))
	allocate(this%r2TE(ubound(this%grid,1),ubound(this%grid,2)))
	allocate(this%t2TE,this%r2TM,this%t2TM, source=this%r2TE)
	allocate(this%prod_TE1(ubound(this%grid,2)))
	allocate(this%prod_TM1,this%prod_TE2,this%prod_TM2,source=this%prod_TE1)
	allocate(this%part_TE1(ubound(this%layers,1),ubound(this%grid,2)))
	allocate(this%part_TM1,this%part_TE2,this%part_TM2,source=this%part_TE1)
	allocate(this%l_row(this%l_left-1:this%l_right+1,ubound(this%grid,2)))
	
	if(this%integrals)then
		allocate(this%int_fact(ubound(this%grid,1)))
		select case (this%energy_unit)
		case (UNIT_E_EV)
			this%int_fact%energy= this%grid(:,1)%k0*HBARC
		case (UNIT_E_NM)
			this%int_fact%energy= this%wl_store
		end select
	end if
	!this%dKBdk0_TE(:,:)= 1._pr
	!this%dKBdk0_TM(:,:)= 1._pr
	
	status= STATUS_OK
	return
	
100	status= STATUS_ERROR
	return
	
	!Inherited error status
200	return
	
	end function init_purcell
	
	
	subroutine external_grid_init(this)
	class(job_purcell_t),intent(inout)		::	this
	integer									::	unit, ios, en_grid, kx_grid, i
	character(1024)							::	string,fmt
	real(pr)								::	energy, tmp(1000)
	
	deallocate(this%grid)
	en_grid=0;kx_grid=0
	open(newunit=unit,file=this%ext_grid_file,status='old',action='read',iostat=ios)
	if(ios/=0)then
		call report_error(ERR_OTHER,SEV_RETRY,&
					"External grid is fucked up for "//trim(this%job_name))
	end if
	
	do
		read(unit,*,iostat=ios)
		if(ios/=0)exit
		en_grid=en_grid+1
	end do
	rewind(unit)
	if(en_grid==0)then
		call report_error(ERR_OTHER,SEV_RETRY,&
					"External grid is empty for "//trim(this%job_name))
	end if
	read(unit,'(a)')string
	rewind(unit)
	do
		write(fmt,*)'(',kx_grid+2,'f)'
		read(string,*,iostat=ios)tmp(1:kx_grid+2)
		if(ios/=0)exit
		kx_grid= kx_grid+1
	end do
	if(kx_grid==0)then
		call report_error(ERR_OTHER,SEV_RETRY,&
					"External grid is kx-empty for "//trim(this%job_name))
	end if
	write(fmt,*)'(',kx_grid+1,'f)'
	allocate(this%grid(en_grid,kx_grid))
	do i=1,en_grid
		read(unit,*)energy,tmp(1:kx_grid)
		this%grid(i,:)%k0= energy/HBARC
		this%grid(i,:)%kx= tmp(1:kx_grid)
	end do
	close(unit)
	deallocate(this%r1TE,this%t1TE,this%r1TM,this%t1TM)
	allocate(this%r1TE(ubound(this%grid,1),ubound(this%grid,2)))
	allocate(this%t1TE,this%r1TM,this%t1TM, mold=this%r1TE)

	
	end subroutine external_grid_init
	
	
	subroutine driver_purcell(this)
	class(job_purcell_t),intent(inout)		::	this
	type(computed_layer_t),&
		dimension(&
		this%l_left-1:this%l_right+1,ubound(this%grid,2))				::	l_row
	type(matrix_t(2))						::	dipTE, dipTM
	integer									::	i, j, k
	type(un_layer_t)						::	aux_layer
	type(computed_layer_t)					::	aux_comp_layer
	type(vector_t(2)),dimension(2)			::	field_X, field_Y, field_Z
	type(vector_t(2)),dimension(2,&
		ubound(this%grid,2))				::	init_X, init_Y, init_Z
	complex(pr),dimension(2)				::	ev_X, ev_Y, ev_Z
	complex(pr)								::	factorZ, sinth, costh, tmp, tmp1
	logical									::	flag
	
	
	associate(	tl=> this%layers, tg=> this%grid, td=> this%d,&
				lbtl=> this%l_left-1, ubtl=> this%l_right+1, dpl=> this%dipole_layer,&
				tot_thick=> sum(this%layers(this%l_left:this%l_right)%thick), &
				sqrt_tot_thick=> sqrt(sum(this%layers(this%l_left:this%l_right)%thick)),&
				prod_TE1=> this%prod_TE1, prod_TM1=> this%prod_TM1, prod_TE2=> this%prod_TE2, prod_TM2=> this%prod_TM2,&
				part_TE1=> this%part_TE1, part_TM1=> this%part_TM1, part_TE2=> this%part_TE2, part_TM2=> this%part_TM2,&
				l_row=> this%l_row)
		
		energy:do i= lbound(tg,1),ubound(tg,1)
		!Cycle through energy
			!Assign refractive indices
			print *,i,' out of ',ubound(tg,1)
			layers:do j= lbtl,ubtl
				if(tl(j)%is_var_refr)then
					l_row(j,:)%n= tl(j)%n_func(tg(i,1))
				else
					l_row(j,:)%n= tl(j)%const_n
				end if
				if(this%no_absorption)then
					l_row(j,:)%n= sqrt(cmplx(real(l_row(j,:)%n**2),0._pr))
				end if
			end do layers
			if(tl(dpl)%is_var_refr)then
				tmp1= tl(dpl)%n_func(tg(i,1),tmp)
			end if
			this%diag(i,1)%eff_eps= l_row(dpl,1)%n**2+tg(i,1)%k0*HBARC*tmp
			!Assign transfer matrices
			kx:do j= lbound(tg,2),ubound(tg,2)
			!Cycle through kx
				layers:do k= lbtl+1,ubtl-1
				!Cycle through layers
					call tl(k)%TrMatTE_sub(l_row(k,j),tg(i,j))
					call tl(k)%TrMatTM_sub(l_row(k,j),tg(i,j))
				end do layers
				associate(	r1TE=> this%r1TE(i,j), r2TE=> this%r2TE(i,j),&
							r1TM=> this%r1TM(i,j), r2TM=> this%r2TM(i,j),&
							t1TE=> this%t1TE(i,j), t2TE=> this%t2TE(i,j),&
							t1TM=> this%t1TM(i,j), t2TM=> this%t2TM(i,j),&
							nrm=> td(i,j)%norm, fld=> td(i,j)%field, msk=> td(i,j)%mask, us=>td(i,j)%u,&
							diagrt=> this%diag(i,j)%rt)
					
					!Get product of TMs
					!Partial product from the end up to dipole layer
					part_TE1(ubtl,j)%m= reshape([1._pr,0._pr,0._pr,1._pr],[2,2])
					part_TM1(ubtl,j)%m= reshape([1._pr,0._pr,0._pr,1._pr],[2,2])
					do k= ubtl-1,lbtl+1,-1
						part_TE1(k,j)= l_row(k,j)%TrMatTE .dot. part_TE1(k+1,j)
						part_TM1(k,j)= l_row(k,j)%TrMatTM .dot. part_TM1(k+1,j)
					end do
					!Make dummy layer and dummy structure
					aux_layer= tl(dpl)
					aux_layer%thick= aux_layer%thick - this%dipole_z
					aux_comp_layer= l_row(dpl,j)
					call aux_layer%TrMatTE_sub(aux_comp_layer,tg(i,j))
					call aux_layer%TrMatTM_sub(aux_comp_layer,tg(i,j))
					dipTE= aux_comp_layer%TrMatTE .dot. part_TE1(dpl+1,j)
					dipTM= aux_comp_layer%TrMatTM .dot. part_TM1(dpl+1,j)
					prod_TE1(j)= part_TE1(lbtl+1,j)
					prod_TM1(j)= part_TM1(lbtl+1,j)
					
					!Field initialization
					if(this%s_quantization)then
						part_TE2(ubtl,j)%m= reshape([1._pr,0._pr,0._pr,1._pr],shape(part_TE2(ubtl,j)%m))
						part_TM2(ubtl,j)%m= reshape([1._pr,0._pr,0._pr,1._pr],shape(part_TM2(ubtl,j)%m))
						do k= ubtl-1,lbtl+1,-1
							part_TE2(k,j)= l_row(ubtl-k+lbtl,j)%TrMatTE .dot. part_TE2(k+1,j)
							part_TM2(k,j)= l_row(ubtl-k+lbtl,j)%TrMatTM .dot. part_TM2(k+1,j)
						end do
						prod_TE2(j)= part_TE2(lbtl+1,j)
						prod_TM2(j)= part_TM2(lbtl+1,j)
						call rta(prod_TE1(j),tg(i,j),l_row(lbtl,j),l_row(ubtl,j),POL_TE,&
								r1TE,t1TE)
						call rta(prod_TE2(j),tg(i,j),l_row(ubtl,j),l_row(lbtl,j),POL_TE,&
								r2TE,t2TE)
						call rta(prod_TM1(j),tg(i,j),l_row(lbtl,j),l_row(ubtl,j),POL_TM,&
								r1TM,t1TM)
						call rta(prod_TM2(j),tg(i,j),l_row(ubtl,j),l_row(lbtl,j),POL_TM,&
								r2TM,t2TM)
						diagrt= [r1TE,r2TE,t1TE,t2TE,r1TM,r2TM,t1TM,t2TM]
						if(is_waveguide(tg(i,j),l_row(:,j)))then
							call init_field_decay(prod_TM1(j),&
											layer=l_row([lbtl,ubtl],j),point=tg(i,j),direction=SQ_X,&
											ivp=init_X(1,j),ivm=init_X(2,j),&
											evp=ev_X(1),evm=ev_X(2),&
											u_p=us(1),u_m=us(2))
							call init_field_decay(prod_TE1(j),&
											layer=l_row([lbtl,ubtl],j),point=tg(i,j),direction=SQ_Y,&
											ivp=init_Y(1,j),ivm=init_Y(2,j),&
											evp=ev_Y(1),evm=ev_Y(2),&
											u_p=us(3),u_m=us(4))
							call init_field_decay(prod_TM1(j),&
											layer=l_row([lbtl,ubtl],j),point=tg(i,j),direction=SQ_Z,&
											ivp=init_Z(1,j),ivm=init_Z(2,j),&
											evp=ev_Z(1),evm=ev_Z(2),&
											u_p=us(5),u_m=us(6))	
						else
							call init_field_Sq(r1=r1TM,r2=r2TM,&
											t1=t1TM,t2=t2TM,&
											layer=l_row([lbtl,ubtl],j),point=tg(i,j),direction=SQ_X,&
											ivp=init_X(1,j),ivm=init_X(2,j),&
											evp=ev_X(1),evm=ev_X(2),&
											u_p=us(1),u_m=us(2))
							call init_field_Sq(r1=r1TE,r2=r2TE,&
											t1=t1TE,t2=t2TE,&
											layer=l_row([lbtl,ubtl],j),point=tg(i,j),direction=SQ_Y,&
											ivp=init_Y(1,j),ivm=init_Y(2,j),&
											evp=ev_Y(1),evm=ev_Y(2),&
											u_p=us(3),u_m=us(4))
							call init_field_Sq(r1=r1TM,r2=r2TM,&
											t1=t1TM,t2=t2TM,&
											layer=l_row([lbtl,ubtl],j),point=tg(i,j),direction=SQ_Z,&
											ivp=init_Z(1,j),ivm=init_Z(2,j),&
											evp=ev_Z(1),evm=ev_Z(2),&
											u_p=us(5),u_m=us(6))
						end if
						this%diag(i,j)%u_abs= abs(us)
					else
						!Periodic boundary conditions
						call init_field_bloch(ivp=init_Y(1,j),ivm=init_Y(2,j),polar=POL_TE,tm=prod_TE1(j),&
							mask=msk(3:4),Kbloch=td(i,j)%KBloch_TE,tl=tot_thick)
						call init_field_bloch(ivp=init_Z(1,j),ivm=init_Z(2,j),polar=POL_TM,tm=prod_TM1(j),&
							mask=msk(5:6),Kbloch=td(i,j)%KBloch_TM,tl=tot_thick)
						init_X= init_Z
						msk(1:2)= msk(5:6)
						associate(	dte=> dTrMdw(tg(i,j),part_TE1(lbtl+1:ubtl-1,j),l_row(lbtl+1:ubtl-1,j),tl(lbtl+1:ubtl-1),POL_TE),&
									dtm=> dTrMdw(tg(i,j),part_TM1(lbtl+1:ubtl-1,j),l_row(lbtl+1:ubtl-1,j),tl(lbtl+1:ubtl-1),POL_TM),&
									te=> prod_TE1(j), tm=> prod_TM1(j))
							td(i,j)%dKBdk0_TE= -(dte%m(1,1)+dte%m(2,2))/2._pr/sqrt(1._pr-(te%m(1,1)+te%m(2,2))**2/4._pr)/tot_thick
							td(i,j)%dKBdk0_TM= -(dtm%m(1,1)+dtm%m(2,2))/2._pr/sqrt(1._pr-(tm%m(1,1)+tm%m(2,2))**2/4._pr)/tot_thick
						end associate
					end if
					!Get field in the dipole point
					field_Y(1)= dipTE .dot. init_Y(1,j); field_Y(2)= dipTE .dot. init_Y(2,j)
					field_X(1)= dipTM .dot. init_X(1,j); field_X(2)= dipTM .dot. init_X(2,j)
					field_Z(1)= dipTM .dot. init_Z(1,j); field_Z(2)= dipTM .dot. init_Z(2,j)
				
					!Assign masks and non-WG norms
					if(this%s_quantization)then
						associate(n0=> l_row(lbtl,j)%n, n1=> l_row(ubtl,j)%n)
							sinth= tg(i,j)%kx/(tg(i,j)%k0*n1)
							costh= sqrt(1._pr - sinth**2)
							if(is_waveguide(tg(i,j),l_row(:,j),near=flag,factor=this%b_fact))then
								if(flag)then
									msk(:)= MASK_DENY
								else
									msk(:)= MASK_NO_MODE
									if(this%no_trim .and. .not. this%no_norm)then
										call this%wg_norms(i, j, l_row, init_X, init_Y, init_Z, tails=this%ext_grid)
									else
										nrm= 1._pr
									end if
								end if
							else
								this%d(i,j)%mask(:)= MASK_NO_WG
								nrm(1)= costh**2*Sq_3D_norm(vec=ev_X(1), r1=r1TM, r2=r2TM, t1=t1TM, t2=t2TM, n0=n0, n1=n1, complete=this%norm_3d)
								nrm(2)= costh**2*Sq_3D_norm(vec=ev_X(2), r1=r1TM, r2=r2TM, t1=t1TM, t2=t2TM, n0=n0, n1=n1, complete=this%norm_3d)
								nrm(3)= Sq_3D_norm(vec=ev_Y(1), r1=r1TE, r2=r2TE, t1=t1TE, t2=t2TE, n0=n0, n1=n1, complete=this%norm_3d)
								nrm(4)= Sq_3D_norm(vec=ev_Y(2), r1=r1TE, r2=r2TE, t1=t1TE, t2=t2TE, n0=n0, n1=n1, complete=this%norm_3d)
								nrm(5)= (n1)**2*Sq_3D_norm(vec=ev_Z(1), r1=r1TM, r2=r2TM, t1=t1TM, t2=t2TM, n0=n0, n1=n1, complete=this%norm_3d)
								nrm(6)= (n1)**2*Sq_3D_norm(vec=ev_Z(2), r1=r1TM, r2=r2TM, t1=t1TM, t2=t2TM, n0=n0, n1=n1, complete=this%norm_3d)
							end if
						end associate
					else
						if(this%no_norm)then
							nrm=2._pr
						else
							nrm(1)= 2._pr/tot_thick*Sq_WG_norm(init=init_X(1,j),tms=part_TM1(lbtl+1:ubtl,j),&
											s_type=BL_X,pt=tg(i,j),&
											c_layers=l_row(lbtl:ubtl,j),layers=tl(lbtl:ubtl),tails=.false.)
							nrm(2)= 2._pr/tot_thick*Sq_WG_norm(init=init_X(2,j),tms=part_TM1(lbtl+1:ubtl,j),&
											s_type=BL_X,pt=tg(i,j),&
											c_layers=l_row(lbtl:ubtl,j),layers=tl(lbtl:ubtl),tails=.false.)
							nrm(3)= 2._pr/tot_thick*Sq_WG_norm(init=init_Y(1,j),tms=part_TE1(lbtl+1:ubtl,j),&
											s_type=BL_Y,pt=tg(i,j),&
											c_layers=l_row(lbtl:ubtl,j),layers=tl(lbtl:ubtl),tails=.false.)
							nrm(4)= 2._pr/tot_thick*Sq_WG_norm(init=init_Y(2,j),tms=part_TE1(lbtl+1:ubtl,j),&
											s_type=BL_Y,pt=tg(i,j),&
											c_layers=l_row(lbtl:ubtl,j),layers=tl(lbtl:ubtl),tails=.false.)
							nrm(5)= 2._pr/tot_thick*Sq_WG_norm(init=init_Z(1,j),tms=part_TM1(lbtl+1:ubtl,j),&
											s_type=BL_Z,pt=tg(i,j),&
											c_layers=l_row(lbtl:ubtl,j),layers=tl(lbtl:ubtl),tails=.false.)
							nrm(6)= 2._pr/tot_thick*Sq_WG_norm(init=init_Z(2,j),tms=part_TM1(lbtl+1:ubtl,j),&
											s_type=BL_Z,pt=tg(i,j),&
											c_layers=l_row(lbtl:ubtl,j),layers=tl(lbtl:ubtl),tails=.false.)
						end if
					end if
					
					!Apply norm
					associate (fields=> [field_X(1)%v(2),field_X(2)%v(2),field_Y(1)%v(1),field_Y(2)%v(1),field_Z(1)%v(1),field_Z(2)%v(1)])
						fld= abs(fields**2/nrm)
						this%diag(i,j)%norm= nrm
					end associate
				end associate
			end do kx
			
			!Integral factor (only for S-quantization)
			if(this%integrals)then
			intsquant:if(this%s_quantization)then
				!Non-waveguide integral
				call this%int_light_cone(i,l_row,MASK_NO_WG)
				!Waveguide integral	and masks
				if(this%no_trim)then
					call this%int_light_cone(i,l_row,MASK_NO_MODE)
				else					
					call this%wg_masks(i, l_row, init_X, init_Y, init_Z)
				end if
			else
				call this%int_light_cone(i,l_row,MASK_BLOCH)
			end if intsquant
			end if
		end do energy
		
		intsquant2:if(this%s_quantization .and. .not. this%no_trim)then
			!Effective refractive index adjustment
			!call this%eff_refr_adjust()
			call this%wg_renorm()
			!Integral out of the light cone, summation stage
			do i= lbound(tg,1),ubound(tg,1)
				do j=1,3
					this%int_fact(i)%mode_num(j)= maxval(td(i,:)%mask(2*j-1))+&
											maxval(td(i,:)%mask(2*j))
				end do
				!VALUE CHECK
				this%int_fact(i)%vals(5)= sum(td(i,:)%field(1),mask=td(i,:)%mask(1)>MASK_NO_MODE)&
											+sum(td(i,:)%field(2),mask=td(i,:)%mask(2)>MASK_NO_MODE)
				this%int_fact(i)%vals(4)= sum(td(i,:)%field(3),mask=td(i,:)%mask(3)>MASK_NO_MODE)&
											+sum(td(i,:)%field(4),mask=td(i,:)%mask(4)>MASK_NO_MODE)
				this%int_fact(i)%vals(6)= sum(td(i,:)%field(5),mask=td(i,:)%mask(5)>MASK_NO_MODE)&
											+sum(td(i,:)%field(6),mask=td(i,:)%mask(6)>MASK_NO_MODE)
			end do
		end if intsquant2
		
	end associate
	
	end subroutine driver_purcell
	
		
	!Writing all to a file
	subroutine output_purcell(this)
	class(job_purcell_t),intent(in)			::	this
	type(output_purcell_t),dimension&
		(lbound(this%grid,1):&
		ubound(this%grid,1),&
		lbound(this%grid,2):&
		ubound(this%grid,2))				::	out_arr
	type(diag_purcell_t),dimension&
		(lbound(this%grid,1):&
		ubound(this%grid,1),&
		lbound(this%grid,2):&
		ubound(this%grid,2))				::	diag_arr
	integer									::	unit, rl, i, j, index
	
	diag_arr= this%diag
	select case (this%energy_unit)
	case (UNIT_E_EV)
		out_arr%energy= this%grid%k0*HBARC
	case (UNIT_E_NM)
		out_arr%energy= spread(this%wl_store,2,size(this%grid,2))
	end select
	select case (this%lat_unit)
	case (UNIT_LAT_KX_NM)
		out_arr%lateral= this%grid%kx
	case (UNIT_LAT_DEGREE)
		out_arr%lateral= spread(this%angle_store,1,size(this%grid,1))
	end select
	diag_arr%energy= out_arr%energy
	diag_arr%lateral= out_arr%lateral
	out_arr%x1= this%d%field(1); out_arr%x2= this%d%field(2)
	out_arr%y1= this%d%field(3); out_arr%y2= this%d%field(4)
	out_arr%z1= this%d%field(5); out_arr%z2= this%d%field(6)
	out_arr%mask_x1= this%d%mask(1); out_arr%mask_x2= this%d%mask(2)
	out_arr%mask_y1= this%d%mask(3); out_arr%mask_y2= this%d%mask(4)
	out_arr%mask_z1= this%d%mask(5); out_arr%mask_z2= this%d%mask(6)
	out_arr%KBloch_TE= this%d%KBloch_TE; out_arr%KBloch_TM= this%d%KBloch_TM
	out_arr%dKBdk0_TE= this%d%dKBdk0_TE; out_arr%dKBdk0_TM= this%d%dKBdk0_TM
	do i=1,6
		out_arr%n_eff(i)= this%d%n_eff(i)
	end do
	out_arr%n_eff(1)= this%d%dKBdk0_TE*sqrt(this%grid%k0**2-this%grid%kx**2)/this%grid%k0
	out_arr%n_eff(2)= 1100
	inquire(iolength=rl)output_purcell_t()
	open(newunit=unit,file=trim(this%job_name)//".out",form="unformatted",status="replace",&
		access="direct",recl=rl)
	index= 1
	write(unit,rec=index)TASK_PURCELL,rl,size(out_arr,1),size(out_arr,2)
	do i=lbound(out_arr,1),ubound(out_arr,1)
		do j=lbound(out_arr,2),ubound(out_arr,2)
			index=index+1
			write(unit,rec=index)out_arr(i,j)
		end do
	end do
	close(unit)
	if(this%integrals)then
		inquire(iolength=rl)int_fact_t()
		open(newunit=unit,file=trim(this%job_name)//".out_aux",form="unformatted",status="replace",&
			access="direct",recl=rl)
		index= 1
		do i=lbound(this%int_fact,1),ubound(this%int_fact,1)
			write(unit,rec=index)this%int_fact(i)
			index=index+1
		end do
		close(unit)
	end if
	if(this%do_diag)then
		inquire(iolength=rl)diag_purcell_t()
		open(newunit=unit,file=trim(this%job_name)//".out_diag",form="unformatted",status="replace",&
			access="direct",recl=rl)
		index= 0
		do i=lbound(diag_arr,1),ubound(diag_arr,1)
			do j=lbound(diag_arr,2),ubound(diag_arr,2)
				index=index+1
				write(unit,rec=index)diag_arr(i,j)
			end do
		end do
		close(unit)
	end if
	
	end subroutine output_purcell
	
	
	!Return a pointer to a Purcell task
	function make_purcell(job)result(res)
	type(job_purcell_t),pointer				::	res
	type(job_t),intent(in)					::	job
	
	allocate(res)
	res%job_t= job
	if(res%init() /= STATUS_OK)then
		deallocate(res)
		nullify(res)
	end if
	
	end function make_purcell
	
	
	end module job_purcell