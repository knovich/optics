	module error_report
	!Generic error and messages reporter
	
	implicit none

	!Error codes
	integer,parameter::ERR_OTHER= -2
	integer,parameter::ERR_USER= -1
	integer,parameter::ERR_OK= 0
	integer,parameter::ERR_NO_FILE= 1						!No input file found
	integer,parameter::ERR_EOF= 2							!Unexpected end of file
	integer,parameter::ERR_UNKNOWN_KEYWORD= 3
	integer,parameter::ERR_UNKNOWN_SECTION= 4
	integer,parameter::ERR_UNMATCHED_SECTION= 5				!Unexpected %section statement or incorrect %end section statement
	integer,parameter::ERR_INCONSISTENT_TASK= 6
	integer,parameter::ERR_INCORRECT_COMMON_BLOCK= 7		!Duplicate or late common block
	integer,parameter::ERR_NO_JOBS= 8
	integer,parameter::ERR_DUPLICATE_KEYWORD= 9				!Duplicate keyword within block
	integer,parameter::ERR_REDEFINED_KEYWORD= 10
	integer,parameter::ERR_VALUE= 11						!Incorrect keyword value
	integer,parameter::ERR_EMPTY_JOB= 12
	integer,parameter::ERR_STRUCTURE= 13					!Some error with structure
	integer,parameter::ERR_NOENERGY= 14						!No energy specified
	integer,parameter::ERR_NOLAT= 15						!No lateral dimension specified
	integer,parameter::ERR_MATERIAL= 16						!Unknown material name
	integer,parameter::ERR_NODIP= 17						!No dipole layer
	integer,parameter::ERR_NODIPZ= 18						!No dipole coordinate
	integer,parameter::ERR_NOPRPOINTS= 19					!No profile points
	integer,parameter::ERR_NOSQ= 20							!Incorrect S-quantization modifier

	!Severity codes
	integer,parameter::SEV_MSG= 0							!Message
	integer,parameter::SEV_WARN= 1							!Non-critical error (unknown keyword, etc)
	integer,parameter::SEV_RETRY= 2							!Retry user interaction or procedure
	integer,parameter::SEV_ABORT= 3							!Critical error, abort execution

	
	contains
	
	
	!Report error and terminate in case of SEV_ABORT errors
	subroutine report_error(code, severity, message)
	integer,intent(in)						::	code
	integer,intent(in)						::	severity
	character(*),optional,intent(in)		::	message
	character(256)							::	msg

	if(.not. present(message))then
		msg= ""
	else
		msg= message
	end if
	select case(severity)
	case (SEV_MSG)
		print *,"Message: '",trim(msg),"', code: ", code

	case (SEV_WARN)
		print *,"Warning: '",trim(msg),"', code: ", code

	case (SEV_RETRY)
		print *,"Retry: '",trim(msg),"', code: ", code
		read *
		error stop 1

	case (SEV_ABORT)
		print *,"Program aborted: '",trim(msg),"', code: ", code
		print *,"Hit <Enter> to exit"
		read *
		error stop 2
	
	end select

	end subroutine report_error
	
	
	!Ask for retry and terminate execution if user doesn't want to
	subroutine ask_retry()
	character(1)							::	ans
	
	print *,"Would you like to retry? [y]/n"
	read '(a)',ans
	if(ans == 'n' .or. ans == 'N')then
		call report_error(ERR_USER,SEV_ABORT,message="Aborted by user")
		read *
		error stop 3
	end if

	end subroutine ask_retry
	
	
	end module error_report