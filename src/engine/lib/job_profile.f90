	module job_profile
	!Profile (for S-quantization)
	
	use types
	use keywords
	use math
	use error_report
	
	implicit none
	
	private
	
	public :: make_profile
	
	type:: output_profile_t
		real(po)				::	z = 0.
		complex(po)				::	n = 0.
		type(vector_t(3))		::	field_TE_plus, field_TE_minus,&
									field_TM_plus, field_TM_minus
	end type output_profile_t
	
	type,extends(computation_t):: job_profile_t
		logical					::	s_quantization = .true.
		integer					::	l_left =1, l_right =0
		type(grid_point_t)		::	point
		integer					::	energy_unit	= UNIT_E_EV,&
									lat_unit	= UNIT_LAT_KX_NM
		integer					::	points = 0
		type(output_profile_t),&
			dimension(:),&
			allocatable			::	out_fields
		integer					::	total_pts = 0
		logical					::	no_absorption	= .false.
		logical					::	sq_norm			= .false.
	contains
		procedure				::	init=>		init_profile
		procedure				::	driver=>	driver_profile
		procedure				::	output=>	output_profile
	end type job_profile_t
	
	
	contains
	
	
	!Init
	function init_profile(this)result(status)
	integer									::	status
	class(job_profile_t),intent(inout)		::	this
	integer									::	pos, ios
	character(MAX_NAME_LEN)					::	unit
	complex(pr)								::	tmp_en, tmp_kx_compl
	real(pr)								::	tmp_ang, tmp_kx, tmp_en_re, tmp_en_im
	complex(pr)								::	n0
	
	pos= 0; ios= 0; unit= ""; tmp_en= 0.; tmp_kx= 0.; tmp_ang= 0.; tmp_en_re= 0.; tmp_en_im= 0.; n0= 0.
	associate(tkws=> this%keywords, sq=> this%s_quantization)
		pos= find_keyword("s_quantization",tkws)
		squant:if(pos /= 0)then
			associate (kw=> tkws(pos))
				read(kw%value,*,iostat=ios) sq
				if(ios /= 0)then
					!Something went wrong during value reading
					call report_error(ERR_NOSQ,SEV_RETRY,&
						"Incorrect S-quantization specification for "//trim(this%job_name))
					go to 100
				end if
			end associate
		else
			!Default to S-quantization
			sq= .true.
		end if squant
		
		pos= find_keyword("left_layer",tkws)
		if(pos /= 0)then
			read(tkws(pos)%value,*,iostat=ios) this%l_left
			if(ios /= 0 .or. this%l_left <= lbound(this%layers,1) .or. this%l_left >= ubound(this%layers,1))then
				!Something went wrong during value reading
				call report_error(ERR_OTHER,SEV_RETRY,&
					"Incorrect left_layer specification for "//trim(this%job_name))
				go to 100
			end if
		end if
		
		pos= find_keyword("right_layer",tkws)
		if(pos /= 0)then
			read(tkws(pos)%value,*,iostat=ios) this%l_right
			if(ios /= 0 .or. this%l_right <= lbound(this%layers,1) .or. this%l_right >= ubound(this%layers,1))then
				!Something went wrong during value reading
				call report_error(ERR_OTHER,SEV_RETRY,&
					"Incorrect right_layer specification for "//trim(this%job_name))
				go to 100
			end if
		else
			this%l_right= ubound(this%layers,1)-1
		end if
		
		if(this%l_left > this%l_right)then
			call report_error(ERR_OTHER,SEV_RETRY,&
				"Right layer is earlier than left in "//trim(this%job_name))
			go to 100
		end if
		
		pos= find_keyword("energy",tkws)
		energy:if(pos /= 0)then
			associate (kw=> tkws(pos))
				select case (kw%key)
				case ("energy")
					read(kw%value,*,iostat=ios) unit
					if(ios /= 0 .or. verify(trim(unit),NUMCHARS) == 0)then
						!No unit specified, assuming eV
						if(sq)then
							read(kw%value,*,iostat=ios) tmp_en_re
							tmp_en%re= tmp_en_re
						else
							read(kw%value,*,iostat=ios) tmp_en
						end if
					else
						select case (unit)
						case ("eV")
							this%energy_unit= UNIT_E_EV
						case ("nm")
							this%energy_unit= UNIT_E_NM
						case default
							go to 100
						end select
						if(sq)then
							read(kw%value,*,iostat=ios) unit, tmp_en_re
							tmp_en%re= tmp_en_re
						else
							read(kw%value,*,iostat=ios) unit, tmp_en
						end if
					end if
				case default
					go to 100
				end select
				if(ios /= 0)then
					!Something went wrong during value reading
					call report_error(ERR_NOENERGY,SEV_RETRY,&
						"Incorrect energy specification for "//trim(this%job_name))
					go to 100
				end if
			end associate
		else
			call report_error(ERR_NOENERGY,SEV_RETRY,"No energy specified for "//trim(this%job_name))
			go to 100
		end if energy
		
		pos= find_keyword("kx",tkws)
		lateral:if(pos /= 0)then
			associate (kw=> tkws(pos))
				select case (kw%key)
				case ("angle")
					this%lat_unit= UNIT_LAT_DEGREE
					read(kw%value,*,iostat=ios) tmp_ang
				case ("kx")
					this%lat_unit= UNIT_LAT_KX_NM
					read(kw%value,*,iostat=ios) tmp_kx_compl
					if(ios/=0)then
						read(kw%value,*,iostat=ios) tmp_kx
						tmp_kx_compl= tmp_kx
					else
						print *,'Complex kx ',tmp_kx_compl
					end if
				case default
					go to 100
				end select
				if(ios /= 0)then
					!Something went wrong during value reading
					call report_error(ERR_NOLAT,SEV_RETRY,&
						"Incorrect lateral dimension specification for "//trim(this%job_name))
					go to 100
				end if
			end associate
		else
			call report_error(ERR_NOLAT,SEV_RETRY,&
				"No lateral dimension specified for "//trim(this%job_name))
			go to 100
		end if lateral

		pos= find_keyword("profile_points",tkws)
		points:if(pos /= 0)then
			associate (kw=> tkws(pos))
				read(kw%value,*,iostat=ios) this%points
				if(ios /= 0 .or. this%points < 0)then
					!Something went wrong during value reading
					call report_error(ERR_NOPRPOINTS,SEV_RETRY,&
						"Incorrect profile points number specification for "//trim(this%job_name))
					go to 100
				end if
			end associate
		else
			call report_error(ERR_NOPRPOINTS,SEV_RETRY,&
				"No profile points number specified for "//trim(this%job_name))
			go to 100
		end if points
		
		pos= find_keyword("no_absorption",tkws)
		if(pos /= 0)then
			read(tkws(pos)%value,*,iostat=ios) this%no_absorption
			if(ios /= 0)then
				!Something went wrong during value reading
				call report_error(ERR_OTHER,SEV_RETRY,&
					"Incorrect absorption specification for "//trim(this%job_name))
				go to 100
			end if
		end if
		
		pos= find_keyword("sq_norm",tkws)
		if(pos /= 0)then
			read(tkws(pos)%value,*,iostat=ios) this%sq_norm
			if(ios /= 0)then
				!Something went wrong during value reading
				call report_error(ERR_OTHER,SEV_RETRY,&
					"Incorrect sq_norm specification for "//trim(this%job_name))
				go to 100
			end if
		end if
		
	end associate
	
	this%total_pts = this%points+ubound(this%layers,1)-2
	allocate(this%out_fields(this%total_pts))
	
	associate(tp=> this%point)
		!Assign grid values
		select case (this%energy_unit)
		case (UNIT_E_EV)
			tp%k0= tmp_en/HBARC
		case (UNIT_E_NM)
			tp%k0= 2.*PI/tmp_en
		end select
		select case (this%lat_unit)
		case (UNIT_LAT_KX_NM)
			tp%kx= tmp_kx_compl
		case (UNIT_LAT_DEGREE)
			!Need to calculate kx based on the first layer refractivity
			select case (this%layers(0)%is_var_refr)
			case (.true.)
				n0= this%layers(0)%n_func(tp)
			case (.false.)
				n0= this%layers(0)%const_n
			end select
			tp%kx= get_kx(tp%k0,n0,tmp_ang)
		end select
	end associate
	
	status= STATUS_OK
	return
	
100	status= STATUS_ERROR
	return
	
	end function init_profile
	

	!Main process
	subroutine driver_profile(this)
	class(job_profile_t),intent(inout)		::	this
	type(computed_layer_t),&
		dimension&
		(lbound(this%layers,1)&
		:ubound(this%layers,1))				::	l_row
	type(computed_layer_t)					::	dummy
	type(un_layer_t)						::	dummy_l
	type(matrix_t(2)),&
		dimension(this%total_pts)			::	prod_TE, prod_TM
	integer									::	i,j,k,&
												prev_layer, nl
	real(pr),dimension&
		(0:ubound(this%layers,1)-1)&
											::	part_len
	type(matrix_t(2)),dimension&
		(ubound(this%layers,1))				::	part_TE, part_TM
	real(pr),dimension(this%points)			::	coords
	type(vector_t(2)),&
		dimension(this%total_pts,6)			::	calc
	complex(pr)								::	r1TE,r2TE,t1TE,t2TE,&
												r1TM,r2TM,t1TM,t2TM
	complex(pr)								::	n0,n1,sinth,costh
	complex(pr)								::	nrm(6) = 1._pr
	complex(pr),dimension(this%total_pts)	::	factorZ
	complex(pr),dimension(2)				::	ev_X, ev_Y, ev_Z
		
	
	associate(tl=> this%layers, tp=> this%point, pts=> this%points, total_pts=> this%total_pts, ou=> this%out_fields,&
				lbtl=> this%l_left-1, ubtl=> this%l_right+1)
		
		part_len= [0._pr,(sum(tl(1:i)%thick),i=1,ubtl-1)]
		coords= interpolate_grid(0._pr,part_len(ubtl-1),pts)
		do j= lbtl,ubtl
			!Assign refractive indices
			if(tl(j)%is_var_refr)then
				l_row(j)%n= tl(j)%n_func(tp)
			else
				l_row(j)%n= tl(j)%const_n
			end if
			if(this%no_absorption)then
				l_row(j)%n= sqrt(cmplx(real(l_row(j)%n**2),0._pr))
			end if
		end do
		n0= l_row(lbtl)%n
		n1= l_row(ubtl)%n
		do j= lbtl+1,ubtl-1
			!Assign transfer matrices
			call tl(j)%TrMatTE_sub(l_row(j),tp)
			call tl(j)%TrMatTM_sub(l_row(j),tp)
		end do
		part_TE(ubtl)%m= reshape([1._pr,0._pr,0._pr,1._pr],shape(part_TE(ubtl)%m))
		part_TM(ubtl)%m= reshape([1._pr,0._pr,0._pr,1._pr],shape(part_TM(ubtl)%m))
		do i= ubtl-1,lbtl+1,-1
			part_TE(i)= l_row(i)%TrMatTE .dot. part_TE(i+1)
			part_TM(i)= l_row(i)%TrMatTM .dot. part_TM(i+1)
		end do
		prev_layer= ubtl
		j= total_pts
		do i= pts-1,2,-1
			nl= get_layer(coords(i))
			do while (nl < prev_layer)
				prod_TE(j)= part_TE(prev_layer)
				prod_TM(j)= part_TM(prev_layer)
				ou(j)%z= part_len(prev_layer-1)
				ou(j)%n= l_row(prev_layer-1)%n
				prev_layer= prev_layer-1;
				j= j-1
			end do
			dummy_l= tl(nl)
			dummy_l%thick= part_len(nl)-coords(i)
			dummy= l_row(nl)
			call dummy_l%TrMatTE_sub(dummy,tp)
			call dummy_l%TrMatTM_sub(dummy,tp)
			prod_TE(j)= dummy%TrMatTE .dot. part_TE(prev_layer+1)
			prod_TM(j)= dummy%TrMatTM .dot. part_TM(prev_layer+1)
			ou(j)%z= coords(i)
			ou(j)%n= dummy%n
			j= j-1
		end do
		ou(1)%z= coords(1)
		
		ou(1)%n= l_row(1)%n
		prod_TE(1)= part_TE(1)
		prod_TM(1)= part_TM(1)
		!Initialize fields
		if(this%s_quantization)then
			call rta(part_TE(1),tp,l_row(lbtl),l_row(ubtl),POL_TE,r1TE,t1TE)
			call rta(part_TM(1),tp,l_row(lbtl),l_row(ubtl),POL_TM,r1TM,t1TM)
			call rta(mat_prod(l_row(ubtl-1:lbtl+1:-1)%TrMatTE),tp,l_row(ubtl),&
				l_row(lbtl),POL_TE,r2TE,t2TE)
			call rta(mat_prod(l_row(ubtl-1:lbtl+1:-1)%TrMatTM),tp,l_row(ubtl),&
				l_row(lbtl),POL_TM,r2TM,t2TM)
			
			if(is_waveguide(tp,l_row(:)))then 
				call init_field_decay(part_TE(1),l_row([lbtl,ubtl]),tp,SQ_Y,&
					calc(total_pts,1),calc(total_pts,2))
				call init_field_decay(part_TM(1),l_row([lbtl,ubtl]),tp,SQ_X,&
					calc(total_pts,3),calc(total_pts,4))
				call init_field_decay(part_TM(1),l_row([lbtl,ubtl]),tp,SQ_Z,&
					calc(total_pts,5),calc(total_pts,6))
			else
				call init_field_Sq(r1TE,r2TE,t1TE,t2TE,l_row([lbtl,ubtl]),tp,SQ_Y,&
					calc(total_pts,1),calc(total_pts,2),evp=ev_Y(1),evm=ev_Y(2))
				call init_field_Sq(r1TM,r2TM,t1TM,t2TM,l_row([lbtl,ubtl]),tp,SQ_X,&
					calc(total_pts,3),calc(total_pts,4),evp=ev_X(1),evm=ev_X(2))
				call init_field_Sq(r1TM,r2TM,t1TM,t2TM,l_row([lbtl,ubtl]),tp,SQ_Z,&
					calc(total_pts,5),calc(total_pts,6),evp=ev_Z(1),evm=ev_Z(2))
				if(this%sq_norm)then
					sinth= tp%kx/(tp%k0*n1)
					costh= sqrt(1._pr - sinth**2)
					nrm(1)= Sq_3D_norm(vec=ev_Y(1), r1=r1TE, r2=r2TE, t1=t1TE, t2=t2TE, n0=n0, n1=n1)
					nrm(2)= Sq_3D_norm(vec=ev_Y(2), r1=r1TE, r2=r2TE, t1=t1TE, t2=t2TE, n0=n0, n1=n1)
					nrm(3)= costh**2*Sq_3D_norm(vec=ev_X(1), r1=r1TM, r2=r2TM, t1=t1TM, t2=t2TM, n0=n0, n1=n1)
					nrm(4)= costh**2*Sq_3D_norm(vec=ev_X(2), r1=r1TM, r2=r2TM, t1=t1TM, t2=t2TM, n0=n0, n1=n1)
					nrm(5)= (n1)**2*Sq_3D_norm(vec=ev_Z(1), r1=r1TM, r2=r2TM, t1=t1TM, t2=t2TM, n0=n0, n1=n1)
					nrm(6)= (n1)**2*Sq_3D_norm(vec=ev_Z(2), r1=r1TM, r2=r2TM, t1=t1TM, t2=t2TM, n0=n0, n1=n1)
				end if
			end if
		else
			call init_field_bloch(ivp=calc(total_pts,1),ivm=calc(total_pts,2),polar=POL_TE,tm=part_TE(1))
			call init_field_bloch(ivp=calc(total_pts,3),ivm=calc(total_pts,4),polar=POL_TM,tm=part_TM(1))
			calc(total_pts,5:6)= calc(total_pts,3:4)
			!calc(total_pts,6)= calc(total_pts,4)
		end if
		!Compute profile
		do i=1,total_pts-1
			calc(i,1)= prod_TE(i) .dot. calc(total_pts,1)
			calc(i,2)= prod_TE(i) .dot. calc(total_pts,2)
			calc(i,3)= prod_TM(i) .dot. calc(total_pts,3)
			calc(i,4)= prod_TM(i) .dot. calc(total_pts,4)
			calc(i,5)= prod_TM(i) .dot. calc(total_pts,5)
			calc(i,6)= prod_TM(i) .dot. calc(total_pts,6)
		end do
		do i=1,6
			calc(:,i)=calc(:,i)/sqrt(nrm(i))
		end do
		do i=1,total_pts
			!Assign aux Ez for TM [and Hz for TE]
			!TODO implement Hz here
			ou(i)%field_TE_plus= vector_t(3)([calc(i,1)%v(1),calc(i,1)%v(2),(0._pr,0._pr)])
			ou(i)%field_TE_minus= vector_t(3)([calc(i,2)%v(1),calc(i,2)%v(2),(0._pr,0._pr)])
			factorZ(i)= -tp%kx/(tp%k0*ou(i)%n)
			ou(i)%field_TM_plus= vector_t(3)([calc(i,5)%v(1),calc(i,3)%v(2),calc(i,5)%v(1)*factorZ(i)])
			ou(i)%field_TM_minus= vector_t(3)([calc(i,6)%v(1),calc(i,4)%v(2),calc(i,6)%v(1)*factorZ(i)])
		end do
	end associate
	
	contains
	
	integer function get_layer(z)
	real(pr),intent(in)						::	z
	
	get_layer= 0
	do while (part_len(get_layer) < z)
		get_layer= get_layer+1
	end do
	
	end function get_layer
	
	end subroutine driver_profile
	
	
	!Writing all to file
	subroutine output_profile(this)
	class(job_profile_t),intent(in)			::	this
	integer									::	unit, rl, index, i
	type(output_profile_t)					::	ou
	
	
	inquire(iolength=rl)this%out_fields(1)%field_TE_plus%v(1)
	rl= 13*rl+rl/2
	open(newunit=unit,file=trim(this%job_name)//".out",form="unformatted",status="replace",&
		access="direct",recl=rl)
	index= 1
	associate(out_arr=> this%out_fields)
		write(unit,rec=index)TASK_PROFILE,rl,size(out_arr,1),1
		do i=lbound(out_arr,1),ubound(out_arr,1)
			index=index+1
			write(unit,rec=index)out_arr(i)%z,out_arr(i)%n,&
				out_arr(i)%field_TE_plus%v, out_arr(i)%field_TE_minus%v,&
				out_arr(i)%field_TM_plus%v, out_arr(i)%field_TM_minus%v
		end do
	end associate
	close(unit)
	
	end subroutine output_profile
	
	
	!Return a pointer to a profile task
	function make_profile(job)result(res)
	type(job_profile_t),pointer				::	res
	type(job_t),intent(in)					::	job
	
	allocate(res)
	res%job_t= job
	if(res%init() /= STATUS_OK)then
		deallocate(res)
		nullify(res)
	end if
	
	end function make_profile
	
	end module job_profile