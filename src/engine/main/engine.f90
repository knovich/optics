	program engine
	use types
	use input_parser
	use job_control
	implicit none
	
	integer st,i
	type(input_block_t),dimension(:),allocatable::ibs
	type(job_t),dimension(:),allocatable::jobs,tmp
	type(computation_p),dimension(:),allocatable::arr
	character(MAX_FILE_NAME)::infile,cmd,arg_wait
	logical::flag_wait


	call get_command(cmd)
	print *,trim(cmd)
	call get_command_argument(1,value=infile,status=st)
	if(.not.st==0)then
		!No input file name supplied
		write(*,'(a)',advance='no')"Input file:"
		read *,infile
		flag_wait= .true.
	else
		flag_wait= .false.
	end if
	call get_command_argument(2,value=arg_wait,status=st)
	if(st==0)then
		if(trim(arg_wait)=='wait')then
			flag_wait= .true.
		else
			flag_wait= .false.
		end if
	end if
	call parse_input(infile,ibs,st)
	print *,st
	if(st == STATUS_OK)then
		call populate_jobs(ibs, jobs)
		deallocate(ibs)
		tmp= pack(jobs,jobs%active)
		if(size(tmp) > 0)then
			allocate(arr(1:size(tmp)))
			do i= 1,size(tmp)
				arr(i)%comp=> gen_job(tmp(i))
				if(associated(arr(i)%comp))then
					print *,"STARTED ",trim(tmp(i)%job_name)
					call arr(i)%comp%driver()
					call arr(i)%comp%output()
					deallocate(arr(i)%comp)
					print *,"DONE ",trim(tmp(i)%job_name)
				end if
			end do
		end if
	end if

	if(flag_wait)then
		print *, '==========================================='
		print *, '==================DONE====================='
		print *, '==========================================='
		read *
	end if
	
	end program engine